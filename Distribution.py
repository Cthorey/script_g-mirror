import sys,os
import matplotlib.pylab as plt
import numpy as np
import pandas as pd
import Fig_Tool as FST
from mpl_toolkits.axes_grid1 import make_axes_locatable
from Data_Load import *
from Plot import *
import scipy.stats as st
from sympy import latex
import StringIO #Used as buffer

def SE(a):
    return np.mean(a)/np.sqrt(len(a))

class Scatter(Load_G,Plot):
    
    '''
    Set of method and attribut that handle the distribution for
    FFC or UC

    attribut :
    col : Column that we want to examine. If we want to consider gravity,
    put the name of the column +_, e.g 'GE_mean_' or 'GI_mean_'. Instead,
    when considerign a column with no filter dependance, put 'Depth','Class'.
    
    model = List that contains the value of lambda we want to
    consider, e,g [10,20] will considere the value lambda = 10 and
    lambda =20 for the low pass filter. Let blank if considering the
    depth or other column that do not depend upon the filter

    bin_x1 : bin for the histogram of the diameter

    bin_y1 : bin for the histogram of the crust anomaly

    method:

    m_table(name) : return a table of shape with different parameter
    for the distribution for different values of the number lambda
            F_MARIA     C_MARIA  Model
    SD    20.196680   18.195813     10
    Size  37.000000  450.000000     10
    SE     0.975291    0.049602     10
    Mean   5.932465    1.052217     10
    SD    18.239519   16.676838     20
    Mean   5.539254    1.177461     20
    SE     0.910648    0.055506     20
    Size  37.000000  450.000000     20

    m_ttest : return a cross table that show the (t,p) values for the
    different location indicateds in mask_C and mask_F for the different
    values of the filter lambda

    m_scat(mod) : take as an argumetn the value of the filter as a
    string (e.g '40') and return a plot Crustal_anomaly versus Diameter

    m_hist_1(mod): return m_scat + an hist above that show the diameter
    distribution binned in bin_x1

    m_hist_2(mod): return m_scat + an hist on the right that show an histo
    gram of the crust anomaly binned in bin_y1

    '''

    def __init__(self,mask_C,mask_F):
        Load_G.__init__(self)
        Plot.__init__(self)
        self.mask_C = mask_C
        self.mask_F = mask_F
        self.model = [10,20,30,40,60,80,100]
        self.x_label = 'Diameter km'
        self.y_label = 'Crust anomaly $\delta_g$'
        self.y_lim = -80,80
        self.x_lim = 0,200
        self.bin_x1 = 10
        self.bin_y1 = 10
        self.col = 'Local_Mean_'
        self.col_l = ['Local_Mean_','S_mean_100_']
        self.axScatter = ''
        self.axHisty = ''
        self.axHistx = ''
        self.compteur = 0
        self.lable = ['FFC','FFC corrected','FFC']
        self.info = ''
        self.choice = ''
        self.zone = 'H'
        self.size = 0.5
        
    def m_init(self):
        self.axScatter = ''
        self.axHisty = ''
        self.axHistx = ''
        self.divider = ''

    def m_table(self):

        tab = dict.fromkeys(self.model)
        for i,mod in enumerate(self.model):
            
            index = {'Size':len,'Mean':np.mean,'SE':SE,'SD':np.std} #Index creation
            table = pd.DataFrame({'Index':index.keys()})
            table['Model']=mod
            
            mask = self.m_mask_F_C(self.mask_F,self.mask_C)
            table = []
            table = pd.DataFrame([[func(df[self.col+str(mod)]) for df in mask.itervalues()] for func in index.itervalues()],columns=mask.keys(),index=index.keys())
            table['Model'] = mod
            tab[mod] = table
        Tab = pd.concat(tab.values())
        Tab = Tab.drop('Index',0).sort('Model')
        
        return Tab

    def m_ttest(self):
        ''' Realise un tableau avec les different valeur du ttest'''
        tab = dict.fromkeys(self.model)        
        for i,mod in enumerate(self.model):
            
            mask = self.m_mask_F_C(self.mask_F,self.mask_C)
            table = pd.DataFrame({'Index':mask.keys()})
            table = pd.DataFrame([[st.ttest_ind(elt1[self.col+str(mod)],elt2[self.col+str(mod)]) for elt1 in mask.itervalues()] for elt2 in mask.itervalues()],columns=mask.keys(),index=mask.keys())
            table['Model']=mod
            tab[mod]=table
            
        return pd.concat(tab.values()).sort('Model')

    def m_table_paper(self,mod,Loc):
        ''' Retunr a table for the paper concerning the crustal anomlay'''

        columns = ['N',r'$\mu_{\delta_g}$',r'SE$_{\delta_g}$',r'$\sigma_{\delta_g}$',r'$t$',r'$p$']
        mask = self.m_mask_F_C(self.mask_F,self.mask_C)
        tab = dict.fromkeys(columns)
        
        a=[]
        for key,df in mask.iteritems():
            df = df[self.col+str(mod)]
            tab_tmp =map(lambda x: '%3.2f' % x,[df.mean(),df.std()/np.sqrt(len(df)),df.std(),st.ttest_ind(mask[Loc][self.col+str(mod)],df)[0]])
            a.append([len(df)]+tab_tmp+map(lambda x: '%3.5f' % x,[st.ttest_ind(mask[Loc][self.col+str(mod)],df)[1]]))
              
        return  pd.DataFrame(a,index=mask.keys(),columns=columns)
        

    def m_scat(self,mode):
        ''' Scatter plot '''

        if self.axScatter == '':
            self.fig, self.axScatter = self.m_figure()
        #self.axScatter.cla()
        c = self.m_color_map(len(self.mask_C+self.mask_F))
        mask = self.m_mask_F_C(self.mask_F,self.mask_C)
        
        i=0
        for key,df in mask.iteritems():
            self.axScatter.scatter(df.Diameter,df[self.col+str(mode)],color=self.info[key]['color'],alpha=self.info[key]['alpha'],marker=self.info[key]['marker'],s=self.info[key]['s'],zorder=0,label = self.info[key]['label'])
            self.axScatter.hlines(0,xmin =0,xmax =200,color='k',linestyles = 'dashed')
            i +=1
            #self.axScatter.text(95,-25,'trend: mean $\pm$ standard error',size = self.font)
        self.m_axes(self.axScatter)
        self.m_save_fig(self.fig)
        self.axScatter.set_rasterization_zorder(1)
        
        if self.axHisty == '':
            self.divider = make_axes_locatable(self.axScatter)

        return self.fig,self.axScatter

    def m_hist_1(self,mode):
        fig,divider,axScatter = self.m_scat(mode)
        
        axHistx = divider.append_axes("top", 2, pad=0.4, sharex=axScatter)
        #make some labels invisible
        plt.setp(axHistx.get_xticklabels(),
                visible=False)
        
        bins = np.arange(self.x_lim[0],self.x_lim[1]+self.bin_x1,self.bin_x1)
        width1 = (bins[1]-bins[0])/(len(self.mask_C+self.mask_F))
        width = (bins[1]-bins[0])
        c = self.m_color_map(len(self.mask_C+self.mask_F))
        mask = self.m_mask_F_C(self.mask_F,self.mask_C)
        
        i=0
        for key,elt in mask.iteritems():
            n,bin = np.histogram(elt.Diameter,bins = bins)
            n = n/np.float(n.sum())*100
            axHistx.bar(bin[:-1]+width1*np.float(i),n,width1,color = c[i],label ='hello' )
            i+=1
        
        axScatter.set_xlim(self.x_lim)
        axHistx.set_xlim(self.x_lim)
        FST.tick(axHistx,self.font)

        #axHistx.axis["bottom"].major_ticklabels.set_visible(False)
        for tl in axHistx.get_xticklabels():
            tl.set_visible(False)
        axHistx.set_yticks([0,25,50,75])
        axHistx.set_ylabel('Frequency (%)',size = self.font)
        self.m_save_fig(fig)
        
    def m_hist_2(self,mode):

        self.m_scat(mode)
        if self.axHisty == '':
            self.axHisty = self.divider.append_axes("right", 3, pad=0.4, sharey=self.axScatter)
            #make some labels invisible
            plt.setp(self.axHisty.get_yticklabels(),visible=False)

        bins = np.arange(self.y_lim[0],self.y_lim[1]+self.bin_y1,self.bin_y1)
        width1 = (bins[1]-bins[0])/len(self.mask_C+self.mask_F)
        width = (bins[1]-bins[0])
        c = self.m_color_map(len(self.mask_C+self.mask_F))
        mask = self.m_mask_F_C(self.mask_F,self.mask_C)
        i=0
        for key,elt in mask.iteritems():
            n,bin = np.histogram(elt[self.col+str(mode)],bins = bins)
            n = n/np.float(n.sum())*100
            #self.axHisty.barh(bin[:-1]-width/np.float(len(mask))+width1*i,n,height=width1,color = c[i])
            self.axHisty.plot(n,bin[:-1],color=self.info[key]['color'],marker ='o',markersize=2,linewidth = 2)
            self.axHisty.hlines(0,xmin =0,xmax =40,color='k',linestyles = 'dashed')
            i+=1
       
        for tl in self.axHisty.get_yticklabels():
            tl.set_visible(False)
        self.axHisty.set_xticks([0,20,40])
        self.axHisty.set_xlabel('Frequency (%)',size = self.font)
        
        self.axScatter.set_ylim(self.y_lim)
        self.axHisty.set_ylim(self.y_lim)
        FST.tick(self.axHisty,self.font)
        self.m_save_fig(self.fig)
        self.compteur+=1
        
        return self.fig,self.axScatter
    
    def m_hist_only(self,mode):
        
        fig, ax = self.m_figure()
        c = self.m_color_map(len(self.mask_C+self.mask_F))
        mask = self.m_mask_F_C(self.mask_F,self.mask_C)
        i=0

        bins = np.arange(self.x_lim[0],self.x_lim[1]+self.bin_y1,self.bin_y1)
        width1 = (bins[1]-bins[0])/(len(self.mask_C+self.mask_F)+0.5)
        width = (bins[1]-bins[0])

        for key,df in mask.iteritems():
            n,bin = np.histogram(df[self.col+str(mode)],bins = bins)
            n = n/np.float(n.sum())*100
            ax.plot(bin[:-1]-width/np.float(len(mask))+width1*i,n,width1,color = c[i],label = key + ' M = {0:1.2f} mGal; SD = {1:1.2f} mGal'.format(df[self.col+str(mode)].mean(),df[self.col+str(mode)].std()))
            i+=1

            
        self.m_axes(ax)
        self.m_save_fig(fig)

        
    def m_plot_distance(self,mod):

        mask_F = ['F_'+self.zone]
        mask_F = mask_F+['F_H_'+str(elt) for elt in np.arange(10,300,10)]
        mask_C = ['C_'+self.zone]
        mask_C = mask_C+['C_H_'+str(elt) for elt in np.arange(10,300,10)]
        dist=[0]
        dist= dist +[int(elt.split('_')[-1]) for elt in mask_F if elt != 'F_H']
        tab = self.m_table()

        serie_C= tab[tab.Model == mod][mask_C].loc['Mean']
        serie_C_err = tab[tab.Model == mod][mask_C].loc['SE']

        serie_F= tab[tab.Model== mod][mask_F].loc['Mean']
        serie_F_err = tab[tab.Model == mod][mask_F].loc['SE']

        print np.array(dist)
        print np.array(serie_C)
        fig,ax= self.m_figure()
        ax.errorbar(np.array(dist),np.array(serie_C),np.array(serie_C_err),color='b')
        ax.errorbar(np.array(dist),np.array(serie_F),np.array(serie_F_err),color='r')
        self.m_axes(self.ax)
        self.m_save_fig(self.fig)


    def To_LaTeX(self,df,size):
        """
        Convert a pandas dataframe to a LaTeX tabular.
        Prints labels in bold, does not use math mode
        """
    
        df.index= [elt.replace('_','\_') for elt in df.index]
        numColumns = df.shape[1]
        numRows = df.shape[0]
        output = StringIO.StringIO()
        alignment='>{\\centering\\arraybackslash}m{'+size+'}|'
        numColumns = df.shape[1]
        colFormat = ("%s | %s " % (alignment, alignment * (numColumns)))
        #Write header
        output.write("\\begin{tabular}{%s}\n" % colFormat)
        columnLabels = ["%s" % label for label in df.columns]
        output.write('&\multicolumn{4}{'+alignment+'}{}&\multicolumn{2}{c|}{Student"s t-test} \\\\ \n')
        output.write("& %s \\\\ \n" % " & ".join(columnLabels))
        output.write("\hline \n")
        #output.write('&'*(numColumns) + '\\\\ \n')
        #Write data lines
        for i in xrange(numRows):
            output.write("%s & %s \\\\ \n"
                        % (df.index[i], " & ".join([str(val) for val in df.ix[i]])))
           # output.write('&'*(numColumns) + '\\\\ \n')
        #Write footer
        output.write("\\end{tabular}")
        return output.getvalue()
