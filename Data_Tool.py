#####################################################################################
################# Ensemble de fonction qui serve pour Crater_Detph #################
######################################################################################

def Sample_Grd(Path,Carte_Final):
    
    import numpy as np
    from scipy.io import netcdf_file as netcdf
    from Scientific.IO.NetCDF import NetCDFFile
    import scipy as sp
    import pandas as pd
    import sys
    import os

    file=os.listdir(Path)
    grdfile=Path+'/'+Carte_Final
    print grdfile

    if Carte_Final == 'LDEM_64.grd':
        xf = 'x'
        yf = 'y'
    else:
        xf ='lon'
        yf = 'lat'
        
    x=pd.DataFrame(np.copy(NetCDFFile(grdfile,'r').variables[xf][::],'f4'),columns=['long'],dtype='f4')
    y=pd.DataFrame(np.copy(NetCDFFile(grdfile,'r').variables[yf][::],'f4'),columns=['lat'],dtype='f4')
    z=pd.DataFrame(np.copy(NetCDFFile(grdfile,'r').variables['z'][::],'f4'),columns=x.index,index=y.index,dtype='f4')
    

    return x,y,z 

def Select_Zoom(F,i,x,y,z):
    
    import window

    a=float(F.Lat[i])
    c=float(F.Long[i])
    Fen=str(window.lambert((float(F.Diameter[i])*360/(2*10917.0)),a,c)[4])
    Fen=[elt for elt in Fen.split() if elt!=''][0]
    Fen=Fen[1:-1].split('/')
    long_Min=float(Fen[0])
    long_Max=float(Fen[2])
    if long_Min<0 :
        long_Min=360+long_Min
    if long_Max<0:
        long_Max=360+long_Max

    lat_Min=float(Fen[1])
    lat_Max=float(Fen[3])
    data=z.iloc[y[(y.lat>lat_Min) & (y.lat<lat_Max)].index.tolist(),x[(x.long>long_Min) & (x.long<long_Max)].index.tolist()]

def Select_Zoom_2(x,y,z,size,lat_0,long_0):
    
    import window
    import numpy as np
    import matplotlib.pylab as plt
    import sys
    
    Fen=str(window.lambert((float(size)*360/(10917.0)),lat_0,long_0)[4])
    Fen=[elt for elt in Fen.split() if elt!=''][0]
    Fen=Fen[1:-1].split('/')
    long_Min=float(Fen[0])
    long_Max=float(Fen[2])

    if long_Min<0 :
        long_Min=360+long_Min
    if long_Max<0:
        long_Max=360+long_Max


    lat_Min=float(Fen[1])
    lat_Max=float(Fen[3])
    if long_Min>long_Max:
        data_zoom_x1=x[(x.long>long_Min)]
        data_zoom_x2=x[(x.long<long_Max)]
        data_zoom_x=data_zoom_x1.append(data_zoom_x2)
    else:
        data_zoom_x=x[(x.long>long_Min) & (x.long<long_Max)]
    data_zoom_y=y[(y.lat>lat_Min) & (y.lat<lat_Max)]
    data_zoom=z.iloc[data_zoom_y.index.tolist(),data_zoom_x.index.tolist()]
    return data_zoom_x,data_zoom_y,data_zoom


def Circular_Mask(name,data):

    import numpy as np
    import matplotlib.pyplot as plt
    import sys
    import Figure_Tool as ft

    data=np.array(data)
    radius_a=np.floor((data.shape[1])/2.) #Nombre de colonnes diviser par 2
    radius_b=np.floor((data.shape[0])/2.) # Nombre de ligne diviser par 2
    y,x = np.ogrid[-radius_b:radius_b+1, -radius_a:radius_a+1]
    mask = (x/radius_a)**2 + (y/radius_b)**2 <= 1
    if  data.shape[1] % 2 == 0 :
        mask = np.delete(mask,radius_a,1)
    if  data.shape[0] % 2 == 0 :
        mask = np.delete(mask,radius_b,0)

    return data[mask],data[~mask]

def Circular_Mask_2(x,y,z,diametre,lat_0,long_0):
    # fait un mask plus precis que Circular_Mask
    import numpy as np
    import matplotlib.pyplot as plt
    import sys
    import window

    # Extrait la fenetre qui correspond a cote = diametre
    Fen=str(window.lambert((diametre*360/(2.0*10917.0)),lat_0,long_0)[4])
    Fen=[elt for elt in Fen.split() if elt!=''][0]
    Fen=Fen[1:-1].split('/')
    long_Min=float(Fen[0])
    long_Max=float(Fen[2])
    if long_Min<0 :
        long_Min=360+long_Min
    if long_Max<0:
        long_Max=360+long_Max
    
    if long_0<0 :
        long_0=360+long_0

    lat_Min=float(Fen[1])
    lat_Max=float(Fen[3])

    d_x=long_Max-long_0
    d_y=lat_Max-lat_0
    
    x=np.array(x)
    y=np.array(y)
    z=np.array(z)
    
    X,Y = np.meshgrid(x,y)
    mask = ((X-long_0)/d_x)**2+((Y-lat_0)/d_y)**2 <=1 

    return mask,z[mask],z[~mask]

def Couronne_Mask_2(x,y,z,diametre_int,diametre_ext,lat_0,long_0):
    # fait un mask en forme de courronne
    import numpy as np
    import matplotlib.pyplot as plt
    import sys
    import window

    # Extrait la fenetre qui correspond a cote = diametre
    Fen_Int=str(window.lambert((diametre_int*360/(2.0*10917.0)),lat_0,long_0)[4])
    Fen_Ext=str(window.lambert((diametre_ext*360/(2.0*10917.0)),lat_0,long_0)[4])
    Fen_Int=[elt for elt in Fen_Int.split() if elt!=''][0]
    Fen_Ext=[elt for elt in Fen_Ext.split() if elt!=''][0]
     
    Fen_Int=Fen_Int[1:-1].split('/')
    Fen_Ext=Fen_Ext[1:-1].split('/')

    long_Min_Int=float(Fen_Int[0])
    long_Max_Int=float(Fen_Int[2])
    long_Min_Ext=float(Fen_Ext[0])
    long_Max_Ext=float(Fen_Ext[2])
    
    if long_Min_Int<0 :
        long_Min_Int=360+long_Min_Int
    if long_Max_Int<0:
        long_Max_Int=360+long_Max_Int
    if long_Min_Ext < 0 :
        long_Min_Ext = 360 + long_Min_Ext
    if long_Max_Ext < 0 :
        long_Max_Ext = 360 + long_Max_Ext
    
    if long_0 < 0 :
        long_0 = 360 + long_0

    lat_Min_Int = float(Fen_Int[1])
    lat_Max_Int = float(Fen_Int[3])
    lat_Min_Ext = float(Fen_Ext[1])
    lat_Max_Ext = float(Fen_Ext[3])
    
    d_x_Int = long_Max_Int-long_0
    d_y_Int = lat_Max_Int-lat_0
    d_x_Ext = long_Max_Ext-long_0
    d_y_Ext = lat_Max_Ext-lat_0
   
    x = np.array(x)
    y = np.array(y)
    z = np.array(z)
    
    X,Y = np.meshgrid(x,y)
    mask = (((X-long_0)/d_x_Int)**2+((Y-lat_0)/d_y_Int)**2 >=1) & (((X-long_0)/d_x_Ext)**2+((Y-lat_0)/d_y_Ext)**2 <=1)

    return mask,z[mask],z[~mask]


def Histo_Data(Int,step) :
    import numpy as np
    
    bins_Int=np.arange(Int.min(),Int.max(),step)
    n,bins= np.histogram(Int,bins=bins_Int)

    return n,bins

def match(Ref,Df1,Df2,Df3):
    import pandas as pd
    import numpy as np
    import sys
    
    if Ref.Name.tolist() != Df1.Name.tolist() :
        raise ValueError('Probleme de merging Dataframe')
        sys.exit()
    
    if Ref.Name.tolist() != Df2.Name.tolist() :
        raise ValueError('Probleme de merging Dataframe')
        sys.exit()

    if Ref.Name.tolist() != Df3.Name.tolist() :
        raise ValueError('Probleme de merging Dataframe')
        sys.exit()

    Df1 = Df1.drop('Name',1)
    Df2 = Df2.drop('Name',1)
    Df3 = Df3.drop('Name',1)

    if len(Ref)!= len(Df1) or len(Ref)!=len(Df2):
        raise ValuerError('Probleme de merging Dataframe')
        sys.exit()
    if Ref.index.tolist() != Df1.index.tolist() :
        raise ValueError('Probleme de merging Dataframe')
        sys.exit()
    if Ref.index.tolist() != Df2.index.tolist() :
        raise ValueError('Probleme de merging Dataframe')
        sys.exit()
    if Ref.index.tolist() != Df3.index.tolist() :
        raise ValueError('Probleme de merging Dataframe')
        sys.exit()
    return Ref,Df1,Df2,Df3

def Load_DataFrame(C_lola,C_gravi,F_lola,F_gravi):

    import pandas as pd
    import sys
    import numpy as np

    Liste_Lola=['Name','Lat','Long','Class','Diameter','Location','L_Max_Bins_Int','L_Max_Int','L_Min_Int','L_Mean_Int','L_Std_Int','L_StdErr_Int','L_Max_Bins_Ext','L_Max_Ext','L_Min_Ext','L_Mean_Ext','L_Std_Ext','L_StdErr_Ext']
    Liste_Gravi=['Name','Lat','Long','Class','Diameter','Location','G_Max_Bins_Int','G_Max_Int','G_Min_Int','G_Mean_Int','G_Std_Int','G_StdErr_Int','G_Max_Bins_Ext','G_Max_Ext','G_Min_Ext','G_Mean_Ext','G_Std_Ext','G_StdErr_Ext']
    Liste3=['G_Max_Bins_Int','G_Max_Int','G_Min_Int','G_Mean_Int','G_Std_Int','G_StdErr_Int','G_Max_Bins_Ext','G_Max_Ext','G_Min_Ext','G_Mean_Ext','G_Std_Ext','G_StdErr_Ext']

    F_Lola = pd.read_csv(F_lola)
    C_Lola = pd.read_csv(C_lola)
    F_Gravi =  pd.read_csv(F_gravi)
    C_Gravi = pd.read_csv(C_gravi)
    
    F_Lola=F_Lola.drop_duplicates(cols='Name', take_last=True)
    F_Gravi=F_Gravi.drop_duplicates(cols='Name', take_last=True)
    C_Gravi=C_Gravi.drop_duplicates(cols='Name', take_last=True)
    C_Lola=C_Lola.drop_duplicates(cols='Name', take_last=True)

    F_Lola.columns=Liste_Lola
    C_Lola.columns=Liste_Lola
    F_Gravi.columns=Liste_Gravi
    C_Gravi.columns=Liste_Gravi

    F_Lola=F_Lola[F_Lola.Name.isin([elt for elt in F_Lola.Name if elt in np.array(F_Gravi.Name)])]
    F_Gravi=F_Gravi[F_Gravi.Name.isin([elt for elt in F_Lola.Name if elt in np.array(F_Gravi.Name)])]
    C_Lola=C_Lola[C_Lola.Name.isin([elt for elt in C_Lola.Name if elt in np.array(C_Gravi.Name)])]
    C_Gravi=C_Gravi[C_Gravi.Name.isin([elt for elt in C_Lola.Name if elt in np.array(C_Gravi.Name)])]

    F_Lola=F_Lola.sort('Name')
    C_Lola=C_Lola.sort('Name')
    F_Gravi=F_Gravi.sort('Name')
    C_Gravi=C_Gravi.sort('Name')
    
    if F_Lola.Name.tolist()!=F_Gravi.Name.tolist() or len(F_Lola)!=len(F_Gravi):
        raise ValueError('Probleme de merging Dataframe')
        sys.exit()
    if C_Gravi.Name.tolist()!=C_Lola.Name.tolist() or len(C_Lola)!=len(C_Gravi):
        raise ValueError('Probleme de merging Dataframe')
        sys.exit()       

    C_Gravi.index=np.arange(0,len(C_Gravi),1)
    F_Gravi.index=np.arange(0,len(F_Gravi),1)
    C_Lola.index=np.arange(0,len(C_Lola),1)
    F_Lola.index=np.arange(0,len(F_Lola),1)

    C_Gravi=C_Gravi[Liste3]
    F_Gravi=F_Gravi[Liste3]
    
    C=C_Lola.join(C_Gravi)
    F=F_Lola.join(F_Gravi)

    if len(F)!=len(F_Lola) or len(F)!=len(F_Gravi):
        raise ValueError('Probleme de merging Dataframe')
        sys.exit()
    if len(C)!=len(C_Lola) or len(C)!=len(C_Gravi):
        raise ValueError('Probleme de merging Dataframe')
        sys.exit()
        
    return C,F
    
    
########################################################################################
###################### ##################################################################
# Fonction obsolete mais qui peuvent quand meme resservir un jour


def Coordinate_carte(Carte2) :
    for elt in Carte2:
        elt['Long_1']=float(elt['Long_1'].split('e')[0])
        elt['Long_2']=float(elt['Long_2'].split('e')[0])
        
        if len(elt['Lat_1'].split('n'))>1:
            elt['Lat_1']=float(elt['Lat_1'].split('n')[0])
        else :
            elt['Lat_1']=-float(elt['Lat_1'].split('s')[0])
        if  len(elt['Lat_2'].split('n'))>1:
            elt['Lat_2']=float(elt['Lat_2'].split('n')[0])
        else :
            elt['Lat_2']=-float(elt['Lat_2'].split('s')[0])
    for elt in Carte2:
        if elt['Lat_1']>elt['Lat_2'] :
            a=elt['Lat_1']
            b=elt['Lat_2']
            elt['Lat_2']=a
            elt['Lat_1']=b
            
def Select_carte(F,i,Carte) :
    import window
    import sys
    a=float(F.Lat[i])
    c=float(F.Long[i])
    Fen=str(window.lambert((float(F.Diameter[i])*360/(2*10917.0)),a,c)[4])
    Fen=[x for x in Fen.split() if x!=''][0]
    Fen=Fen[1:-1].split('/')
    lom=float(Fen[0])
    loM=float(Fen[2])
    Lam=float(Fen[1])
    LaM=float(Fen[3])
    if -180<lom<0:
        lom=lom+360
    if -180<loM<0 :
        loM=loM+360
    print lom,loM,Lam,LaM
    
    if Lam>55.0 :
        Carte_Final=Carte[0]['Name']
    elif LaM<-55.0 :
        Carte_Final=Carte[1]['Name']
    else :
        if 120.0<lom<240.0 :
            Carte_Final=Carte[2]['Name']
        elif 240.0<lom<360.0 :
            Carte_Final=Carte[3]['Name']
        elif 0.0<lom<120.0:
            Carte_Final=Carte[4]['Name']

    print a,c,lom,loM,Lam,LaM,Carte_Final,Fen
    return(Carte_Final)



def Sample_Grd_Pickle(Path_LOLA,Carte_Final):
    # version avec le pickle thing
    import numpy as np
    from scipy.io import netcdf_file as netcdf
    import scipy as sp
    import pandas as pd
    #import cPickle as pickle
    import Unload as u
    import sys
    import os

    file=os.listdir(Path_LOLA)
    
    #if Carte_Final.split('.')[0] in file :
    #    print 'Pickle file found'
    #    x,y,z = u.unload_pickle(Path_LOLA+'/'+Carte_Final.split('.')[0])
        
    #else:
    #   print 'Pickle file not found'
    grdfile=Path_LOLA+'/'+Carte_Final
    
    x=pd.DataFrame(np.copy(netcdf(grdfile,'r').variables['x'][::-1],'f4'),columns=['long'],dtype='f8')
    y=pd.DataFrame(np.copy(netcdf(grdfile,'r').variables['y'][::-1],'f4'),columns=['lat'],dtype='f8')
    z=pd.DataFrame(np.copy(netcdf(grdfile,'r').variables['z'][::-1],'f4'),columns=x.index,index=y.index,dtype='f8')

        #D_pickle={'x':x,'y':y,'z':z}
        #with open(Path_LOLA+'/'+Carte_Final.split('.')[0],'wb') as f:
        #    pickle.dump(D_pickle,f,pickle.HIGHEST_PROTOCOL)

    return(x,y,z)

def Select_Zoom_GMT(F,i,Script,Executable,Path_LOLA,Carte_Final):
    import window
    from scipy.io import netcdf_file as netcdf
    import subprocess
    import numpy as np

    #Executable = 'SampleGrd'
    #Script = '//Users/Clemantes/Documents/These/Projet/FFC/Gravi_GRAIL/Traitement/Script_GMT/Figure/'
    #Clean = 'clean.sh'
    
    a=float(F.Lat[i])
    c=float(F.Long[i])
    Fen=str(window.lambert((float(F.Diameter[i])*360/(2*10917)),a,c)[4])
    print Fen
    Fen=[x for x in Fen.split() if x!=''][0]
    Projection="JA"+str(c)+"/"+str(a)+"/"+str(2.5)+"i"
    with open(str(Script)+str(Executable)+'.sh', 'r') as script:
        with open(str(Script)+str(Executable)+'_tmp.sh', 'wr+') as script_tmp:
            for l in script:
                if l == 'Sample=Name\n':
                    to_write = l.replace('Name',str(Path_LOLA)+'/Subset/'+str(F.Name[i])+'.grd')
                elif l == 'Fenetre=Cadre\n':
                    to_write = l.replace('Cadre',str(Fen))
                elif l == 'Projection=proj\n':
                    to_write = l.replace('proj',str(Projection))
                elif l == 'path=Direction\n':
                    to_write =  l.replace('Direction',str(Path_LOLA)+'/'+Carte_Final)
                else:
                    to_write = l
                script_tmp.write(to_write)
        proc = subprocess.Popen(str(Script)+str(Executable)+'_tmp.sh',shell=True,stdout=subprocess.PIPE)
        output = proc.stdout.read()
        Grdfile=str(Path_LOLA)+'/Subset/'+str(F.Name[i])+'.grd'
    data=np.copy(netcdf(Grdfile,'r').variables['z'][::-1])

    return(data)

def figure(z_1):

    import numpy as np
    import matplotlib.pylab as plt
    
    plt.imshow(z_1)
    plt.colorbar()
    plt.show()
    plt.subplot(121)
    plot1=np.copy(z_1)
    plot1[~mask]=0
    plt.imshow(plot1)
    plt.colorbar()
    plt.subplot(122)
    plot2=np.copy(z_1)
    plot2[~mask_c]=0
    plt.imshow(plot2)
    plt.show()

def Num_Int(Synth,D):

    import pandas as pd
    import numpy as np
    ''' Numerical integration poyr synth'''
    rn = Synth.r-Synth.r[0]/2.0
    rn[0]=0
    Gi = Synth.G
    Mean=[]
    for i,elt in enumerate(rn):
        if i == 0:
            Mean.append(Gi[0]*rn[1]**2/2.0)
        elif i != len(rn)-1:
            print i,rn[i+1]
            Mean.append(Gi[i]*(rn[i+1]**2-rn[i]**2)/2.0)
        else:
            Mean.append(Gi[i]*((rn[i]+Synth.r[0])**2-rn[i]**2)/2.0)
    
    return np.array(Mean).sum()*2.0/D**2

def Num_Int_2(Synth,D,Dr):
    import pandas as pd
    import numpy as np
    ''' Numerical integration poyr synth'''

    G = Synth.G
    r = Synth.r
    Mean =[]
    for i,elt in enumerate(r):
        if i == 0:
            Mean.append(G[0]*Dr**2/2.0)
        else:
            Mean.append(G[i]*2*Dr*r[i])

    return np.array(Mean).sum()*2.0/D**2
            

