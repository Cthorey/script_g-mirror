#!/usr/bin/env python
################################################
# Script definition
# OUTPUT : Fichier qui contient les caracteristiques ds FFC + une collonne avc l'anomalie moyennne synthetique. A lancer sur clavius !

# DESCRIPTION : Ce script sert a ecrire un fichier ds lequelle figure les donne relatif au valeur d'anomalie que l'on attend pour les FFCs.
# Pour cela, il a besoin d'un set de crater normaux. A partir de ce dataset, on calcule une loi d'echelle Profondeur en fonction du Diametre #pour les craters normaux dont on se sert de refernce pour calculer l'epaisseur de l'intrusion sour ls FFCS. L'epaisseur de l'intrusion etant#juste la difference entre la profondeur calculer du crater et la loi d'echelle. 3 epaisseur sont defini, lepaisseur relatif a la loi dechell#e, l epaisseur relatif a la loi d echelle + la dispersion du nuage de point et la meme chose ou l'on soustrai la dispersion. A partir de la #, on se sert des profil theorique d'epaisseur adimensionne obtenue a l'aide de notre model. Ces profil sont normalise, donc il suffit
# de multiplier r par le rayon du crater et h par l'epaisseur de l'intrusion. A partir de ce profil radial, on peut utilise le script de marc# FFC_Gravity.f95 pour construire le profil de gravi associe. De ceprofil on extrait la valeur moyenne au sein du Crater. On repet cette oper#ation autant de fois que de FFCs et le tout est inscrit ds un fichier output.

# NB: a la difference de Data_Synthetic.py, le fichier inscrit juste la valeur moyenne qui est la seul valeur qui va nous servir par la suit

# NB2: Pour empaqueter FFC_Gravity.f95 dans une librairie python, utiliser:
# f2py -I/Users/Shared/SHTOOLS -L/Users/Shared/SHTOOLS -lSHTOOLS2.8 -lfftw3 -lm -llapack -lblas -c -m FFC_Gravity_2 FFC_Gravity.f95

################################################
# Library a utiliser
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import sys
import Data_Tool as CDT # Librairie qui contient toutes les fonction ds ls fichier.py qui commence par data
import FFC_Gravity_2 # Librairie faire a partir du fichier fortran
import Fig_Tool as FST
from Data_Load import *
from Histo_Mean import *
from Depth import *
import Data_Obs_Tool as DOT

################################################
# Chose a renseigner

Root = DOT.Init('clavius')
Crater_Plot = ['F_Taruntius'] # Si on veut tester le script, et pour pas que l'operation se repete sur plein de crater. Mettre le nom du crater

lambda_filter = ['10','20','30','40','60','80','100']
#lambda_filter = ['10']
rho = 1
Path_Save = Root+'/Gravi_GRAIL/Traitement/GRAIL_SYNTH/'
Plot.Path_Save = '/Users/thorey/Documents/These/Projet/FFC/Gravi_GRAIL/Traitement/GRAIL_SYNTH/'
 
###################################
# Load Dataset

mask_F = ['F_H','F_M','F_SPA']
mask_C = ['C_F_H','C_M','C_SPA']

obj = Depth(mask_C,mask_F)
obj.Lola = 'LDEM_64_'
Load_G.Root_F = Root+'Gravi_GRAIL/Traitement/Data_Observation/'
Load_G.Root = Root+'Gravi_GRAIL/Traitement/Data_Observation/'
mask_F = obj.m_table_thickness()
F = pd.concat(mask_F.values())
F['Thick_C_max']=F.Thick_C+F.Sigma_Thick_C
F['Thick_C_min']=F.Thick_C-F.Sigma_Thick_C
#F=F[F.Name.isin(Crater_Plot)]
F.index = np.arange(0,len(F),1)

Thick =['Thick_C_min','Thick_C','Thick_C_max']

################################################
# Thickness intrusion

Path_I = Root+'Gravi_GRAIL/Traitement/GRAIL_SYNTH/Input/' # Input pour FFC_Gravity. Place ou il va chercher les profil d'epaisseur
Path_O = Root+'Gravi_GRAIL/Traitement/GRAIL_SYNTH/Output/' # Input pour FFC_Gravity. Place ou il enregistre les porfil de gravi
   
Class1 = ['1','3','5','6']
Class2 = ['2','4']
    
Stat = []
error = []

for lamb in lambda_filter :
    col = ['S_min_'+str(lamb),'S_mean_'+str(lamb),'S_max_'+str(lamb)]

    for j,thick in enumerate(Thick):
        Stat=[]
        for i,elt in enumerate(F.Name):
            Input = Path_O+elt+'.txt' #Path_input pour FFC_Gravity
            Output = Path_O+elt+'_grav.dat' #Path_output pour FFC_Gravity
    
            if str(F.Class[i]) in Class1:
                profil = Path_I+'Profil_Grav_Min.txt' # Si Ds la classe flat floor, va chercher le profil associe a flat floor
            elif str(F.Class[i]) in Class2:
                profil = Path_I + 'Profil_El_Min.txt' # Si Ds la classe convex, va chercher le profil associe a convex
            
            prof=pd.read_csv(profil,header=None,names=['r','h']) # Lit le profil Input d'epaisseur normalise selectionner (convex ou flat floor)
            prof['r']=prof.r*F.Diameter[i]/2.0*1000 # Multiplie par le rayon du crater et par 1000 ( doit etre en metre)
            prof['h']=prof.h*F[thick][i]*1000 # Multiplier par l'epaisseur de l'intrusion et par 1000 ( en m)
            prof.to_csv(Input,sep=',',index=None,header=None) # Ecrie dans Input le fichier d'epaisseur conforme et que va prendre FFC_Gravity.f95
            FFC_Gravity_2.ffc_gravity(Input,Output,rho,lamb) 
            Synth=pd.read_csv(Output,sep=',',header=None,names=['r','G']) # Dataframe ou l'on recupere le profil gravi, output de FFC_Gravity
            Dr = Synth.r[1]/2.0
            Mean = CDT.Num_Int_2(Synth[Synth.r<F.Diameter[i]*1000/2.0],F.Diameter[i]*1000/2.0,Dr)
            Stat.append(Mean) # On rajoute la valeur mean dans un vecteur
            
        map(os.remove,[Path_O+elt for elt in os.listdir(Path_O)])
        F[col[j]] = Stat

F.to_csv(Path_Save+str(obj.Lola)+str(obj.Grav)+'_FFC.txt',sep=',',index=None)

def figure():
    Synth1 = Synth[Synth.r<F.Diameter[i]*1000/2.0]
    fig,ax = obj2.m_figure()
    obj2.fig_name = 'f_'+str(lamb)+'_'+elt
    FST.Axi_fig(Synth1.r,Synth1.G,F.Diameter[i]/2.0,ax,[Synth.G.min(),Synth.G.max()])
    obj2.fig_b = False
    obj2.m_save_fig(fig)
