import sys,os
import matplotlib.pylab as plt
import numpy as np
import pandas as pd
import Fig_Tool as FST
from mpl_toolkits.axes_grid1 import make_axes_locatable
from Data_Load import *
from Plot import *
import scipy.stats as st

def SE(a):
    return np.mean(a)/np.sqrt(len(a))


class Distance_Maria(Load_G,Plot):

    def __init__(self,mask_F,mask_C):
        Load_G.__init__(self)
        Plot.__init__(self)
        self.x_label = 'X km from the lunar maria'
        self.y_label = 'Crust anomaly $\delta_g$'
        self.y_lim = -5,5
        self.x_lim = 0,400
        self.zone = 'H'
        self.col = 'Local_Mean_'
        self.mask_F = mask_F
        self.mask_C = mask_C
        self.model = [10,20,30,40,60,80,100]

    def m_table(self):

        tab = dict.fromkeys(self.model)
        for i,mod in enumerate(self.model):
            
            index = {'Size':len,'Mean':np.mean,'SE':SE,'SD':np.std} #Index creation
            table = pd.DataFrame({'Index':index.keys()})
            table['Model']=mod
            
            mask = self.m_mask_F_C(self.mask_F,self.mask_C)
            table = []
            table = pd.DataFrame([[func(df[self.col+str(mod)]) for df in mask.itervalues()] for func in index.itervalues()],columns=mask.keys(),index=index.keys())
            table['Model'] = mod
            tab[mod] = table
        Tab = pd.concat(tab.values())
        Tab = Tab.drop('Index',0).sort('Model')
        
        return Tab

    def m_plot_distance(self,mod):

        dist=[0]
        dist= dist +[int(elt.split('_')[-1]) for elt in self.mask_F if elt != 'F_H']
        print dist
        tab = self.m_table()

        serie_C= tab[tab.Model == mod][self.mask_C].loc['Mean']
        serie_C_err = tab[tab.Model == mod][self.mask_C].loc['SE']

        serie_F= tab[tab.Model== mod][self.mask_F].loc['Mean']
        serie_F_err = tab[tab.Model == mod][self.mask_F].loc['SE']

        fig,ax= self.m_figure()
        ax.errorbar(np.array(dist),np.array(serie_C),np.array(serie_C_err),color='b',label = 'Crater_Head')
        ax.errorbar(np.array(dist),np.array(serie_F),np.array(serie_F_err),color='r',label = 'FFC' )
        self.m_axes(ax)
        self.m_save_fig(fig)

    def m_table_mean(self,mod):

        tab = self.m_table()
        return tab[tab.Model == 80].loc[['Mean','SE','Size']]

    def m_plot_corrona(self,mod):

        dist= [int(elt.split('_')[-1]) for elt in self.mask_F if elt != 'F_H']
        tab = self.m_table()
        serie_C= tab[tab.Model == mod][self.mask_C].loc['Mean']
        serie_C_err = tab[tab.Model == mod][self.mask_C].loc['SE']
        serie_F= tab[tab.Model== mod][self.mask_F].loc['Mean']
        serie_F_err = tab[tab.Model == mod][self.mask_F].loc['SE']

        fig,ax= self.m_figure()
        ax.errorbar(np.array(dist),np.array(serie_C),np.array(serie_C_err),color='b',label = 'Crater_Head')
        ax.errorbar(np.array(dist),np.array(serie_F),np.array(serie_F_err),color='r',label = 'FFC' )
        self.m_axes(ax)
        self.m_save_fig(fig)
