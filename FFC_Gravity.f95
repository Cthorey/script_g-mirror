SUBROUTINE  FFC_GRAVITY(Input, Output,rho,lambda)

  USE SHTOOLS
  USE PlanetsConstants

  IMPLICIT NONE

  character*300 ,intent(in)        :: Input, Output
  real*8        , intent(in)       :: rho
  integer       , intent(in)       :: lambda

  integer, parameter ::	degmax = 1440
  real*8 :: 		cilm(2, degmax+1, degmax+1), &
       			pi, d_out, gm, mass, interval_deg, interval_m, mpr, thick(1000), dist(1000), d, t, r_moho, h0
  integer :: 		lmax, i, j, nlat, nlong, n_out, nmax, lmax_calc, astat, l, num, q, k, l_start, l_width, &
  			cos_filter, high_pass
  real*8, allocatable ::	topo_grid(:,:), grav_grid(:,:)
  character*300 ::    infile, outfile

  pi = acos(-1.0d0)
  
  infile = Input 
  outfile = Output
  print*,'input',infile
  print*,'output',outfile
  
  gm = GM_Moon
  mass = mass_moon	
  mpr = R_Moon

  cos_filter = 1	! 1 = apply filter
  high_pass = 1
  
  h0 = 34.0d3	! average crustal thickness 
  !lambda = 60	! degree where the filter is 0.5
  r_moho = mpr - h0
  
  if (high_pass == 1) then
  	print*, "Lambda = ", lambda
  endif
  
 ! delta rho between mare 3400, and crust 2900, 

  nmax = 5	! This is more than sufficient for low amplitude topography

  lmax = 1000 ! this controls the output grid spacing
  l_start = 550	! this is the start of the cosine filter
  l_width = 100	! this is the width of the cosine filter
  lmax_calc = l_start + l_width	! This is the maximum degree used when evaluating the gravity field at each point. 
  
  if (cos_filter==1) then
  	print*, "Start of cosine taper = ", l_start
  	print*, "End of cosine taper = ", l_start + l_width
  endif
  
  if (lmax_calc > lmax) then
  	print*, "L_START + L_WIDTH must be less than or equal to LMAX"
  	print*, "L_START = ", l_start
  	print*, "L_WIDTH = ", l_width
  	print*, "LMAX = ", lmax
  	stop
  endif

  if (lmax > degmax) then
     print*, "LMAX must be less than or equal to DEGMAX"
     print*, "Change LMAX and recompile"
     print*, "LMAX = ", lmax
     print*, "DEGMAX = ", degmax
     stop
  endif

  nlat = 2*lmax + 2
  nlong = 2*nlat
  interval_deg = 180.0d0 / dble (nlat)
  interval_m = interval_deg * (2.0d0*pi*mpr / 360.0d0)
  print*, "Interval (in degrees) of topography grid = ", interval_deg
  print*, "Interval (in m) of topography grid = ", interval_m

  print*, "Lmax (sampling interval of output grid) = ", lmax
  print*, "Nlat, Nlong = ", nlat, nlong

  allocate(topo_grid(nlat, nlong), stat = astat)
  if (astat /= 0) then
     print*, "Problem allocating TOPO_GRID", astat
     stop
  endif

  allocate(grav_grid(nlat, nlong), stat = astat)
  if (astat /=0) then
     print*, "Problem allocating GRAV_GRID", astat
     stop
  endif

  cilm = 0.0d0
  topo_grid = 0.0d0
  grav_grid = 0.0d0

  open(12, file=infile)
  print*, infile
  thick= 0.0d0
  dist = 0.0d0
  num = 0
  do 
     read(12,*, iostat=astat) d, t
     if (astat /=0) exit
     num = num + 1
     dist(num) = d
     thick(num) = t
  enddo
  close(12)

  print*, "Number of data points = ", num
  print*, "Minimum and Maximum thickness (m) = ", minval(thick), maxval(thick)

  q=1
  do i=1, nlat
     d = dble(i-1)*interval_m

     if (d >= dist(num)) then
        topo_grid(i,1:nlong) = mpr
     else
        do j=q, num-1
           if (d >= dist(j) .and. d < dist(j+1)) then
              q=j
              t = thick(j) + (d-dist(j)) * (thick(j+1)-thick(j)) / (dist(j+1)-dist(j))
              topo_grid(i,1:nlong) = mpr + t
           endif
        enddo
     endif
  enddo
 

  print*, "Calculating gravity coefficients"

  call CilmPlus (cilm, topo_grid, lmax, nmax, mass, d_out, rho, 3, n=nlat)
  print*, d_out, mpr ! These should be almost the same, d_out is the mean elevation of topo_grid
  cilm(1,1,1) = 0.0d0	! set the degree-0 gravity to zero (we don't care about gm/r2 term.

  do l=1, lmax
     cilm(1:2,l+1,1:l+1) = cilm(1:2,l+1,1:l+1) * dble(l+1) * gm/(d_out**2) * 1.0d5 ! convert potential coefficients to gravity 
     										! in units of mGal	
  enddo
  
  if (cos_filter == 1) then
  	do l=l_start, l_start+l_width,1
  		cilm(1:2,l+1,1:l+1) = cilm(1:2,l+1,1:l+1) * cos(pi/2.0d0 * dble(l-l_start)/dble(l_width) )
 	enddo
 	 cilm(1:2,l_start+l_width+2:degmax+1,1:degmax+1) = 0.0d0
  endif

  if (high_pass == 1) then
  	do l=1, degmax
  		cilm(1:2,l+1,1:l+1) = cilm(1:2,l+1,1:l+1) * ( 1.0d0 - wl(l, lambda, mpr, r_moho) )
  	enddo
  endif

  print*, "Making grid of radial gravity field"
  call MakeGridDH(grav_grid, n_out,  cilm, lmax, sampling = 2, lmax_calc = lmax_calc)

  print*, "Minimum and maximum radial gravity anomaly (mGals) = ", &
       minval(grav_grid(1:nlat,1:nlong)), maxval(grav_grid(1:nlat,1:nlong))

  open(12,file=outfile)

  do i=1, nlat
     d = dble(i-1)*interval_m
     write(12,*) d , ',' , grav_grid(i,1)
  enddo
  close(12)


  deallocate (topo_grid)
  deallocate (grav_grid)

END SUBROUTINE FFC_GRAVITY


