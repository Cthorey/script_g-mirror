#!/usr/bin/env python
###################################
# Load Dataset

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import sys
import Data_Tool as CDT # Librairie qui contient toutes les fonction ds ls fichier.py qui commence par data
import FFC_Gravity_2 # Librairie faire a partir du fichier fortran
import Fig_Tool as FST
from Data_Load import *
from Histo_Mean import *
from Depth import *
import Data_Obs_Tool as DOT
from Plot import *

################################################
# Chose a renseigner
Plot.Path_Save = '/Users/thorey/Documents/These/Submission/Conference/GRAIL_MEETING/'
Root = DOT.Init('thorey')
Crater_Plot = ['F_Taruntius','F_Vitello'] # Si on veut tester le script, et pour pas que l'operation se repete sur plein de crater. Mettre le nom du crater

rho_contrast = [1]
lambda_filter = [80]

Path_Save = Root+'Gravi_GRAIL/Traitement/GRAIL_SYNTH/' # Path ou il va enregistrer le dataframe final
 
###################################
# Load Dataset

mask_C = ['C_C_FFC']
mask_F =['F_ALL']
Load_G.Root = Root+'Gravi_GRAIL/Traitement/Data_Observation/'
obj = Depth(mask_C,mask_F)
obj.Lola = 'LDEM_64_'
obj2 = Plot()

mask_F = obj.m_table_thickness()
#F = pd.concat([mask_F['F_MARIA'],mask_F['F_H_SPA']])
#F = pd.concat([F,mask_F['F_SPA'][~mask_F['F_SPA'].Name.isin(mask_F['F_MARIA'].Name)]])
F = pd.concat(mask_F.values())
F = F[F.Name.isin(Crater_Plot)]
F.index = np.arange(0,len(F),1)

###################################
# Selection des cratere a plotter

F = F[F.Name.isin(Crater_Plot)] # Selection des crater qu'on veut ploter
F.index=np.arange(0,len(F),1)
F.Thickness = 1.1     # A mettre si on veut faire les graph synthetic maximum pour les deux types de FFCs
F.Diameter = 56.6      # A mettre si on veut faire les graph synthetic maximum pour les deux types de FFCs

################################################
# Thickness intrusion

Path_I = Root+'Gravi_GRAIL/Traitement/GRAIL_SYNTH/Input/' # Input pour FFC_Gravity. Place ou il va chercher les profil d'epaisseur
Path_O = Root+'Gravi_GRAIL/Traitement/GRAIL_SYNTH/Output/' # Input pour FFC_Gravity. Place ou il enregistre les porfil de gravi
    
Class1 = ['1','3','5','6']
Class2 = ['2','4']
    
Stat = []

for lamb in lambda_filter :
    for rho in rho_contrast :
        col = 'S_mean_'+str(rho)+'_'+str(lamb)
        print 'helloooooooooooooo',col
        Stat = []
        for i,elt in enumerate(F.Name):
            obj2.fig_name = 'f_'+elt
            fig,ax = obj2.m_figure()
            Input = Path_O+elt+'.txt' #Path_input pour FFC_Gravity
            Output = Path_O+elt+'_grav.dat' #Path_output pour FFC_Gravity
    
            if str(F.Class[i]) in Class1:
                profil = Path_I+'Profil_Grav_Min.txt' # Si Ds la classe flat floor, va chercher le profil associe a flat floor
            elif str(F.Class[i]) in Class2:
                profil = Path_I + 'Profil_El_Min.txt' # Si Ds la classe convex, va chercher le profil associe a convex
        
            prof=pd.read_csv(profil,header=None,names=['r','h']) # Lit le profil Input d'epaisseur normalise selectionner (convex ou flat floor)
            #prof['r']=prof.r*F.Diameter[i]/2.0*1000 # Multiplie par le rayon du crater et par 1000 ( doit etre en metre)
            #prof['h']=prof.h*F.Thick_C[i]*1000 # Multiplier par l'epaisseur de l'intrusion et par 1000 ( en m)
            prof['r']=prof.r*56.2/2.0*1000 # Multiplie par le rayon du crater et par 1000 ( doit etre en metre)
            prof['h']=prof.h*1.1*1000 # Multiplie par le rayon du crater et par 1000 ( doit etre en metre)
            prof.to_csv(Input,sep=',',index=None,header=None) # Ecrie dans Input le fichier d'epaisseur conforme et que va prendre FFC_Gravity.f95
            FFC_Gravity_2.ffc_gravity(Input,Output,rho,lamb) # Script FFC_Gravity qui calcule le profil de gravi. rho correspond a la diffenrece de densite en tre le magma et l'encaissant
            Synth=pd.read_csv(Output,sep=',',header=None,names=['r','G']) # Dataframe ou l'on recupere le profil gravi, output de FFC_Gravity
            Synth = Synth[Synth.r<F.Diameter[i]*1000/2.0]
            FST.Axi_fig(Synth.r,Synth.G,F.Diameter[i]/2.0,ax,[Synth.G.min(),Synth.G.max()])
            Dr = Synth.r[1]/2.0
            Mean = CDT.Num_Int_2(Synth[Synth.r<F.Diameter[i]*1000/2.0],F.Diameter[i]*1000/2.0,Dr)
            Stat.append(Mean)
            obj2.fig_b = True
            ax.set_title(r'$\delta_{g}^s=')
            obj2.m_save_fig(fig)
        map(os.remove,[Path_O+elt for elt in os.listdir(Path_O)])
        F[col] = Stat

F.to_csv(Path_Save+str(obj.Lola)+str(obj.Grav)+'_FFC_3.txt',sep=',',index=None)
            


