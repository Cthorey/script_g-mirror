program LambertCorners
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
!	This program is used to determine the bottom-left and upper-right 
!	coordinates for a square Lambert Azimuthal equal area projection
!	to be used in GMT (-JA).
!
!	Written by Mark Wieczorek
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	implicit none
	
	real*8 ::	radius, pi, x, y, bot, lat0, long0, rho, c, lat, lon
	real*8, external :: kp
	character*120 ::	lons, lats, gmt
	
	pi = acos(-1.0d0)
	
	do
	
	gmt = "-R"
	
	print*, "Radius of circle to be inscribe in square projection (degrees) ="
	read(*,*) radius
	
	radius = radius * pi / 180.0d0
	
	print*, "Center of projection, latitude (degrees) = "
	read(*,*) lat0
	print*, "Center of projection, longitude (degrees) = "
	read(*,*) long0
	
	lat0 = lat0*pi/180.0d0
	long0 = long0*pi/180.0d0
	
	bot = kp(lat0-radius, long0, lat0, long0)
	bot = bot * ( cos(lat0)*sin(lat0-radius) - sin(lat0)*cos(lat0-radius) )
	
	x = bot
	y = bot
	
	rho = sqrt(x**2 + y**2)
	c = 2.0d0 * asin(rho/2.0d0)
	
	lat = asin(cos(c)*sin(lat0)  + y*sin(c)*cos(lat0)/rho ) * 180.0d0 / pi
	
	lon = long0  + atan2(x*sin(c), rho*cos(lat0)*cos(c) - y*sin(lat0)*sin(c))
	lon = lon * 180.0d0 / pi
	
	print*, "Lower left coordinates (lat, long) = ", lat, lon
	
	write(lons,"(f9.4)")  Lon
	write(lats,"(f9.4)")  lat
	lons = adjustl(lons)
	lats = adjustl(lats)
	gmt = trim(gmt)//trim(lons)//"/"//trim(lats)//"/"
	
	x = -bot
	y = -bot
	
	rho = sqrt(x**2 + y**2)
	c = 2.0d0 * asin(rho/2.0d0)
	
	lat = asin(cos(c)*sin(lat0)  + y*sin(c)*cos(lat0)/rho ) * 180.0d0 / pi
	
	lon = long0  + atan2(x*sin(c), rho*cos(lat0)*cos(c) - y*sin(lat0)*sin(c))
	lon = lon * 180.0d0 / pi

	print*, "Upper right coordinates (lat, long) = ", lat, lon
	
	write(lons,"(f9.4)")  Lon
	write(lats,"(f9.4)")  lat
	lons = adjustl(lons)
	lats = adjustl(lats)
	
	gmt = trim(gmt)//trim(lons)//"/"//trim(lats)//"r"
	
	print*, "GMT string = ", gmt
	

	enddo
	
end program

	
real*8 function kp(lat, lon, lat0, long0)
	implicit none
	real*8 :: lat, lon, lat0, long0
		
	kp = 1.0d0 + sin(lat0)*sin(lat) + cos(lat0)*cos(lat)*cos(lon-long0)
	
	kp = sqrt(2.0d0 / kp)
		
end function kp
	