import Fig_Tool as FST
import numpy as np
import pandas as pd
import matplotlib.pylab as plt
from matplotlib.patches import Polygon
import os

class Master_Table(object):
    
    Model = np.arange(40,120,20)
    Root = '/Users/thorey/Documents/These/Projet/FFC/Gravi_GRAIL/Traitement/Data_Observation/'

    def __init(self):
        self.Lola = 'it_Lola'
        self.Grav = 'JGGRAIL_900C9A_CrustAnom_'
        self.value = 'Local_Mean'
        self.crit_std = 1000

    def tab(self):
        Iter = [self.Grav+str(elt) for elt in Model]
        tab = dict.fromkeys(Iter)
        colormap = plt.cm.gist_ncar
        
        for i,mod in enumerate(Iter):

            C = pd.read_csv(Root+Lola+Grav+str(Model[i])+'_Crater_Head.txt')
            F = pd.read_csv(Root+Lola+Grav+str(Model[i])+'_FFC.txt')
            C = C[C.GE_std<self.crit_std]
            
            index = {'Size':len,'Mean':np.mean,'SE':SE,'SD':np.std} #Index creation
            table = pd.DataFrame({'Index':index.keys()})
            table['Model']=mod
            
            # Definition des mask
            ALL = C.Mask_Not_FFC
            SPA = ~C.Mask_SPA_Out
            HIGH = C.Mask_Highlands
            MARIA = ~C.Mask_Highlands
            H_SPA = C.Mask_Highlands & C.Mask_SPA_Out
            C_FFC = C.Mask_C_FFC & C.Mask_Highlands & C.Mask_SPA_Out
            mask_C={'C_ALL':ALL,'C_SPA':SPA,'C_HIGH':HIGH,'C_MARIA':MARIA,'C_H_SPA':H_SPA,'C_C_FFC':C_FFC}

            table_C=[]
            table_C = pd.DataFrame([[func(C[elt].Local_Mean) for elt in mask_C.itervalues()] for func in index.itervalues()],columns=mask_C.keys(),index=index.keys())
            table_C['Index']=index.keys()

        # Definition des mask FFC

            ALL = F == F
            HIGH = F.Maria == 'Highland'
            MARIA = F.Maria != 'Highland'
            mask_F={'F_All':ALL,'F_Highlands':HIGH,'F_Maria':MARIA}
    
            table_F=[]
            table_F = pd.DataFrame([[func(F[elt].Local_Mean) for elt in mask_F.itervalues()] for func in index.itervalues()],columns=mask_F.keys(),index=index.keys())
            table_F['Index']=index.keys()
    
            table = table.merge(table_C,on='Index').merge(table_F,on='Index')
            table.index=table.Index
            table=table.drop('Index',1)
            tab[mod]=table

        Tab = pd.concat([tab[Iter[elt]] for elt in np.arange(0,4,1)])
        return Tab
        
    
class Taper(object):
    Model = np.arange(40,120,20)
    Root = '/Users/thorey/Documents/These/Projet/FFC/Gravi_GRAIL/Traitement/Data_Observation/'
    Lola = 'it_Lola_'
    Grav = 'JGGRAIL_900C9A_CrustAnom_'
    Iter = [Grav+str(elt) for elt in Model]
    tab = dict.fromkeys(Iter)
    colormap = plt.cm.gist_ncar
    
    
    def __init__(self,C_Select):
        self.C_Select = C_Select
        self.Tab = None
        self.x_label = 'Value of the center cosine taper $\lambda$'
        self.y_label = r'Crust anomaly $\delta_g$'
        self.font = 18
        
    def degree(self,name):
        F = pd.read_csv(Taper.Root+Taper.Lola+Taper.Grav+str(Taper.Model[0])+'_FFC.txt')
        return 'Degree min that contribute to the signal = {0:3.0f}'.format(np.float(2*np.pi*1734/(2*F[F.Name == name].Diameter)))
        
    def Table(self):
        for i,mod in enumerate(Taper.Iter):
            F = pd.read_csv(Taper.Root+Taper.Lola+Taper.Grav+str(Taper.Model[i])+'_FFC.txt')
            F = F[['Name','Diameter','GI_mean']][F.Name.isin(self.C_Select)]
            F['Lambda']=Taper.Model[i]
            Taper.tab[mod]=F
        self.Tab = pd.concat([Taper.tab[Taper.Iter[elt]] for elt in np.arange(0,4,1)])
        
        return self.Tab
        
        
    def plot1(self):
        self.Table()
        Tab =self.Tab
        N=len(self.C_Select)
        c=[colormap(n) for n in np.linspace(0, 0.9,N)]
        fig, ax = plt.subplots()
        for i,elt in enumerate(self.C_Select):
            ax.scatter(Tab[Tab.Name==elt].Lambda,Tab[Tab.Name==elt].GI_mean,label=elt,color = c[i])
        ax.set_xlabel(self.x_label,size=self.font)
        ax.set_ylabel(self.y_label,size=self.font)
        FST.legd_Scatter(ax,'upper right',self.font)
        FST.tick(ax,self.font)
    


class SH(object):
    def __init__(self,range):
        self.wavelength = range
        self.table = pd.DataFrame(self.wavelength,columns =['Lambda'])
        self.a = 26
        self.b = 900
        self.y_lim = 0,400
    
    def spherical(self):
        self.table['l']=2*np.pi*1734/self.table.Lambda
        

    def plot(self):
        a=self.a
        b=self.b
        fig, ax = plt.subplots()
        self.spherical()
        plt.scatter(self.table.l,self.table.Lambda,color='r')
        plt.xlim(0,900)
        plt.ylim(self.y_lim)
        plt.ylabel(r'Features wavelength (km)')
        plt.xlabel(r'Spherical hamronic coefficient $l$')
        
        # Make the shaded region
        ix = np.linspace(a, b)
        iy = 2*np.pi*1734/ix
        verts = [(a, 0)] + list(zip(ix, iy)) + [(b, 0)]
        poly = Polygon(verts, facecolor='b', edgecolor='b',alpha=0.3)
        ax.add_patch(poly)

class Taper_Synth(Taper):
    
    def __init__(self,C_Select,rho):
        Taper.__init__(self,C_Select)
        self.rho = '150'
        self.Tab = None

    def table(self):
        Root_s = '/Users/thorey/Documents/These/Projet/FFC/Gravi_GRAIL/Traitement/Stat_FFC_Synth/Clavius/'
        file = [elt for elt in os.listdir(Root_s) if elt.split('_')[-1].split('.')[0] == self.rho]
        Iter = [elt for elt in file]
        tab = dict.fromkeys(Iter)
        for i,elt in enumerate(file):
            F = pd.read_csv(Root_s+elt)
            F = F[['Name','Diameter','Mean_Anomaly']][F.Name.isin(self.C_Select)]
            F['Lambda']=elt.split('_')[3]
            tab[elt]=F
        self.Tab = pd.concat([tab[Iter[elt]] for elt in np.arange(0,4,1)])
        
        return self.Tab.sort(columns='Lambda')

    def plot1(self):
        self.table()
        Tab = self.Tab
        N=len(self.C_Select)
        c=[Taper.colormap(n) for n in np.linspace(0, 0.9,N)]
        fig, ax = plt.subplots()
        for i,elt in enumerate(self.C_Select):
            ax.scatter(Tab[Tab.Name==elt].Lambda.tolist(),Tab[Tab.Name==elt].Mean_Anomaly.tolist(),label=elt,color = c[i])
        ax.set_xlabel(self.x_label,size=self.font)
        ax.set_ylabel(self.y_label,size=self.font)
        FST.legd_Scatter(ax,'upper right',self.font)
        FST.tick(ax,self.font)
        
