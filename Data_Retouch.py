#!/usr/bin/env python
################################################
# Script definition
# OUTPUT : Fichier qui ecrit les caracteristique relatif a la carte de gravi pour chaque crater

# DESCRIPTION : On prend la carte indiquer dans Path et on va en tirer les information d'interet pour chauqe crater. 
# Les information sont 'I_mod','I_min','R_mod','R_max'. La definition est indiquer dans ce papier. 
# http://www.lpi.usra.edu/meetings/lpsc2013/pdf/1309.pdf. En gros I refere to Int. Int est un mask de rayon le rayon du crater.
# R refere ro Rim, Rim est une courronne centrer sur le centre du crater, de rayon interieru 0.98R et de rayon ext 1.05R.

################################################
# Library a utiliser

import numpy as np
import pandas as pd
import Variable as v
import os
import matplotlib.pylab as plt
import sys
import Data_Tool as CDT
import Data_Obs_Tool as DOT
import window
from Data_Load import *
from Histo_Mean import *

################################################
# Chose a renseigner

Root = DOT.Init('thorey')
mode = ['10','80']
Path_Save = Root+'Gravi_GRAIL/Traitement/GRAIL_SYNTH/' # Path ou il va enregistrer le dataframe final

mask_C = ['C_H_SPA_O']
mask_F = ['F_H_SPA_O']
obj = HistoMean(mask_C,mask_F,['10'])
Load_G.Root_F = '/Users/thorey/Documents/These/Projet/FFC/Gravi_GRAIL/Traitement/GRAIL_SYNTH/'
Load_G.Range = 20,184
obj.Lola = 'LDEM_64_'
F = obj.m_load_F()
obj.bin = 20
obj.bin_min = 25
obj.col = 'Local_Mean_'
std= {'10':684,'80':701}

for mod in mode:
    obj.model = [mod]
    F['Coeff_'+str(mod)] = (F['S_mean_250_'+mod] -F['S_mean_100_'+mod])/(250.0-100.0)
    tab = obj.m_table()
    C_h = tab[tab.Loc == 'C_H_SPA_O']

    def my_test(a,b):
        for i,l in enumerate(tab.Diameter):
            if (a<=l+obj.bin/2.0) & (a>=l-obj.bin/2.0):
                return b-C_h['mean_'+mod][i]

    F['N_Local_Mean_'+mod] = F.apply(lambda row: my_test(row['Diameter'],row['Local_Mean_'+mod]), axis=1)
    F['S_mean_'+str(int(F['N_Local_Mean_'+mod].mean()/F['Coeff_'+mod].mean()))+'_'+mod]= F['Coeff_'+str(mod)]*(F['N_Local_Mean_'+mod].mean()/F['Coeff_'+mod].mean())
    F['S_mean_'+str(std[mod])+'_'+mod]=F['Coeff_'+str(mod)]*std[mod]
    F['Delta_Rho_'+mod] = F['N_Local_Mean_'+mod]/F['Coeff_'+str(mod)]
F.to_csv(Path_Save+str(obj.Lola)+str(obj.Grav)+'_FFC_R.txt',sep=',',index=None)
    
