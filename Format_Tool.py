
def Maria_Location(Root):

    # Return two vector, one containing the lattitude for the maria, and another containiung
    # the longitude for the maria
    
    import numpy as np

    source = Root+'Gravi_GRAIL/Grail_Dataset/USGS_Mare_Basalts/USGS_mare_basalts.gmt'

    Lat = []
    Long = []
    with open(source, 'r') as file:
        for line in file :
            if (len(line.strip().split(' ')) < 2) or (line.strip().split(' ')[0] == '#'):
                pass
            else :
                Long.append(np.float(line.strip().split(' ')[0]))
                Lat.append(np.float(line.strip().split(' ')[1]))
    return Long, Lat

def Maria_Location_2(Root):

    # Return two vector, one containing the lattitude for the maria, and another containiung
    # the longitude for the maria
    
    import numpy as np

    source = Root+'Gravi_GRAIL/Grail_Dataset/USGS_Mare_Basalts/USGS_mare_basalts_2.gmt'

    Lat = []
    Long = []
    Bool= False
    with open(source, 'r') as file:
        for line in file :
            if line.strip().split(' ')[1] == '-G0' :
                Bool = True
            elif line.strip().split(' ')[1] == '-G255' :
                Bool = False

            if Bool:
                if line.strip().split(' ')[0] == '>':
                    pass
                else:
                    Long.append(np.float(line.strip().split(' ')[0]))
                    Lat.append(np.float(line.strip().split(' ')[1]))
    return Long, Lat

def Maria_Location_3(Root):

    # Return two vector, one containing the lattitude for the maria, and another containiung
    # the longitude for the maria
    
    import numpy as np

    source = Root+'Gravi_GRAIL/Grail_Dataset/USGS_Mare_Basalts/USGS_mare_basalts_2.gmt'

    Lat = []
    Long = []
    Bool= False
    List=[]
    l=[]
    with open(source, 'r') as file:
        for line in file :
            if line.strip().split(' ')[1] == '-G0' :
                Bool = True
                List.append(l)
                l=[]
            elif line.strip().split(' ')[1] == '-G255' :
                Bool = False

            if Bool:
                if line.strip().split(' ')[0] == '>':
                    pass
                else:
                    Long = (np.float(line.strip().split(' ')[0]))
                    Lat = (np.float(line.strip().split(' ')[1]))
                    l.append((Long,Lat))
    return List

def haversine(lon1, lat1, lon2, lat2):

    import numpy as np
    """
    Calculate the great circle distance between two points 
    on the earth (specified in decimal degrees). The angle
    should be in rad
    """
    # haversine formula 
    dlon = lon2 - lon1 
    dlat = lat2 - lat1 
    a = np.sin(dlat/2)**2 + np.cos(lat1) * np.cos(lat2) * np.sin(dlon/2)**2
    c = 2 * np.arcsin(np.sqrt(a)) 

    # 6367 km is the radius of the Earth
    km = 1734 * c
    return km

def Is_Maria(F):

    import numpy as np
    import pandas as pd

    Long_Maria,Lat_Maria = FST.Maria_Location()
    mask=[]
    Maria_loc = pd.DataFrame(Long_Maria,columns = ['Long']).join(pd.DataFrame(Lat_Maria,columns=['Lat']))
    for i,elt in enumerate(F.Lat):
        a= haversine(Maria_loc.Long*np.pi/180.0,Maria_loc.Lat*np.pi/180.0,F.Long[i]*np.pi/180.0,F.Lat[i]*np.pi/180.0).min()
        if a<10 :
            mask.append(False)
        else :
            mask.append(True)

    return F[mask]


    
def Maria_Location_Remove():

    # Return two vector, one containing the lattitude for the maria, and another containiung
    # the longitude for the maria
    
    import numpy as np
    import sys

    source = '/Users/thorey/Documents/These/Projet/FFC/Gravi_GRAIL/Grail_Dataset/USGS_Mare_Basalts/USGS_mare_basalts_2.gmt'
    output = '/Users/thorey/Documents/These/Projet/FFC/Gravi_GRAIL/Grail_Dataset/USGS_Mare_Basalts/USGS_mare_basalts_3.gmt'
    Lat = []
    Long = []
    with open(source, 'r') as file:
        with open(output ,'wr+') as f:
            for line in file:
                if line.strip(' ')[0] == '>':
                    if line.strip(' ')[4] == '0':
                        bool = True
                        print 'R'
                    elif line.strip(' ')[4] == '2':
                        bool = False
                        print 'F'
                print bool
                if bool :
                    f.write(line)
            


    
def point_in_poly(vertices, x, y):
    
    from matplotlib.path import Path as mpPath
    import numpy as np
    path =mpPath(vertices)
    path = path.to_polygons()
    point = (x,y)
    if path.contains_point(point) == 0:
        return True
    elif path.contains_point(point) == 1:
        return False
