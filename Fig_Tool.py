def Load_DataFrame(C_lola,C_gravi,F_lola,F_gravi,F_synth):

    import pandas as pd
    import sys
    import numpy as np

    Liste_Lola=['Name','Lat','Long','Class','Diameter','Location','L_Max_Bins_Int','L_Max_Int','L_Min_Int','L_Mean_Int','L_Std_Int','L_StdErr_Int','L_Max_Bins_Ext','L_Max_Ext','L_Min_Ext','L_Mean_Ext','L_Std_Ext','L_StdErr_Ext']
    Liste_Gravi=['Name','Lat','Long','Class','Diameter','Location','G_Max_Bins_Int','G_Max_Int','G_Min_Int','G_Mean_Int','G_Std_Int','G_StdErr_Int','G_Max_Bins_Ext','G_Max_Ext','G_Min_Ext','G_Mean_Ext','G_Std_Ext','G_StdErr_Ext']
    Liste_Synth=['Name','Lat','Long','Class','Diameter','Location','Thickness','S_Max_Bins_Int','S_Max_Int','S_Min_Int','S_Mean_Int','S_Std_Int','S_StdErr_Int','S_Max_Bins_Ext','S_Max_Ext','S_Min_Ext','S_Mean_Ext','S_Std_Ext','S_StdErr_Ext']
    Liste3=['G_Max_Bins_Int','G_Max_Int','G_Min_Int','G_Mean_Int','G_Std_Int','G_StdErr_Int','G_Max_Bins_Ext','G_Max_Ext','G_Min_Ext','G_Mean_Ext','G_Std_Ext','G_StdErr_Ext']
    Liste4=['Thickness','S_Max_Bins_Int','S_Max_Int','S_Min_Int','S_Mean_Int','S_Std_Int','S_StdErr_Int','S_Max_Bins_Ext','S_Max_Ext','S_Min_Ext','S_Mean_Ext','S_Std_Ext','S_StdErr_Ext']

    F_Lola = pd.read_csv(F_lola)
    C_Lola = pd.read_csv(C_lola)
    F_Gravi =  pd.read_csv(F_gravi)
    C_Gravi = pd.read_csv(C_gravi)
    F_Synth = pd.read_csv(F_synth)

    F_Lola=F_Lola.drop_duplicates(cols='Name', take_last=True)
    F_Gravi=F_Gravi.drop_duplicates(cols='Name', take_last=True)
    C_Gravi=C_Gravi.drop_duplicates(cols='Name', take_last=True)
    C_Lola=C_Lola.drop_duplicates(cols='Name', take_last=True)
    F_Synth=F_Synth.drop_duplicates(cols='Name', take_last=True)

    F_Lola.columns = Liste_Lola
    C_Lola.columns = Liste_Lola
    F_Gravi.columns = Liste_Gravi
    C_Gravi.columns = Liste_Gravi
    F_Synth.columns =  Liste_Synth
    
    F_Lola = F_Lola[F_Lola.Name.isin([elt for elt in F_Lola.Name if elt in np.array(F_Synth.Name)])]
    F_Gravi = F_Gravi[F_Gravi.Name.isin([elt for elt in F_Lola.Name if elt in np.array(F_Synth.Name)])]
    F_Synth = F_Synth[F_Synth.Name.isin([elt for elt in F_Lola.Name if elt in np.array(F_Synth.Name)])]
    
    C_Lola=C_Lola[C_Lola.Name.isin([elt for elt in C_Lola.Name if elt in np.array(C_Gravi.Name)])]
    C_Gravi=C_Gravi[C_Gravi.Name.isin([elt for elt in C_Lola.Name if elt in np.array(C_Gravi.Name)])]
    F_Lola=F_Lola.sort('Name')
    C_Lola=C_Lola.sort('Name')
    F_Gravi=F_Gravi.sort('Name')
    C_Gravi=C_Gravi.sort('Name')
    F_Synth=F_Synth.sort('Name')

    if F_Lola.Name.tolist()!=F_Gravi.Name.tolist() or len(F_Lola)!=len(F_Gravi):
        raise ValueError('Probleme de merging Dataframe')
        sys.exit()
    if F_Lola.Name.tolist()!=F_Synth.Name.tolist() or len(F_Lola)!=len(F_Synth):
        raise ValueError('Probleme de merging Dataframe')
        sys.exit()
    if C_Gravi.Name.tolist()!=C_Lola.Name.tolist() or len(C_Lola)!=len(C_Gravi):
        raise ValueError('Probleme de merging Dataframe')
        sys.exit()       

    C_Gravi.index=np.arange(0,len(C_Gravi),1)
    F_Gravi.index=np.arange(0,len(F_Gravi),1)
    C_Lola.index=np.arange(0,len(C_Lola),1)
    F_Lola.index=np.arange(0,len(F_Lola),1)
    F_Synth.index=np.arange(0,len(F_Synth),1)
     
    C_Gravi=C_Gravi[Liste3]
    F_Gravi=F_Gravi[Liste3]
    F_Synth=F_Synth[Liste4]
    
    C=C_Lola.join(C_Gravi)
    F=F_Lola.join(F_Gravi).join(F_Synth)

    if len(F)!=len(F_Lola) or len(F)!=len(F_Gravi):
        raise ValueError('Probleme de merging Dataframe')
        sys.exit()
        
    if len(F)!=len(F_Synth) or len(F)!=len(F_Gravi):
        raise ValueError('Probleme de merging Dataframe')
        sys.exit()
        
    if len(C)!=len(C_Lola) or len(C)!=len(C_Gravi):
        raise ValueError('Probleme de merging Dataframe')
        sys.exit()
        
    return C,F


def font(family,weight,size):
    # Fonction qui definit les propriete up-level de la figure
    import matplotlib
    
    font = {'family' : family,
            'weight' : weight,
            'size'   : size}
    matplotlib.rc('font', **font)
    

def legd(ax,place,size) :
     # given the handle, return a legend
    legend = ax.legend(loc=place, frameon=False)
    for label in legend.get_texts():
        label.set_fontsize(size)
    for label in legend.get_lines():
        label.set_linewidth(3)  # the legend line width

def legd_Scatter(ax,place,size) :
     # given the handle, return a legend
    legend = ax.legend(loc=place, frameon=False,scatterpoints = 1)
    for label in legend.get_texts():
        label.set_fontsize(size)
    for label in legend.get_lines():
        label.set_linewidth(3)  # the legend line width

def cbar_p(mesh,axs,label,tick):
    # given the mesh, the handle, the label and the tick, return a colorbar
    
    from mpl_toolkits.axes_grid1 import make_axes_locatable
    import matplotlib.pylab as plt
    
    # create an axes on the right side of ax. The width of cax will be 3%
    # of ax and the padding between cax and ax will be fixed at 0.1 inch.
    divider = make_axes_locatable(axs)
    cax = divider.append_axes("right", size="5%", pad="10%")
    cbar = plt.colorbar(mesh,ax=axs,cax=cax)
    cbar.set_clim(vmin=-60,vmax=60)
    cbar.ax.set_ylabel(label)
    cbar.set_ticks(tick)
    cbar.set_ticklabels(tick)

    return cbar

def tick(ax,size) :
    #Change le tick des axes pour un handle donne
    for tick in ax.xaxis.get_major_ticks():
        tick.label.set_fontsize(size)
    for tick in ax.yaxis.get_major_ticks():
        tick.label.set_fontsize(size)
        
def make_colormap(seq):

    import numpy as np
    import matplotlib.pyplot as plt
    import matplotlib.colors as mcolors
    """Return a LinearSegmentedColormap
    seq: a sequence of floats and RGB-tuples. The floats should be increasing
    and in the interval (0,1).
    """
    seq = [(None,) * 3, 0.0] + list(seq) + [1.0, (None,) * 3]
    cdict = {'red': [], 'green': [], 'blue': []}
    for i, item in enumerate(seq):
        if isinstance(item, float):
            r1, g1, b1 = seq[i - 1]
            r2, g2, b2 = seq[i + 1]
            cdict['red'].append([item, r1, r2])
            cdict['green'].append([item, g1, g2])
            cdict['blue'].append([item, b1, b2])
    return mcolors.LinearSegmentedColormap('CustomMap', cdict)



def Sample_Grd(Path_LOLA,Carte_Final):
    
    import numpy as np
    from scipy.io import netcdf_file as netcdf
    from Scientific.IO.NetCDF import NetCDFFile
    import scipy as sp
    import pandas as pd
    import sys
    import os

    file=os.listdir(Path_LOLA)
    grdfile=Path_LOLA+'/'+Carte_Final
    
    x=pd.DataFrame(np.copy(NetCDFFile(grdfile,'r').variables['lon'][::],'f4'),columns=['long'],dtype='f4')
    y=pd.DataFrame(np.copy(NetCDFFile(grdfile,'r').variables['lat'][::],'f4'),columns=['lat'],dtype='f4')
    z=pd.DataFrame(np.copy(NetCDFFile(grdfile,'r').variables['z'][::],'f4'),columns=x.index,index=y.index,dtype='f4')

    return(x,y,z)

def Select_Zoom(F,i,x,y,z,Radius):
    
    import window

    a=float(F.Lat[i])
    c=float(F.Long[i])
    Fen=str(window.lambert((Radius*360/(10917.0)),a,c)[4])
    Fen=[elt for elt in Fen.split() if elt!=''][0]
    Fen=Fen[1:-1].split('/')
    long_Min=float(Fen[0])
    long_Max=float(Fen[2])
    if long_Min<0 :
        long_Min=360+long_Min
    if long_Max<0:
        long_Max=360+long_Max

    lat_Min=float(Fen[1])
    lat_Max=float(Fen[3])
    data=z.iloc[y[(y.lat>lat_Min) & (y.lat<lat_Max)].index.tolist(),x[(x.long>long_Min) & (x.long<long_Max)].index.tolist()]
    
    return a,c,Fen,x[(x.long>long_Min) & (x.long<long_Max)],y[(y.lat>lat_Min) & (y.lat<lat_Max)],data

def Select_Zoom_2(x,y,z,size,lat_0,long_0):
    
    import window
    import numpy as np

    Fen=str(window.lambert((float(size)*360/(10917.0)),lat_0,long_0)[4])
    Fen=[elt for elt in Fen.split() if elt!=''][0]
    Fen=Fen[1:-1].split('/')
    long_Min=float(Fen[0])
    long_Max=float(Fen[2])
    if long_Min<0 :
        long_Min=360+long_Min
    if long_Max<0:
        long_Max=360+long_Max

    lat_Min=float(Fen[1])
    lat_Max=float(Fen[3])
    data_zoom_x=x[(x.long>long_Min) & (x.long<long_Max)]
    data_zoom_y=y[(y.lat>lat_Min) & (y.lat<lat_Max)]
    data_zoom=z.iloc[data_zoom_y.index.tolist(),data_zoom_x.index.tolist()]

    return Fen,data_zoom_x,data_zoom_y,np.array(data_zoom)

def Circular_Mask(name,data):

    import numpy as np
    import matplotlib.pyplot as plt
    import sys

    data=np.array(data)
    radius_a=np.floor((data.shape[1])/2.) #Nombre de colonnes diviser par 2
    radius_b=np.floor((data.shape[0])/2.) # Nombre de ligne diviser par 2
    y,x = np.ogrid[-radius_b:radius_b+1, -radius_a:radius_a+1]
    mask = (x/radius_a)**2 + (y/radius_b)**2 <= 1
    if  data.shape[1] % 2 == 0 :
        mask = np.delete(mask,radius_a,1)
    if  data.shape[0] % 2 == 0 :
        mask = np.delete(mask,radius_b,0)

    data[~mask]=-60
    
    return data

def Circular_Mask_2(x,y,z,diametre,lat_0,long_0):
    # fait un mask plus precis que Circular_Mask
    import numpy as np
    import matplotlib.pyplot as plt
    import sys
    import window

    # Extrait la fenetre qui correspond a cote = diametre
    Fen=str(window.lambert((diametre*360/(2.0*10917.0)),lat_0,long_0)[4])
    Fen=[elt for elt in Fen.split() if elt!=''][0]
    Fen=Fen[1:-1].split('/')
    long_Min=float(Fen[0])
    long_Max=float(Fen[2])
    if long_Min<0 :
        long_Min=360+long_Min
    if long_Max<0:
        long_Max=360+long_Max
    
    if long_0<0 :
        long_0=360+long_0


    lat_Min=float(Fen[1])
    lat_Max=float(Fen[3])

    d_x=long_Max-long_0
    d_y=lat_Max-lat_0
    

    x=np.array(x)
    y=np.array(y)
    z=np.array(z)
    
    X,Y = np.meshgrid(x,y)
    mask = ((X-long_0)/d_x)**2+((Y-lat_0)/d_y)**2 <=1
    z[~mask]=0
    
    return z

def Axi_fig(r,G,d,ax2,clim):

    from mpl_toolkits.axes_grid1 import make_axes_locatable
    import matplotlib.pylab as plt
    import pandas as pd
    import numpy as np
    import matplotlib.mlab as mlab
    import matplotlib.gridspec as gridspec
    
    theta=np.arange(-np.pi,np.pi,0.01)
    X=np.array([[f*g for f in r] for g in np.cos(theta)])
    Y=np.array([[f*g for f in r] for g in np.sin(theta)])
    Z=np.array([[f for f in G] for g in np.sin(theta)])
    
    #######FIGURE ########

    mesh=ax2.pcolormesh(X/1000,Y/1000,Z,shading='gouraud')
    mesh.set_rasterized(True)
    cbar_p(mesh,ax2,'Gravity Anomaly (mGals)',np.arange(0,0.06,0.01))
    mesh.set_clim([0,0.051])
    #mesh.set_rasterized(True)
    cir1=plt.Circle((0,0),d,color='r',fc='none',linestyle='dashed',linewidth=4)
    ax2.add_patch(cir1)
    ax2.set_aspect('equal')
    tick(ax2,18)


def Select_Synth(F,i,elt,Path_I,Path_O):

    import pandas as pd
    import numpy as np
    import FFC_Gravity
    import os

    Class1=['1','3','5','6']
    Class2=['2','4']
    Input=Path_O+elt+'.txt'
    Output=Path_O+elt+'_grav.dat'

    
    if str(F.Class[i]) in Class1:
        profil=Path_I+'Profil_Grav_Min.txt'
    elif str(F.Class[i]) in Class2:
        profil=Path_I+'Profil_El_Min.txt'

    if os.path.exists(Output)==False :
        prof=pd.read_csv(profil,header=None,names=['r','h'])
        prof['r']=prof.r*F.Diameter[i]/2.0*1000
        prof['h']=prof.h*F.Thickness[i]*1000
        prof.to_csv(Input,sep=',',index=None,header=None)
        FFC_Gravity.ffc_gravity(Input,Output)
        Synth=pd.read_csv(Output,sep=',',header=None,names=['r','G'])
    else :
        Synth=pd.read_csv(Output,sep=',',header=None,names=['r','G'])

    return(Synth)


def sample_wr(population, k):

    import random
    import itertools
    import pandas
    import sys
    "Chooses k random elements (with replacement) from a population"
    n = len(population)
    _random, _int = random.random, int  # speed hack
    indice = [_int(_random() * n) for i in itertools.repeat(None, k)]
    print indice
    sys.exit()
    return population.loc[indice]

def Couronne_Mask_2(x,y,z,diametre_int,diametre_ext,lat_0,long_0):
    # fait un mask en forme de courronne
    import numpy as np
    import matplotlib.pyplot as plt
    import sys
    import window

    # Extrait la fenetre qui correspond a cote = diametre
    Fen_Int=str(window.lambert((diametre_int*360/(2.0*10917.0)),lat_0,long_0)[4])
    Fen_Ext=str(window.lambert((diametre_ext*360/(2.0*10917.0)),lat_0,long_0)[4])
    Fen_Int=[elt for elt in Fen_Int.split() if elt!=''][0]
    Fen_Ext=[elt for elt in Fen_Ext.split() if elt!=''][0]
     
    Fen_Int=Fen_Int[1:-1].split('/')
    Fen_Ext=Fen_Ext[1:-1].split('/')

    long_Min_Int=float(Fen_Int[0])
    long_Max_Int=float(Fen_Int[2])
    long_Min_Ext=float(Fen_Ext[0])
    long_Max_Ext=float(Fen_Ext[2])
    
    if long_Min_Int<0 :
        long_Min_Int=360+long_Min_Int
    if long_Max_Int<0:
        long_Max_Int=360+long_Max_Int
    if long_Min_Ext < 0 :
        long_Min_Ext = 360 + long_Min_Ext
    if long_Max_Ext < 0 :
        long_Max_Ext = 360 + long_Max_Ext
    
    if long_0 < 0 :
        long_0 = 360 + long_0

    lat_Min_Int = float(Fen_Int[1])
    lat_Max_Int = float(Fen_Int[3])
    lat_Min_Ext = float(Fen_Ext[1])
    lat_Max_Ext = float(Fen_Ext[3])
    
    d_x_Int = long_Max_Int-long_0
    d_y_Int = lat_Max_Int-lat_0
    d_x_Ext = long_Max_Ext-long_0
    d_y_Ext = lat_Max_Ext-lat_0
   
    x = np.array(x)
    y = np.array(y)
    z = np.array(z)
    
    X,Y = np.meshgrid(x,y)
    mask = (((X-long_0)/d_x_Int)**2+((Y-lat_0)/d_y_Int)**2 >=1) & (((X-long_0)/d_x_Ext)**2+((Y-lat_0)/d_y_Ext)**2 <=1)

    return mask,z[mask],z[~mask]

def LSM(X,Y,Y_err):
    # Calcule par la methode des Least sqaure uen regression de ligne
    # y(x) =  a + bx
    import numpy as np
    import pandas as pd
    import sys

    X = np.array(X)
    Y = np.array(Y)
    if isinstance(Y_err,pd.core.series.Series) :
        Y_err = np.array(Y_err)
    else :
        Y_err = X/X
    

    S = (1/Y_err**2).sum()
    Sx = (X/Y_err**2).sum()
    Sy = (Y/Y_err**2).sum()
    Sxx = (X**2/Y_err**2).sum()
    Sxy = (X*Y/Y_err**2).sum()

    Delta = S*Sxx - Sx**2
    a = (Sxx*Sy-Sx*Sxy)/Delta
    b = (S*Sxy-Sx*Sy)/Delta
    sigma_a = np.sqrt(Sxx/Delta)
    sigma_b = np.sqrt(S/Delta)

    return a,sigma_a,b,sigma_b

def Histo_Data(Int,step) :
    import numpy as np
    
    bins_Int=np.arange(Int.min(),Int.max(),step)
    n,bins= np.histogram(Int,bins=bins_Int)

    return n,bins

def Plot_Lambert(Name,Diameter,lat_0,long_0,Fen,X,Y,data,ax1,clim,label,font):

    from mpl_toolkits.basemap import Basemap,cm
    import matplotlib.pyplot as plt
    import numpy as np
    
    lons, lats = np.meshgrid(np.array(X),np.array(Y))
    map=Basemap(llcrnrlon=Fen[0],llcrnrlat=Fen[1],urcrnrlon=Fen[2],urcrnrlat=Fen[3],projection='lcc',lat_0=lat_0,lon_0=long_0,ax=ax1)
    im1 = map.pcolormesh(lons,lats,np.array(data),shading='gouraud',latlon=True,ax=ax1)
    im1.set_clim(clim)
    im1.set_rasterized(True)
    map.drawparallels([float('{0:2.1f}'.format(elts)) for elts in np.arange(map.latmin+((map.latmax-map.latmin)/3.0),map.latmax,(map.latmax-map.latmin)/2.0)],labels=[1,0,1,0])
    map.drawmeridians([float('{0:2.1f}'.format(elts)) for elts in np.arange(map.lonmin+((map.lonmax-map.lonmin)/3.0),map.lonmax,(map.lonmax-map.lonmin)/2.0)],labels=[1,0,1,0])
    cbar = map.colorbar(im1,"right", size="5%", pad="10%")
    cbar.ax.set_ylabel(label,size = font)
    cbar.set_clim(clim)
    cbar.set_ticks(np.arange(clim[0],clim[1]+(clim[1]-clim[0])/4.0,(clim[1]-clim[0])/4.0))
    cbar.set_ticklabels(['{0:1.1f}'.format(elt) for elt in  np.arange(clim[0],clim[1]+(clim[1]-clim[0])/4.0,(clim[1]-clim[0])/4.0)])
    for t in cbar.ax.get_yticklabels():
        t.set_fontsize(font)
    cir1=plt.Circle((long_0,lat_0),(Diameter*360/(2.0*10917.0)),color='r',fc='none',linestyle='dashed',linewidth=4)
    ax1.add_patch(cir1)
    ax1.set_xlabel(Name,size=font)
    
def Plot_Mollweide(name,X,Y,data,ax1,clim,label):
    
    from mpl_toolkits.basemap import Basemap
    import numpy as np
    import matplotlib.pyplot as plt
    
    lons, lats = np.meshgrid(np.array(X),np.array(Y))
    # lon_0 is central longitude of projection.
    # resolution = 'c' means use crude resolution coastlines.
    map = Basemap(projection='moll',lon_0=0,resolution='c')
    im1 = map.pcolormesh(lons,lats,np.array(data),shading='gouraud',latlon=True,ax=ax1)
    im1.set_clim(clim)
    im1.set_rasterized(True)
    # draw parallels and meridians.
    map.drawparallels(np.arange(-90.,120.,30.))
    map.drawmeridians(np.arange(0.,420.,60.))
    cbar = map.colorbar(im1,"right", size="5%", pad="10%")
    cbar.ax.set_ylabel(label,size=font)
    cbar.set_clim(clim)
    cbar.set_ticks(np.arange(clim[0],clim[1]+(clim[1]-clim[0])/4.0,(clim[1]-clim[0])/4.0))
    cbar.set_ticklabels(['{0:1.1f}'.format(elt) for elt in  np.arange(clim[0],clim[1]+(clim[1]-clim[0])/4.0,(clim[1]-clim[0])/4.0)])
    ax1.set_xlabel(name,size=font)
    tick(ax1,font)
    

