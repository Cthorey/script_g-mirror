import sys,os
import matplotlib.pylab as plt
import numpy as np
import pandas as pd
import Model_Class
import Fig_Tool as FST
from mpl_toolkits.axes_grid1 import make_axes_locatable


''' Definit un ensemble de classe qui agisse sur les distributions.'''

def SE(a):
    return np.mean(a)/np.sqrt(len(a))
import scipy.stats as st
        
class Master_Table(object):
    ''' Renvoie un tableau avec une statistique complete FFC+unmodified craters
    Input : Fichier donne par Data_Observation.py (path = Variable Root)
    
    self.Lola = 'it_Lola'
    self.Grav = 'JGGRAIL_900C9A_CrustAnom_'
    On peut changer ces variables si jamais la norme pour le nom du fichier change
    
        
    Si plusieurs model, boucle sur les differents models.
    Plusieurs masque sont defini a l'interieur de la classe:
    
    Initialement :self.mask_C={'C_ALL':ALL,'C_SPA':SPA,'C_HIGH':HIGH,'C_MARIA':MARIA,'C_H_SPA':H_SPA,'C_C_FFC':C_FFC}
    Si on veut changer on peut, il suffit de changer l'attribue objet.mask_C sous forme d'un dictionnaire
    
    self.value : Permet de choisir la collonne d'interet dans le DataFrame GI_Mean,Local_Mean etc...
    
    Pour chaque sous populations ( defini par self.mask_c), la class renvoie un tableau avec le nomd du model et 
    stat de base Mean,Std,SE,size
    
    la method plot renvoie un graph avec 
    
    '''
    
    Model = np.arange(40,120,20)
    Root = '/Users/thorey/Documents/These/Projet/FFC/Gravi_GRAIL/Traitement/Data_Observation/Old/'
    Path_Save = '/Users/thorey/Documents/These/Projet/FFC/Gravi_GRAIL/Resultat/Notebook_GRAIL/Image/'
    Range = 0,1000

    def __init__(self,mask_C,mask_F):
        self.Lola = 'it_Lola_'
        self.Grav = 'JGGRAIL_900C9A_CrustAnom_'
        self.col = 'Local_Mean'
        self.mask_C = mask_C
        self.mask_F = mask_F
        self.crit_std = 10000
        self.x_lim = [0,200]
        self.y_lim = [-20,20]
        self.x_label = r'Maxmimum value for $\sigma$ outside the crater'
        self.y_label = r'Mean local crust anomaly $\delta_{g}$'
        self.label_y1 = None
        self.colormap = plt.cm.gist_ncar
        self.s_min = 5
        self.s_max = 50
        self.s_step = 5
        self.markersize = 200
        self.figsize = 8,6
        self.fig_b = False
        self.fig_name = 'fig1'
        self.font = 18
        self.loc = 'upper left'
        self.alpha = 0.5

    def f_load(self,mod):
        C = pd.read_csv(Master_Table.Root+self.Lola+str(mod)+'_Crater_Head.txt')
        F = pd.read_csv(Master_Table.Root+self.Lola+str(mod)+'_FFC.txt')
        C = C[(C.Diameter>min(Master_Table.Range)) & (C.Diameter<max(Master_Table.Range))]
        F = F[(F.Diameter>min(Master_Table.Range)) & (F.Diameter<max(Master_Table.Range))]
        return C,F

    def f_mask_C(self,mask,C):
        ''' Return a mask correspondign to UC'''
    
        mask_C = {'C_ALL' : C[C.Mask_Not_FFC],
                  'C_SPA' : C[~C.Mask_SPA_Out & C.Mask_Not_FFC],
                  'C_HIGH': C[C.Mask_Highlands & C.Mask_Not_FFC],
                  'C_MARIA' : C[~C.Mask_Highlands & C.Mask_Not_FFC],
                  'C_H_SPA' : C[~C.Mask_Highlands & C.Mask_Not_FFC],
                  'C_C_FFC' : C[C.Mask_C_FFC & C.Mask_Highlands & C.Mask_SPA_Out & C.Mask_Not_FFC]}
                  #'C_H_B_184' : C[C.Mask_Basin_Out_184 & C.Mask_Highlands & C.Mask_Not_FFC],
                  #'C_H_B_300' : C[C.Mask_Basin_Out_300 & C.Mask_Highlands & C.Mask_Not_FFC],
                  #'C_H_B_400' : C[C.Mask_Basin_Out_400 & C.Mask_Highlands & C.Mask_Not_FFC]}
            
        d = dict.fromkeys(mask)
        
        return {k : v for k, v in mask_C.iteritems() if k in d.iterkeys()}

    def f_mask_F(self,mask,F):
        ''' Return a mask correspondign to FFC'''
        
        mask_F = {'F_ALL': F[F.Diameter == F.Diameter],
                  'F_SPA': F[~F.Mask_SPA_Out],
                  'F_HIGH': F[F.Mask_Highlands],
                  'F_MARIA': F[~F.Mask_Highlands],
                  'F_H_SPA': F[F.Mask_Highlands & F.Mask_SPA_Out]}
                  #'F_H_B_184' : F[F.Mask_Basin_Out_184 & F.Mask_Highlands],
                  #'F_H_B_300' : F[F.Mask_Basin_Out_300 & F.Mask_Highlands],
                  #'F_H_B_400' : F[F.Mask_Basin_Out_400 & F.Mask_Highlands]}
        d = dict.fromkeys(mask)

        return {k : v for k, v in mask_F.iteritems() if k in d.iterkeys()}
    
    def var(self,mod,crit_std):
        C,F = self.f_load(str(mod))
        C = C[C.GE_std<crit_std]
        F = F[F.GE_std<crit_std]
        mask_C = self.f_mask_C(self.mask_C,C) #UC
        mask_F = self.f_mask_F(self.mask_F,F)
        mask = dict(mask_C.items()+mask_F.items())
        tab = dict.fromkeys(mask)
        return mask
    
        
    def ttest(self):
        ''' Realise un tableau avec les different valeur du ttest'''
        Iter = [self.Grav+str(elt) for elt in Master_Table.Model]
        tab = dict.fromkeys(Iter)
        
        for i,mod in enumerate(Iter):
            
            mask = self.var(mod,self.crit_std)
            table = pd.DataFrame({'Index':mask.keys()})
            table['Model']=mod

            table = pd.DataFrame([[st.ttest_ind(elt1[self.col],elt2[self.col]) for elt1 in mask.itervalues()] for elt2 in mask.itervalues()],columns=mask.keys(),index=mask.keys())
            table['Model']=mod
            tab[mod]=table
            
        return pd.concat(tab.values())

    def axes(self,ax):
        ax.set_xlabel(self.x_label,size=self.font)
        ax.set_ylabel(self.y_label,size=self.font)
        ax.set_xlim(self.x_lim)
        ax.set_ylim(self.y_lim)
        FST.tick(ax,self.font)
        FST.legd_Scatter(ax,self.loc,self.font)
        

    def ttest_plot(self,range_D):

        c = self.f_color_map(range_D)
        fig, ax = plt.subplots(figsize=self.figsize)
        
        for j,elt in enumerate(range_D):
            Master_Table.Range = elt
            ttest =[]
            t = self.ttest()
            for i in np.arange(0,4,1):
                m = int(t.get('Model').loc[self.mask_C[0]][i].split('_')[-1])
                p = float(t.get(self.mask_F[0]).loc[self.mask_C[0]][i][1])
                ttest.append([m,p])
            ttest= pd.DataFrame(ttest,columns=['model','p'])
            ttest['size']= self.markersize
            ax.scatter(ttest.model,ttest.p,s=ttest.size,color = c[j],label = elt)
            self.axes(ax)
        
        Master_Table.Range= 0,2000 # On reinitiaise Range.

    
    def __table(self,crit_std):

        Iter = [self.Grav+str(elt) for elt in Master_Table.Model]
        tab = dict.fromkeys(Iter)
        
        for i,mod in enumerate(Iter):
            
            index = {'Size':len,'Mean':np.mean,'SE':SE,'SD':np.std} #Index creation
            table = pd.DataFrame({'Index':index.keys()})
            table['Model']=mod
            
            mask = self.var(mod,crit_std)
            table = []
            table = pd.DataFrame([[func(elt[self.col]) for elt in mask.itervalues()] for func in index.itervalues()],columns=mask.keys(),index=index.keys())
            table['Model'] = mod
            tab[mod] = table
        
        Tab = pd.concat([tab[Iter[elt]] for elt in np.arange(0,4,1)])
        Tab = Tab.drop('Index',0)
        
        return Tab
        
    def table(self):
        crit_std = self.crit_std
        return  self.__table(crit_std)
    
    def f_color_map(self,mask):
        N=len(mask)
        c=[self.colormap(i) for i in np.linspace(0, 0.9,N)]
        return c
    
    def plot(self,mod):
        ''' plot the crust anomaly for a range of value for the max outside std '''
        
        fig, ax = plt.subplots(figsize=self.figsize)
        mask = self.var(self.Grav+str(mod),self.crit_std)
        c = self.f_color_map(mask)
        i=0
        for i,key in enumerate(mask.iterkeys()):
            result=[]
            for elt in np.arange(self.s_min,self.s_max,self.s_step):
                df = self.var(self.Grav+str(mod),elt)[key]
                result.append([elt, df[self.col].mean()])
            df_temp = pd.DataFrame(result,columns =['sigma','Mean'])
            ax.plot(df_temp.sigma,df_temp.Mean,marker='o',color=c[i],label=key)
            i+=1
        self.axes(ax)
        if self.fig_b:
            fig.savefig(Master_Table.Path_Save+'_'+self.fig_name+'.eps',dpi = 200)

class Scatter(Master_Table):
    ''' Class that handle the scatter plot '''

    def __init__(self,mask_C,mask_F):
        Master_Table.__init__(self,mask_C,mask_F)
        self.x_lim = [0,200]
        self.y_lim = [-80,80]
        self.x_label = None
        self.y_label = None
        self.label_y1 = None
        self.bin_x1 = 10
        self.bin_y1 = 10
        self.Size = None
        self.font = None
        self.alpha = 0.5
        self.col = 'Local_Mean'
        self.crit_std = 2000
        
    def scat(self,mod):
        ''' Scatter plot '''
        fig, axScatter = plt.subplots(figsize=self.figsize)
        c = self.f_color_map(self.mask_C+self.mask_F)
        mask = self.var(self.Grav+str(mod),self.crit_std)
        i=0
        for key,elt in mask.iteritems():
            label_y1 = key
            axScatter.scatter(elt.Diameter,elt[self.col],color=c[i],alpha = self.alpha,label = label_y1)
            i +=1
        self.axes(ax)
        
        divider = make_axes_locatable(axScatter)
        if self.fig_b:
            fig.savefig(Master_Table.Path_Save+'_'+self.fig_name+'.eps',dpi = 200)
        return [fig,divider,axScatter]

        
    def hist_1(self,mod):
        fig,divider,axScatter = self.scat(mod)
        
        axHistx = divider.append_axes("top", 1, pad=0.3, sharex=axScatter)
        #make some labels invisible
        plt.setp(axHistx.get_xticklabels(),
                visible=False)
        
        bins = np.arange(self.x_lim[0],self.x_lim[1]+self.bin_x1,self.bin_x1)
        width1 = (bins[1]-bins[0])/(len(self.mask_C+self.mask_F))
        width = (bins[1]-bins[0])
        c = self.f_color_map(self.mask_C+self.mask_F)
        mask = self.var(self.Grav+str(mod),self.crit_std)
        
        i=0
        for key,elt in mask.iteritems():
            n,bin = np.histogram(elt.Diameter,bins = bins)
            n = n/np.float(n.sum())*100
            axHistx.bar(bin[:-1]+width1*np.float(i),n,width1,color = c[i],label ='hello' )
            i+=1
        
        axScatter.set_xlim(self.x_lim)
        axHistx.set_xlim(self.x_lim)
        FST.tick(axHistx,self.font)

        #axHistx.axis["bottom"].major_ticklabels.set_visible(False)
        for tl in axHistx.get_xticklabels():
            tl.set_visible(False)
        axHistx.set_yticks([0,25,50,75])
        axHistx.set_ylabel('Frequency (%)',size = self.font)
        if self.fig_b:
            fig.savefig(Master_Table.Path_Save+'_'+self.fig_name+'.eps',dpi = 200)
        
    def hist_2(self,mod):
        fig,divider,axScatter = self.scat(mod)
        axHisty = divider.append_axes("right", 1, pad=0.3, sharey=axScatter)
        #make some labels invisible
        plt.setp(axHisty.get_yticklabels(),visible=False)

        bins = np.arange(self.y_lim[0],self.y_lim[1]+self.bin_y1,self.bin_y1)
        width1 = (bins[1]-bins[0])/len(self.mask_C+self.mask_F)
        width = (bins[1]-bins[0])
        c = self.f_color_map(self.mask_C+self.mask_F)
        mask = self.var(self.Grav+str(mod),self.crit_std)
        i=0
        for key,elt in mask.iteritems():
            n,bin = np.histogram(elt[self.col],bins = bins)
            n = n/np.float(n.sum())*100
            axHisty.barh(bin[:-1]-width/np.float(len(mask))+width1*i,n,height=width1,color = c[i])
            i+=1
       
        for tl in axHisty.get_yticklabels():
            tl.set_visible(False)
        axHisty.set_xticks([0,25,50,75])
        axHisty.set_xlabel('Frequency (%)',size = self.font)
        
        axScatter.set_ylim(self.y_lim)
        axHisty.set_ylim(self.y_lim)
        FST.tick(axHisty,self.font)
        if self.fig_b:
            fig.savefig(Master_Table.Path_Save+'_'+self.fig_name+'.eps',dpi = 200)

class HistoMean(Master_Table):

    def __init__(self,mask_C,mask_F):
        ''' constructor'''
        Master_Table.__init__(self,mask_C,mask_F)
        self.bin = 10 # valeur de la bin
        self.bin_min = 30 # bin min
        self.bin_max = 210 # bin max
        self.y_label = 'Local Mean $\mu$ (mGal)'
        self.x_label = 'Diameter (km)'
        self.hist = None
        self.Size = None
        self.font = None
        self.crit_std = 2000

    def slice(self,_df):
        ''' Renvoie un Dataframe avec 4 colonnes 'Diameter','Mean','Std','Size' '''

        bins = np.arange(self.bin_min,self.bin_max,self.bin)
        result =[]
        for elt in bins:
            df_temp = _df[(_df.Diameter<elt+self.bin)
                              & (_df.Diameter>elt-self.bin)] # df temporaire correspond a la bin
            try :
                mean,std,size = df_temp[self.col].mean(),df_temp[self.col].std(),len(df_temp)
            except :
                print 'bin trop petite'
            result.append([elt,mean,std,size])
        df = pd.DataFrame(result,columns =['Diameter','Mean','Std','Size'])
        df['StE'] = df.Mean/(np.sqrt(df.Size))
        return df
    
    def tab(self,mod):
        ''' Renvoie un tableau avec ['Diameter','Mean','Std','Size']'''
        mask = self.var(self.Grav+str(mod),self.crit_std)
        tab = dict.fromkeys(mask)
        for key,elt in mask.iteritems():
            tab[key] = self.slice(elt)
            tab[key]['Loc']=key
            final = pd.concat([tab[j] for j in mask.keys()])
        return final
    
    def plot(self,col,mod):
        ''' Renvoie un plot'''
        mask = self.var(self.Grav+str(mod),self.crit_std)
        tab = dict.fromkeys(mask)

        fig, ax = plt.subplots(figsize=self.figsize)
        c = self.f_color_map(self.mask_C+self.mask_F)
        i=0
        for key,elt in mask.iteritems():
            hist = self.slice(elt)
            if col == 'Mean':
                ax.errorbar(hist.Diameter,hist[col],yerr = hist.StE,color=c[i],alpha=self.alpha,fmt='o',label = key)
            else :
                ax.plot(hist.Diameter,hist[col],marker='o',color=c[i],label=key)
            i +=1
        self.axes(ax)
        if self.fig_b:
            fig.savefig(Master_Table.Path_Save+'_'+self.fig_name+'.eps',dpi = 200)


    

class Depth(HistoMean):
    '''
    global :
       Same than MasterTable and HistoMEan
       
    parametre :
       - mask_C : Liste qu contient les locatiosn des UC
        C_ALL : Corrrespond to all the data from Head2010 (complex craters larger than 20 km) (Of course, FFC are remove from this)
        C_SPA : Part of these craters that are within SPA bassin
        C_HIGH : Part of these craters that are outside of the maria
        C_MARIA : Part of these craters in contact or within the maria
        C_H_SPA : Part of these craters outside the maria and outside SPA
        C_FFC : Part of these craters that are close from FFCs
        ( define as the min distance between FFC and one crater smaller than 3 times the FFC Diameter) and outside the maria
       - mask_F : Liste qui contient les locations des FFCs
        F_ALL : Corrrespond to all the FFCs from Jozwiak2012
        F_SPA : Part of these craters that are within SPA bassin
        F_HIGH : Part of these craters that are outside of the maria
        F_MARIA : Part of these craters in contact or within the maria
        F_H_SPA : Part of these craters outside the maria and outside SPA

    attribut :
        col_err  : Name of the colum containing the error on the depth = 'Sigma_Depth' 
        err_max :  Error maximum allowed for the dataset !
        col = 'Thick_C' Name of the collone we use to calculate the thickness
        of the intrusion. Thick_C derived from the data, Thick_K calculate from the Kalynn scalin
        g laws.

    method :
        Coeff_Fit(self): Method that return a dict corresponding to mask
        with the values for A and B for the fit in the power law A*D**B with
        D the diameter. mask is a dict with keys the name of the location and values
        the corresponding dataframe.

        table_thickness(self): Return a dict that contains the keys from mask that are
        FFC locations and the values corresponding to a vector [A,B]
            example :{'C_H_SPA': [0.68135345470221709, 0.41666223160479554],
                      'C_MARIA': [0.32545523618216815, 0.51021076719164349],
                      'F_H_SPA': [0.045934239381247903, 1.0681107790804083],
                      'F_MARIA': [0.092760607203796253, 0.77886144990856132]}
        
        table_thick(self): Retunr a table with 5 columns and each row correspondign to one
        FFC :
            example : 	Name	Diameter	Thick_C	Thick_K	Location
                107	 Schluter	 89.0	-0.787041	 0.222437	 F_MARIA
                108	 Briggs	 37.0	 1.006526	 2.053681	 F_MARIA
                109	 Hansteen	 45.0	 1.293389	 2.346024	 F_MARIA
        
        def plot_thickness(self): Return a plot Intrusion Thickness versus Diameter.
        Don't forget to define self.col = 'Thick_K' or 'Thick_C' corresponding
        to whant you want ( see attribut)
        
        def error_bar(self,mod): retunr a scatter plot depth versus Diameter
        with error bar corresponding to the different location defined
        in self.mask_C and self.mask_F. The corresponding scaling laws ( output of Coeff_Fit)
        are also shown
        
        def plot(self,mod): retunr a scatter plot depth versus Diameter
        corresponding to the different location defined in self.mask_C and self.mask_F.
        The corresponding scaling laws ( output of Coeff_Fit)
        are also shown
        
        def tab_bin(self,mod): Return a table with the Thickness versus Diameter
        separated in different bins. For example with, self.bin = 40. Mean correspond
        to the mean of self.col of the bean, std the standard deviation, ste the standar error
        and loc the location.
              example
                Diameter	Mean	Std	Size	StE	Loc
                0	 30	 1.940439	 0.800327	 37	 0.319006	 F_MARIA
                1	 130	 2.050016	 0.865043	 26	 0.402041	 F_MARIA
                0	 30	 2.192782	 0.780156	 88	 0.233751	 F_H_SPA
                1	 130	 2.101775	 0.878733	 53	 0.288701	 F_H_SPA
        
        def plot_bin(self,mod): Return a plot intrusion thickenss versus Diameter with
        Diameter defined by the bins in tab_bin and the intrusion thickenss is the value of
        the mean in the overlying table.


        Example : Voir notebook DEPTH_UC_FFC
    '''
        
    def __init__(self,mask_C,mask_F):
        Master_Table.__init__(self,mask_C,mask_F)
        HistoMean.__init__(self,mask_C,mask_F)
        self.col_err = 'Sigma_Depth'
        self.err_max = 100
        self.col = 'Thick_C'

    def Coeff_Fit(self):
        '''Method that return a dict corresponding to mask
        with the values for A and B for the fit in the power law A*D**B with
        D the diameter. mask is a dict with keys the name of the location and values
        the corresponding dataframe.'''
        
        mask = self.var(self.Grav+str(80),self.crit_std)
        tab = dict.fromkeys(mask)
        for keys,df in mask.iteritems():
            x_1 = df[df.Sigma_Depth<self.err_max].Diameter
            y_1 = df[df.Sigma_Depth<self.err_max].Depth
            y_err = df[df.Sigma_Depth<self.err_max].Sigma_Depth
            a,sigma_a,b,sigma_b = FST.LSM(np.log(x_1),np.log(y_1),y_err/y_1)
            tab[keys] = [np.exp(a),b]

        return tab

    def table_thickness(self):
        ''' Return a dict that contains the keys from mask that are
        FFC locations and the values corresponding to a vector [A,B]
            example :{'C_H_SPA': [0.68135345470221709, 0.41666223160479554],
                      'C_MARIA': [0.32545523618216815, 0.51021076719164349],
                      'F_H_SPA': [0.045934239381247903, 1.0681107790804083],
                      'F_MARIA': [0.092760607203796253, 0.77886144990856132]}'''

        mask = self.var(self.Grav+str(80),self.crit_std)
        mask_F = { key: mask[key] for key in self.mask_F}
        dD = { key: self.Coeff_Fit()[key] for key in self.mask_C}
        Kal = dict.fromkeys(dD)
        Kal['C_H_SPA'] = [1.558,0.254]
        Kal['C_MARIA'] = [0.870,0.352]

        for key,df in mask_F.iteritems():
            df['Thick_C']= dD['C_'+key[2:]][0]*df.Diameter**dD['C_'+key[2:]][1] - df.Depth
            df['Thick_K']= Kal['C_'+key[2:]][0]*df.Diameter**Kal['C_'+key[2:]][1] -df.Depth

        return mask_F
    
    def table_thick(self):

        '''Retunr a table with 5 columns and each row correspondign to one
        FFC :
            example : 	Name	Diameter	Thick_C	Thick_K	Location
                107	 Schluter	 89.0	-0.787041	 0.222437	 F_MARIA
                108	 Briggs	 37.0	 1.006526	 2.053681	 F_MARIA
                109	 Hansteen	 45.0	 1.293389	 2.346024	 F_MARIA'''
        
        mask_F = self.table_thickness()
        tab = dict.fromkeys(mask_F)
        for key,df in mask_F.iteritems():
            tab[key]= df[['Name','Diameter','Thick_C','Thick_K']]
            tab[key]['Location'] = key
        
        return  pd.concat(tab.values())

    def plot_thickness(self):

        '''Return a plot Intrusion Thickness versus Diameter.
        Don't forget to define self.col = 'Thick_K' or 'Thick_C' corresponding
        to whant you want ( see attribut)'''
        
        mask_F = self.table_thickness()
        fig,ax = plt.subplots(figsize=self.figsize) # create the top-level container
        c = self.f_color_map(mask_F.keys()) # hold for kalynn and derived thickenss
        i=0
        for key,df in mask_F.iteritems():
            df['size']= 100
            ax.scatter(df.Diameter,df[self.col],df.size,color=c[i],label = key+' Derived Thickness')
            i+=1
        self.axes(ax)
        
        
    def error_bar(self,mod):
        ''' retunr a scatter plot depth versus Diameter
        with error bar corresponding to the different location defined
        in self.mask_C and self.mask_F. The corresponding scaling laws ( output of Coeff_Fit)
        are also shown'''

        fig,ax = plt.subplots(figsize=self.figsize) # create the top-level container
        c = self.f_color_map(self.mask_C+self.mask_F)
        mask = self.var(self.Grav+str(mod),self.crit_std)
        coeff = self.Coeff_Fit()
        i=0
        for key,elt in mask.iteritems():
            ax.errorbar(elt.Diameter,elt.Depth,yerr=elt.Sigma_Depth/elt.Depth,color=c[i],alpha=self.alpha,fmt='o',
                        label= key+':'+'d={0:1.3f}'.format(coeff[key][0])+r'$D^{'+'{0:1.3f}'.format(coeff[key][1])+r'}$')
            ax.scatter(elt.Diameter,coeff[key][0]*elt.Diameter**coeff[key][1],color=c[i])
            i +=1
        self.axes(ax)
        if self.fig_b:
            fig.savefig(Master_Table.Path_Save+'_'+self.fig_name+'.eps',dpi = 200)

        return ax

    def plot(self,mod):
        ''' retunr a scatter plot depth versus Diameter
        corresponding to the different location defined in self.mask_C and self.mask_F.
        The corresponding scaling laws ( output of Coeff_Fit)
        are also shown'''

        fig,ax = plt.subplots(figsize=self.figsize) # create the top-level container
        c = self.f_color_map(self.mask_C+self.mask_F)
        mask = self.var(self.Grav+str(mod),self.crit_std)
        coeff = self.Coeff_Fit()
        i=0
        for key,elt in mask.iteritems():
            ax.scatter(elt.Diameter,elt.Depth,color=c[i], alpha=self.alpha, marker='o',
                        label= key+':'+'d={0:1.3f}'.format(coeff[key][0])+r'$D^{'+'{0:1.3f}'.format(coeff[key][1])+r'}$')
            ax.scatter(elt.Diameter,coeff[key][0]*elt.Diameter**coeff[key][1],color=c[i])
            i +=1
        self.axes(ax)
        if self.fig_b:
            fig.savefig(Master_Table.Path_Save+'_'+self.fig_name+'.eps',dpi = 200)

        return ax

    def tab_bin(self,mod):
        ''' Return a table with the Thickness versus Diameter
        separated in different bins. For example with, self.bin = 40. Mean correspond
        to the mean of self.col of the bean, std the standard deviation, ste the standar error
        and loc the location.
              example
                Diameter	Mean	Std	Size	StE	Loc
                0	 30	 1.940439	 0.800327	 37	 0.319006	 F_MARIA
                1	 130	 2.050016	 0.865043	 26	 0.402041	 F_MARIA
                0	 30	 2.192782	 0.780156	 88	 0.233751	 F_H_SPA
                1	 130	 2.101775	 0.878733	 53	 0.288701	 F_H_SPA'''
        
        mask = self.table_thickness()
        tab = dict.fromkeys(mask)
        for key,elt in mask.iteritems():
            tab[key] = self.slice(elt)
            tab[key]['Loc']=key
            final = pd.concat([tab[j] for j in mask.keys()])
        return final

    def plot_bin(self,mod):
        ''' Return a plot intrusion thickenss versus Diameter with
        Diameter defined by the bins in tab_bin and the intrusion thickenss is the value of
        the mean in the overlying table.'''
        
        mask = self.table_thickness()
        tab = dict.fromkeys(mask)
        fig,ax = plt.subplots(figsize=self.figsize)

        c = self.f_color_map(self.mask_C+self.mask_F)
        i=0
        for key,elt in mask.iteritems():
            hist = self.slice(elt)
            ax.errorbar(hist.Diameter,hist['Mean'],yerr = hist.StE,color=c[i],alpha=self.alpha,fmt='o',label = key)
            i +=1
        self.axes(ax)
        if self.fig_b:
            fig.savefig(Master_Table.Path_Save+'_'+self.fig_name+'.eps',dpi = 200)

 
    
    
