#!/usr/bin/env python

#######################################
#Description
#BUT : Realiser un plot en projection Molweiide de la surface de la lune pour la topo

#######################################
#Library

import numpy as np
from scipy.io import netcdf_file as netcdf
import scipy as sp
import pandas as pd
import Variable as v
import os
import matplotlib.pylab as plt
import matplotlib.gridspec as gridspec
import sys
import Fig_Tool as FST
import time
import window

#######################################
#A renseigner

path_save='/Users/thorey/Documents/These/Projet/FFC/Gravi_GRAIL/Resultat/Figure/' # Lieu ou sauver la gigure
Path_G = '/Users/thorey/Documents/These/Projet/FFC/Gravi_GRAIL/Traitement/JGGRAIL_900C9A_CrustAnom_60/Global_map'
Carte_G = '34_12_3220_900_60_misfit_rad.grd' #Gravi
Path_L = '/Users/thorey/Documents/These/Projet/FFC/Gravi_GRAIL/Traitement/LOLA'
Carte_L = '34_12_3220_660_80_misfit_Lola.grd' # Lola

#######################################
#Top level figurer

FST.font('normal','normal',22)
fig=plt.figure(figsize=(16,28)) # create the top-level container
Nb_Ligne = 2 # Nombe de plot sur une page 
gs=gridspec.GridSpec(Nb_Ligne,1) # create a GridSpec object avec 2 colonne et N lignes
ax=[plt.subplot(gs[f,0]) for f in np.arange(0,Nb_Ligne)] # creer une liste de liste a ax[0] relatif a la colone de gauche et ax[1] relatif a la colonne de droite.

################################################
# Load les cartes sous forme de 3 arrays
# x* correspond a un vecteur qui range les longitudes
# y* correspond a un vecteur qui range les longitudes
# z* correspond a une grosse matrice qui prend les valeurs du champ de gravite. Il est index par x_d en collone et y_d en ligne.

x_l,y_l,z_l = FST.Sample_Grd(Path_L,Carte_L)
x_g,y_g,z_g = FST.Sample_Grd(Path_G,Carte_G)


FST.Plot_Mollweide('topo',x_l,y_l,z_l,ax[0],[z_l.min().min(),z_l.max().max()],r'Topography (km)')
sys.exit()

#fig.savefig(path_save+'fig_FFC.pdf',dpi=200,facebolor='w',edgecolor='w',orientation='portrait')
