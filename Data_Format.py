#!/usr/bin/env python
################################################
# Script definition
# Renvoie un fichier formatter pret a etre utiliser avec pandas pour chaque type de
# crater.

################################################
# Library a utiliser

import numpy as np
import pandas as pd
import os
import matplotlib.pylab as plt
import sys
import Format_Tool as FT
import Variable as v
import Data_Obs_Tool as DOT
from shapely.geometry import Polygon
from shapely.geometry import MultiPolygon
from shapely.geometry import Point
from shapely.geometry import MultiPoint

################################################
# Path fichier origine

Root = DOT.Init('thorey')

Path = Root+'/Gravi_Grail/Traitement/Info_General/'

################################################
# Maria locations distance away from the border

Long_Maria,Lat_Maria = FT.Maria_Location_2(Root)
mask = []
Maria_loc = pd.DataFrame(Long_Maria,columns = ['Long']).join(pd.DataFrame(Lat_Maria,columns=['Lat']))

################################################
# Maria locations within the maria
Maria = FT.Maria_Location_3(Root)
pol=[]
for elt in Maria[1:]:
    pol.append(Polygon(elt))
    
polygons=MultiPolygon(pol) # voir notebook Maria Definition

################################################
# FFC

file = Path+ 'LolaLargeLunarCraterCatalog.csv'
C = pd.read_csv(file,sep=',')
C=C.rename(columns = {'Diam_km':'Diameter','Lon':'Long'})
C['Name'] = C.index+1
C['Location'] = 0

Basin_A = C[C.Diameter > 184] # Basin !
Basin_B = C[C.Diameter > 300] # Basin !
Basin_C = C[C.Diameter > 400] # Basin !

[name,lat,long,clas,diame,maria]=v.Variable('FFC',Root)
F2 = pd.DataFrame(name,columns=['Name']).join(pd.DataFrame(lat,columns=['Lat'])).join(pd.DataFrame(long,columns=['Long']))
F2 = F2.join(pd.DataFrame(clas,columns=['Class'])).join(pd.DataFrame(diame,columns=['Diameter'])).join(pd.DataFrame(maria,columns=['Maria']))
point_F2 = MultiPoint(np.array(F2[['Long','Lat']]))


F2['Mask_Not_FFC'] = [FT.haversine(F2.Long*np.pi/180.0,F2.Lat*np.pi/180.0,F2.Long[i]*np.pi/180.0,F2.Lat[i]*np.pi/180.0).min()>
                      F2.Diameter[i]/2.0 for i,row in F2.iterrows()]
                       # Renvoie True si le crater considere n'est pas un FFC, False sinon

F2['Mask_Highlands'] = [not polygons.contains(Point(F2.Long[i],F2.Lat[i])) for i,row in F2.iterrows()]

for elt in np.arange(10,20,10):
    F2['Mask_Highlands_'+str(elt)] = [all(FT.haversine(Maria_loc.Long*np.pi/180.0,Maria_loc.Lat*np.pi/180.0
                                                       ,F2.Long[i]*np.pi/180.0,F2.Lat[i]*np.pi/180.0)>np.array([F2.Diameter[i]/2.0,elt]).max())
                                                       for i,row in F2.iterrows()]
    
F2['Mask_C_FFC'] = [FT.haversine(F2.Long*np.pi/180.0,F2.Lat*np.pi/180.0,F2.Long[i]*np.pi/180.0,F2.Lat[i]*np.pi/180.0).min()
                    <150 for i,row in F2.iterrows()]
                        

F2['Mask_SPA_Out'] = [FT.haversine(-168.9*np.pi/180.0,-55.0*np.pi/180.0,F2.Long[i]*np.pi/180.0,F2.Lat[i]*np.pi/180.0)>
                      1.238*970.0 for i,row in F2.iterrows()]
                    # Renvoie True si le crater est en dehors de SPA, False sinon
                    #Garrick-Bethell & M.T. Zuber derived the best fit topographt for SPA

F2['Mask_Basin_Out_184'] = [all(FT.haversine(Basin_A.Long*np.pi/180.0,Basin_A.Lat*np.pi/180.0,F2.Long[i]*np.pi/180.0,F2.Lat[i]*np.pi/180.0)>
                       Basin_A.Diameter) for i,row in F2.iterrows()] # Renvoie True si le crater considere est loin des basins.
                        #Distance entre les 2 superieur au diametre du basin.

F2['Mask_Basin_Out_300'] = [all(FT.haversine(Basin_B.Long*np.pi/180.0,Basin_B.Lat*np.pi/180.0,F2.Long[i]*np.pi/180.0,F2.Lat[i]*np.pi/180.0)>
                       Basin_B.Diameter) for i,row in F2.iterrows()] # Renvoie True si le crater considere est loin des basins.
                        #Distance entre les 2 superieur au diametre du basin.

F2['Mask_Basin_Out_400'] = [all(FT.haversine(Basin_C.Long*np.pi/180.0,Basin_C.Lat*np.pi/180.0,F2.Long[i]*np.pi/180.0,F2.Lat[i]*np.pi/180.0)>
                       Basin_C.Diameter) for i,row in F2.iterrows()] # Renvoie True si le crater considere est loin des basins.
                        #Distance entre les 2 superieur au diametre du basin.
                        
F2.to_csv(Path+'/'+'FFC_Final.txt',sep=',',index=None)


################################################
# Crater Head
print 'Crater_Head'
file = Path+ 'LolaLargeLunarCraterCatalog.csv'
F = pd.read_csv(file,sep=',')
F=F.rename(columns = {'Diam_km':'Diameter','Lon':'Long'})
F['Name'] = F.index+1
F['Location'] = 0

Basin_A = F[F.Diameter > 184] # Basin !
Basin_B = F[F.Diameter > 300] # Basin !
Basin_C = F[F.Diameter > 400] # Basin !
# Creation de mask pour les locations

F['Mask_Not_FFC'] = [FT.haversine(F2.Long*np.pi/180.0,F2.Lat*np.pi/180.0,F.Long[i]*np.pi/180.0,F.Lat[i]*np.pi/180.0).min()>F.Diameter[i]/2.0 for i,row in F.iterrows()] # Renvoie True si le crater considere n'est pas un FFC, False sinon

F['Mask_Highlands'] = [not polygons.contains(Point(F.Long[i],F.Lat[i])) for i,row in F.iterrows()]

for elt in np.arange(10,20,10):
    F['Mask_Highlands_'+str(elt)] = [all(FT.haversine(Maria_loc.Long*np.pi/180.0,Maria_loc.Lat*np.pi/180.0
                                                      ,F.Long[i]*np.pi/180.0,F.Lat[i]*np.pi/180.0)>np.array([F.Diameter[i]/2.0,elt]).max())
                                                       for i,row in F.iterrows()]

F['Mask_C_FFC'] = [FT.haversine(F2.Long*np.pi/180.0,F2.Lat*np.pi/180.0,F.Long[i]*np.pi/180.0,F.Lat[i]*np.pi/180.0).min()<150
                   for i,row in F.iterrows()] # Renvoie True si le crater est proche d'un FFC


F['Mask_SPA_Out'] = [FT.haversine(-168.9*np.pi/180.0,-55.0*np.pi/180.0,F.Long[i]*np.pi/180.0,F.Lat[i]*np.pi/180.0)>1.238*970.0
                     for i,row in F.iterrows()]# Renvoie True si le crater est en dehors de SPA, False sinon
                     #Garrick-Bethell & M.T. Zuber derived the best fit topographt for SPA

F['Mask_Basin_Out_184'] = [all(FT.haversine(Basin_A.Long*np.pi/180.0,Basin_A.Lat*np.pi/180.0,F.Long[i]*np.pi/180.0,F.Lat[i]*np.pi/180.0)>
                        Basin_A.Diameter) for i,row in F.iterrows()] # Renvoie True si le crater considere est loin des basins.
                        #Distance entre les 2 superieur au diametre du basin.

F['Mask_Basin_Out_300'] = [all(FT.haversine(Basin_B.Long*np.pi/180.0,Basin_B.Lat*np.pi/180.0,F.Long[i]*np.pi/180.0,F.Lat[i]*np.pi/180.0)>
                        Basin_B.Diameter) for i,row in F.iterrows()] # Renvoie True si le crater considere est loin des basins.
                        #Distance entre les 2 superieur au diametre du basin.

F['Mask_Basin_Out_400'] = [all(FT.haversine(Basin_C.Long*np.pi/180.0,Basin_C.Lat*np.pi/180.0,F.Long[i]*np.pi/180.0,F.Lat[i]*np.pi/180.0)>
                        Basin_C.Diameter) for i,row in F.iterrows()] # Renvoie True si le crater considere est loin des basins.
                        #Distance entre les 2 superieur au diametre du basin.

F.to_csv(Path+'/'+'Crater_Head_Final.txt',sep=',',index=None)

################################################
# Crater_Kalynn

F3 = pd.read_csv(Root+'/Gravi_Grail/Traitement/Info_General/Crater_Kalynn_2.csv')
F3.columns = ['Diameter','Lat','Long','Depth_k','S_Depth_k','Maria']
F3['Name'] = F3.index+1
F3['Class'] = 0

F3.to_csv(Path+'/'+'Crater_Kalynn_final.txt',sep=',',index=None)
