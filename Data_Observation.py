#!/usr/bin/env python
################################################
# Script definition
# OUTPUT : Fichier qui ecrit les caracteristique relatif a la carte de gravi pour chaque crater

# DESCRIPTION : On prend la carte indiquer dans Path et on va en tirer les information d'interet pour chauqe crater. 
# Les information sont 'I_mod','I_min','R_mod','R_max'. La definition est indiquer dans ce papier. 
# http://www.lpi.usra.edu/meetings/lpsc2013/pdf/1309.pdf. En gros I refere to Int. Int est un mask de rayon le rayon du crater.
# R refere ro Rim, Rim est une courronne centrer sur le centre du crater, de rayon interieru 0.98R et de rayon ext 1.05R.

################################################
# Library a utiliser

import numpy as np
import pandas as pd
import Variable as v
import os
import matplotlib.pylab as plt
import sys
import Data_Tool as CDT
import Data_Obs_Tool as DOT
import window

################################################
# Chose a renseigner

Root = DOT.Init('clavius')

type = ['FFC','Crater_Head']

Source_r = Root+'Gravi_Grail/Traitement/Info_General/' # Output de formatx

Path_Save = Root+'Gravi_GRAIL/Traitement/Data_Observation' # Path de sauvegarde

Path_Lo = Root+'Gravi_GRAIL/Traitement/LOLA' # Path carte LOla
Carte_Lo = ['LDEM_64','34_12_3220_660_80_misfit_Lola'] # Carte LOLA sans le .grd

Path_Gr = Root+'Gravi_GRAIL/Traitement/GRAV/' # PATH Different model
Carte_Gr = 'JGGRAIL_900C9A_CrustAnom_' # Basename

Mod = [str(elt) for elt in np.concatenate((np.arange(10,40,10),np.arange(40,120,20)),axis=0)]
Mod = dict.fromkeys(Mod)

for carte in Carte_Lo:
    print carte,t
    for t in type: # boucle sur le type
        print carte,t
        ################################################
        # Load des donnes (Output de format.py)

        Source = Source_r+str(t)+'_final.txt'
        F = pd.read_csv(Source)
        F.drop_duplicates(cols='Name', take_last=True)
        F.Name = t[0]+'_'+F.Name.apply(lambda x:str(x))

        ################################################
        # Dataframe pour les donne LOLA

        Carte_Final_L = carte+'.grd' # Nom de la carte qu'on utilise
        Error_L,L = DOT.Obs_Lola(F,Path_Lo,Carte_Final_L)        

        ################################################
        # Dataframe pour les donne Gravi

        for key in Mod.iterkeys() :
                Model = Carte_Gr + key   
                Path_G = Path_Gr + Model + '/Global_map' # Path ou se trouve la map
                Carte_Final_G = '34_12_3220_900_'+ key +'_misfit_rad.grd' # Nom de la carte qu'on utilise
                Mod[key] = DOT.Obs_Gravi(F,Path_G,Carte_Final_G,key)
                
        if not(all([Mod[key][0] == Mod[key][0] for key in Mod.keys()])):
                print 'Erreur les crater pr ls different filtre ne sont pas les meme'
                sys.exit()
        Error_G = Mod[Mod.keys()[0]][0]

        for i,df in enumerate(Mod.values()) :
                if i== 0 :
                        G = df[1]
                else :
                        G = G.merge(df[1],on= F.columns.tolist())
  
        ################################################
        # On rassemble les deux pandas dataframe

        G = G[~G.Name.isin(Error_L+Error_G)]
        L = L[~L.Name.isin(Error_L+Error_G)]
  
        Df = L.merge(G,on=F.columns.tolist())
        
        ################################################
        # Test pour savoir si on a fait du bon boulot

        Boolean_G = (~(Df.Lat==L.Lat).any()) | (~(Df.LI_min==L.LI_min).any())
        Boolean_L = (~(Df.Lat==G.Lat).any()) | (~(Df['GI_mean_'+Mod.keys()[0]]==G['GI_mean_'+Mod.keys()[0]]).any())

        if Boolean_G | Boolean_L :
            print 'Erreur dans le merging'
            sys.exit()

        ################################################
        # Saving du dataframe

        Df.to_csv(Path_Save+'/'+Carte_Final_L.split('.')[0][-7:]+'_'+ Carte_Gr[:-1] +'_'+ t +'.txt',sep=',',index=None)

    
        
    
    

  


