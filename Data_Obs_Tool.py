def Init(mac):
    ''' Initialize les repertoires source according to the good coputer'''
    
    if mac == 'thorey':
        Root = '/Users/thorey/Documents/These/Projet/FFC/'
    elif mac == 'clavius':
        Root = '/Users/clement/Documents/'

    return Root


def Obs_Lola(F,Path,Carte_Final):

    import Data_Tool as CDT
    import numpy as np
    import pandas as pd
    import sys

    ################################################
    # Load de la carte sous forme de 3 arrays
    # x_d correspond a un vecteur qui range les longitudes
    # y_d correspond a un vecteur qui range les longitudes
    # z_d correspond a une grosse matrice qui prend les valeurs du champ de gravite. Il est index par x_d en collone et y_d en ligne.

    x_d,y_d,z_d = CDT.Sample_Grd(Path,Carte_Final)
    error=[] # range les craters ou le script ne va pas fonctione pr quelqu'onque raison
    Name_s=[]
    Index=[]
    Stat=[]

    for i,elt in enumerate(F.Name): # Boucle sur les crater
        diametre=float(F.Diameter[i]) # Diametre du crater
        size_i = 0.98*float(F.Diameter[i]) # Defini le rayon int de la couronne
        size_e = 1.05*float(F.Diameter[i]) # Defini le rayon ext de la couronne
        lat_0 = float(F.Lat[i])
        long_0 = float(F.Long[i])

        try:
            x_1,y_1,z_1 = CDT.Select_Zoom_2(x_d,y_d,z_d,size_e,lat_0,long_0) # Select la restriction de l'enorme matrice a une fenetre carrer centre sur le centre du crater (lat0,long0) dans lequelle est inscrit un cercle de diametre size_e.
            mask,Int,Ext = CDT.Circular_Mask_2(x_1,y_1,z_1,diametre,lat_0,long_0) # Int_Crater, Ext_Crater. Renvoie un vecteur contenant les valeur a l'interieur et a l'exterireur du mask
            mask_c,Int_c,Ext_c = CDT.Couronne_Mask_2(x_1,y_1,z_1,size_i,size_e,lat_0,long_0)#mask,Int couronne, Ext couronne
            n_Int,bins_Int = CDT.Histo_Data(Int,0.01) # Bins a l'interieur du crater
            n_c,bins_c = CDT.Histo_Data(Int_c,0.01) # Bins ds une courone 0.98D - 1.05D
    
            pd_Int=pd.DataFrame(n_Int,columns=['n_Int']).join(pd.DataFrame(bins_Int,columns=['bins_Int']))# Utilie pour calcule le modal pd_Int.bins_Int[pd_Int.n_Int.idxmax()]
    
            pd_C=pd.DataFrame(n_c,columns=['n_c']).join(pd.DataFrame(bins_c,columns=['bins_c']))# Utilie pour calcule le modal pd_C.bins_c[pd_C.n_c.idxmax()]

            Stat.append([pd_Int.bins_Int[pd_Int.n_Int.idxmax()],Int.min(),pd_C.bins_c[pd_C.n_c.idxmax()],Int_c.max()])
            Name_s.append(F.iloc[i].tolist())
    
        except:
            error.append(elt)
            pass
   
    Pandas_Stat=pd.DataFrame(Stat,columns=['LI_mod','LI_min','LR_mod','LR_max'])#F tient pour Floor et R pour Rim
    Pandas_Name=pd.DataFrame(Name_s,columns=F.columns)
    C=Pandas_Name.join(Pandas_Stat)

    C['Floor_Depth'] = (C.LI_mod+C.LI_min)/2.0
    C['sigma_f'] = np.abs((C.LI_mod-C.LI_min)/2.0)
    C['Rim'] = (C.LR_mod+C.LR_max)/2.0
    C['sigma_R'] = np.abs((C.LR_mod-C.LR_max)/2.0)

    C['Depth'] = C.Rim-C.Floor_Depth
    C['Sigma_Depth'] = np.sqrt(C.sigma_f**2+C.sigma_R**2)

    return error,C


def Obs_Gravi(F,Path,Carte_Final,key):

    import Data_Tool as CDT
    import numpy as np
    import pandas as pd
    import matplotlib.pylab as plt
    import sys
    
    ################################################
    # Load de la carte sous forme de 3 arrays
    # x_d correspond a un vecteur qui range les longitudes
    # y_d correspond a un vecteur qui range les longitudesl
    # z_d correspond a une grosse matrice qui prend les valeurs du champ de gravite. Il est index par x_d en collone et y_d en ligne.
    x_d,y_d,z_d = CDT.Sample_Grd(Path,Carte_Final) 

    error=[] # range les craters ou le script ne va pas fonctione pr quelqu'onque raison
    Name_s=[] 
    Index=[]
    Stat=[]

    # Uncomment the following line si on veut tester l'algo sur seulement quelque craters
    #Crater_Select=['Taruntius','Beals','Vitello','Davy']
    #F=F[F.Name.isin(Crater_Select)]
    #F.index=np.arange(0,len(F),1)
    
    for i,elt in enumerate(F.Name): # boucle sur les craters
        diametre = float(F.Diameter[i])
        size_i = float(F.Diameter[i]) # Defini le rayon int de la couronne
        size_e = 2*float(F.Diameter[i]) # Defini le rayon ext de la couronne
        lat_0 = float(F.Lat[i])
        long_0 = float(F.Long[i])
        
        try: # permet de pas bugger le script si une erreur apparait
            diametre=float(F.Diameter[i])
            size_i = float(F.Diameter[i]) # Defini le rayon int de la couronne
            size_e = 2*float(F.Diameter[i]) # Defini le rayon ext de la couronne
            lat_0 = float(F.Lat[i])
            long_0 = float(F.Long[i])
    
            x_1,y_1,z_1 = CDT.Select_Zoom_2(x_d,y_d,z_d,size_e,lat_0,long_0) # Select la restriction de l'enorme matrice a une fenetre carrer centre sur le centre du crater (lat0,long0) dans lequelle est inscrit un cercle de diametre size_e.
            mask,Int,Ext = CDT.Circular_Mask_2(x_1,y_1,z_1,diametre,lat_0,long_0) # Int_Crater, Ext_Crater. Renvoie un vecteur contenant les valeur a l'interieur et a l'exterireur du mask
            mask_c,Int_c,Ext_c = CDT.Couronne_Mask_2(x_1,y_1,z_1,size_i,size_e,lat_0,long_0)#mask,Int couronne, Ext couronne
        
            n_Ext,bins_Ext = CDT.Histo_Data(Int_c,5) # Bins de 5 mGal a l'interieur du crater
            pd_Ext=pd.DataFrame(n_Ext,columns=['n_Ext']).join(pd.DataFrame(bins_Ext,columns=['bins_Ext'])) # Utilsie pour calcule le modal.

            Stat.append([Int.mean(),Int.max(),Int.min(),Int.std(),pd_Ext.bins_Ext[pd_Ext.n_Ext.idxmax()],Int_c.mean(),Int_c.std()])
            Name_s.append(F.iloc[i].tolist())
        
        except:
            error.append(elt)
            pass
        
    Pandas_Stat=pd.DataFrame(Stat,columns=['GI_mean_'+key,'GI_max_'+key,'GI_min_'+key,'GI_sigma_'+key,'GE_mod_'+key,'GE_mean_'+key,'GE_std_'+key])#E tient pour exterieur, I pour interieur
    Pandas_Stat['Local_Mean_'+key]=Pandas_Stat['GI_mean_'+key]-Pandas_Stat['GE_mean_'+key]
    Pandas_Name=pd.DataFrame(Name_s,columns=F.columns)
    C=Pandas_Name.join(Pandas_Stat)

    return error,C
