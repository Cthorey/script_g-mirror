#!/usr/bin/env python
#######################
# Ce script est utiliser pour creer des cartes globals. Permets de plotter les craters ( de differentes sortes)"


#######################
# Import des library

import os
import subprocess
import pandas as pd
import sys
import numpy as np
from Data_Load import *
from Plot import *    
from Histo_Mean import *



Racine = '/Users/thorey/Documents/These/Projet/FFC/Gravi_GRAIL/Traitement/Script_G/'
Name_fig = 'Figure1'
obj = Load_G()
obj.Lola = 'LDEM_64_'
obj.Range = 20,184

mask_F = ['F_H_30','F_M_30','F_SPA']
mask_C = ['C_H_30','C_M_30','C_SPA']

map(os.remove,[elt for elt in os.listdir(Racine) if elt.split('.')[-1]=='xy'])
mask = obj.m_mask_F_C(mask_F,mask_C)
for key,df in mask.iteritems():
    df[['Long','Lat','Diameter']].to_csv(Racine+key+'.xy',sep=',',index=None,header=None)
    
file = [f for f in os.listdir(Racine) if f.split('.')[-1]=='xy']
Name_fig1 = file[0].split('.')[0]+'_'+file[3].split('.')[0]
Name_fig2 = file[1].split('.')[0]+'_'+file[4].split('.')[0]
Name_fig3 = file[2].split('.')[0]+'_'+file[5].split('.')[0]


#######################
# A definir
Name_root = '/Users/thorey/Documents/These/Projet/FFC/Gravi_GRAIL/Resultat/Global_Fig/'
C1='/Users/thorey/Documents/These/Projet/FFC/Gravi_GRAIL/Grail_Dataset/USGS_Mare_Basalts/USGS_mare_basalts_2.gmt'

#######################
# On creer les fichier .xy
obj = Load_G()
obj.Lola = 'LDEM_64_'
obj.Range = 20,184

#######################
# Ecriture du fichier temporaire bash

with open( str(Racine)+'Global_Fig_Paper.sh' , 'r') as script:
    with open(str(Racine)+'Global_Fig_Paper'+'_tmp.sh', 'wr+') as script_tmp:
        for l in script:
            if l == 'Title'+str(1)+'=titre\n':
                to_write = l.replace('titre','Bg30WSen:."{0:s}".:'.format(Name_fig1))
            elif l == 'Title'+str(2)+'=titre\n':
                to_write = l.replace('titre','Bg30WSen:."{0:s}".:'.format(Name_fig2))
            elif l == 'Title'+str(3)+'=titre\n':
                to_write = l.replace('titre','Bg30WSen:."{0:s}".:'.format(Name_fig3))
            elif l == 'Name=Image\n':
                to_write = l.replace('Image',Name_root+Name_fig)
            elif l == 'position=Position_Mer\n':
                to_write = l.replace('Position_Mer',C1)
            elif l == 'position'+str(11)+'=Position\n':
                to_write = l.replace('Position',Racine+file[0])
            elif l == 'position'+str(12)+'=Position\n':
                to_write = l.replace('Position',Racine+file[3])
            elif l == 'position'+str(21)+'=Position\n':
                to_write = l.replace('Position',Racine+file[1])
            elif l == 'position'+str(22)+'=Position\n':
                to_write = l.replace('Position',Racine+file[4])
            elif l == 'position'+str(31)+'=Position\n':
                to_write = l.replace('Position',Racine+file[2])
            elif l == 'position'+str(32)+'=Position\n':
                to_write = l.replace('Position',Racine+file[5])
            else:
                to_write = l
            script_tmp.write(to_write)
    subprocess.call(str(Racine)+'/Global_Fig_Paper'+'_tmp.sh',shell=True)
