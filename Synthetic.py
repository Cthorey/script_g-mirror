import sys,os
import matplotlib.pylab as plt
import numpy as np
import pandas as pd
import Fig_Tool as FST
from mpl_toolkits.axes_grid1 import make_axes_locatable
from Data_Load import *
from Plot import *
from Histo_Mean import *
import scipy.stats as st
from sympy import latex
import StringIO #Used as buffer

class Synth(HistoMean):

    def __init__(self,mask_C,mask_F):
        Load_G.__init__(self)
        Plot.__init__(self)
        self.mask_C = mask_C
        self.mask_F = mask_F
        #self.mode = [str(mod) for mod in np.arange(10,50,10)]+[str(mod) for mod in np.arange(60,120,20)]
        self.mode = ['10']
            
    def m_Substract_Baseline(self):

        mask = self.m_S_mask_F(self.mask_F)
        for mod in self.mode:
            print 'mod',mod,'rr'

            obj = HistoMean(self.mask_C,self.mask_F,[mod])
            obj.bin = 20
            tab = obj.m_table()
        
            for key,df in mask.iteritems():
                print key,len(df)
                if key == 'F_SPA':
                    key_C = 'C_SPA'
                elif key == 'F_M':
                    key_C = 'C_M'
                elif key == 'F_H':
                    key_C = 'C_F_H'
                C_h = tab[tab.Loc == key_C]
                C_h.reindex(index=np.arange(0,len(C_h)))
                
                def my_test(a,b):
                    for i,l in enumerate(C_h.Diameter):
                        if (a<=l+obj.bin/2.0) & (a>=l-obj.bin/2.0):
                            return b-C_h['mean_'+mod][i]
                df['N_Local_Mean_'+str(mod)] = df.apply(lambda row: my_test(row['Diameter'],row['Local_Mean_'+mod]), axis=1)

        return mask


    def m_table_paper(self,mod):
        
        ''' Retunr a table for the paper concerning the crustal anomlay'''

        columns = ['N',r'$\mu_{\delta_g^*}$',r'SE$_{\delta_g^*}$',r'$\mu^S_{\delta_g}$',r'SE$_{\delta_g^S}$',r'$\Delta \rho$',r'SE$_{\Delta \rho}$']
        mask = self.m_Substract_Baseline()
        tab = dict.fromkeys(columns)
        
        a=[]
        for key,df in mask.iteritems():
            Density = df['N_Local_Mean_'+mod]/df['S_mean_'+mod]
            tab_tmp =map(lambda x: '%3.5f' % x,[df['N_Local_Mean_'+mod].mean(),df['N_Local_Mean_'+mod].std()/np.sqrt(len(df)),df['S_mean_'+mod].mean(),df['S_mean_'+mod].std()/np.sqrt(len(df)),Density.mean(),Density.std()/np.sqrt(len(Density))])
            a.append([len(df)]+tab_tmp)
              
        return  pd.DataFrame(a,index=mask.keys(),columns=columns)

