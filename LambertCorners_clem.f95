
SUBROUTINE lambert(radius_a,lat0_a,long0_a,latll,longll,lattr,longtr,gmt)

  Implicit none
	
  real(8), intent(in)               :: radius_a, lat0_a, long0_a
  real(8)                           :: radius,lat0,long0
  real(8), intent(out)              :: latll, longll,lattr,longtr
  real(8)                           :: pi,x,y,bot,rho,c,kp
  real(8)                           :: lon,lat
  character(120)                    :: lons, lats
  character(120), intent(out)       :: gmt

	pi = acos(-1.0d0)
	
	gmt = "R"
	
	radius = radius_a * pi / 180.0d0
	
	lat0 = lat0_a*pi/180.0d0
	long0 = long0_a*pi/180.0d0
		
        kp = 1.0d0 + sin(lat0)*sin(lat0-radius) + cos(lat0)*cos(lat0-radius)*cos(long0-long0)
 
	kp = sqrt(2.0d0 / kp)

	
	bot = kp
	bot = bot * ( cos(lat0)*sin(lat0-radius) - sin(lat0)*cos(lat0-radius) )
	
	x = bot
	y = bot
	
	rho = sqrt(x**2 + y**2)
	c = 2.0d0 * asin(rho/2.0d0)
	
	latll = asin(cos(c)*sin(lat0)  + y*sin(c)*cos(lat0)/rho ) * 180.0d0 / pi
	
	lon = long0  + atan2(x*sin(c), rho*cos(lat0)*cos(c) - y*sin(lat0)*sin(c))
	longll = lon * 180.0d0 / pi
	
!	print*, "Lower left coordinates (lat, long) = ", lat, lon
	
	write(lons,"(f9.4)")  longll
	write(lats,"(f9.4)")  latll
	lons = adjustl(lons)
	lats = adjustl(lats)
	gmt = trim(gmt)//trim(lons)//"/"//trim(lats)//"/"
	
	x = -bot
	y = -bot
	
	rho = sqrt(x**2 + y**2)
	c = 2.0d0 * asin(rho/2.0d0)
	
	lattr = asin(cos(c)*sin(lat0)  + y*sin(c)*cos(lat0)/rho ) * 180.0d0 / pi
	
	lon = long0  + atan2(x*sin(c), rho*cos(lat0)*cos(c) - y*sin(lat0)*sin(c))
	longtr = lon * 180.0d0 / pi

!	print*, "Upper right coordinates (lat, long) = ", lat, lon
	
	write(lons,"(f9.4)")  Longtr
	write(lats,"(f9.4)")  lattr
	lons = adjustl(lons)
	lats = adjustl(lats)
	
	gmt = trim(gmt)//trim(lons)//"/"//trim(lats)//"r"
	
	!print*, "GMT string = ", gmt
	

END SUBROUTINE lambert



	
