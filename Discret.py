import Fig_Tool as FST
import numpy as np
import pandas as pd
import matplotlib.pylab as plt
from matplotlib.patches import Polygon
import os
import matplotlib.gridspec as gridspec
from Data_Load import *
from Plot import *

class Discret(Load_G,Plot):

    '''
    Set de function et d'attribut that characterize craters.

    attribut :
    Name : Liste containing crater name

    col : Column that we want to examine. If we want to consider gravity,
    put the name of the column +_, e.g 'GE_mean_' or 'GI_mean_'. Instead,
    when considerign a column with no filter dependance, put 'Depth','Class'.
    
    model = List that contains the value of lambda we want to
    consider, e,g [10,20] will considere the value lambda = 10 and
    lambda =20 for the low pass filter. Let blank if considering the
    depth or other column that do not depend upon the filter

    glim : tupple( min,max) for the Image plot.e.g. -50,50 

    method:

    m_load(name) : take the crater name and return the DataFrame
    corresponding the the class FFC ou UC
    
    m_degree: method that return a table with three col:
    Name,Diameter,the minimum value of l that contribute to the signal

    m_table :Renvoie une table comme celle dessous pour avec Name def
    dans FFC. Lambda correspond to the value of the filter
    Name  Diameter  GE_mean_10  GE_mean_20  GE_mean_30  GE_mean_40
    Taruntius 56   75.533463   73.843262    50.29911   27.157936

    m_plot : return a plot Crustal_anomaly versus filter model for
    different crater name.

    m_Image : return a two column plot, with each row, on the right
    the topo and on the left the gravi corresponding to one value of the
    filter lambda. This method return an error if obj.Name > a 1.
    Valable seulement pour un crater.
    '''

    def __init__(self,Name):
        Load_G.__init__(self)
        Plot.__init__(self)
        self.Name = Name
        self.col = 'GE_mean_'
        self.model = [10,20,30,40,60,80,100]
        self.x_lim = 0,120
        self.loc = 'upper right'
        self.y_lim = -10,100
        self.g_lim = -50,50

    def m_load(self,name):
        if name.split('_')[0] == 'C':
                F = self.m_load_C()
        else :
                F = self.m_load_F()
        return F
        
    def m_degree(self):
        '''According to the diameter of the input name crater,
        it gives the minimum value of l that contribute to the signal.'''
        
        tab = []
        for elt in self.Name:
            F = self.m_load(elt)
            tab.append([elt,F.iloc[np.float(F[F.Name==elt].index.tolist()[0])]['Diameter']
                        ,np.float(2*np.pi*1734/(2*F[F.Name == elt].Diameter))])
            
        return pd.DataFrame(tab,columns = ['Name','Diameter','Degree min'])

    def m_table(self):

        '''Renvoie une table comme celle dessous pour avec Name def
        dans FFC. Lambda correspond to the value of the filter
        Name  Diameter  GE_mean_10  GE_mean_20  GE_mean_30  GE_mean_40
        Taruntius 56   75.533463   73.843262    50.29911   27.157936
        '''
        cols = [self.col+str(mod) for mod in self.model]
        cols = ['Name','Diameter']+cols
        tab=[]
        for i,name in enumerate(self.Name):
            F = self.m_load(name)
            tab.append(F[cols][F.Name.isin([name])])
        return pd.concat(tab)

    def m_plot(self):

        '''plot self.col(lambda : that characterize the filter)
        self.col correspond to the column in the dataframe we want to plot. For
        example GI_mean or Local_Mean...'''

        fig,ax = self.m_figure()
        c = self.m_color_map(len(self.Name))
        cols = [self.col+str(mod) for mod in self.model]
        for i,name in enumerate(self.Name):
            F = self.m_load(name)
            F['size'] = 200
            ax.scatter(np.array(self.model),np.array(F[cols][F.Name.isin([name])])[0],s=200,label=str(name),color = c[i])
        self.m_axes(ax)
        self.m_save_fig(fig)
            

    def m_Image(self):
        
        '''This method plot a grid plot, 2 columns (topo)+(gravi) and a
        number x of line defined as x = len(Iter). Iter is a vector containing the values of
        lambda (filter) we want to plot. It is defined by the global varialbe Taper.Model.'''

        if len(self.Name)!=1 :
            print 'Error: plot_Im_FFC accept only one argument'
            pass

        fig=plt.figure(figsize = self.figsize) # create the top-level container
        Nb_Ligne = len(self.model) # Nombre de ligne = Nombre de crater a plotter
        gs=gridspec.GridSpec(Nb_Ligne,2) # create a GridSpec object avec 2 colonne et N lignes
        ax=[[plt.subplot(gs[f,g]) for f in np.arange(0,Nb_Ligne)] for g in [0,1]] # creer une liste de liste a ax[0] relatif a la colone de gauche et ax[1] relatif a la colonne de droite.
     
        for i,mod in enumerate(self.model): # Rajoute a la suite les crater normaux que l'onv eut plotter
            Path = '/Users/thorey/Documents/These/Projet/FFC/Gravi_GRAIL/Traitement/GRAV/JGGRAIL_900C9A_CrustAnom_'+str(mod)+'/Global_map'
            Carte_G = '34_12_3220_900_'+str(mod)+'_misfit_rad.grd' #Gravi
            Carte_L = '34_12_3220_660_80_misfit_Lola.grd' # Lola
            
            x_l,y_l,z_l = FST.Sample_Grd(Path,Carte_L)
            x_g,y_g,z_g = FST.Sample_Grd(Path,Carte_G)
            
            F = self.m_load(self.Name[0])
            F = F[F.Name.isin(self.Name)]
            F.index=np.arange(0,len(F),1)
            error=[]
            Name_s=[]
            Index=[]
            size_i = float(F.Diameter)  # Defini le rayon Int de la couronne
            size_e = 2*float(F.Diameter) # Defini le rayon Ext de la couronne autour du rim
            size_f = float(F.Diameter)
            lat_0 = float(F.Lat) # lat central
            long_0 = float(F.Long) # long central

            Fen_l,X_l,Y_l,data_l=FST.Select_Zoom_2(x_l,y_l,z_l,size_f,lat_0,long_0) # Fen contient les lat_basgauche/long_basgauche/lat_hautdroit/Long_Haut droit. Fenetre de lambert centre sur le centre du crater et dans laquelle est inscrit un cercle de rayon le cercle du crater (size_e)
            Fen_g,X_g,Y_g,data_g=FST.Select_Zoom_2(x_g,y_g,z_g,size_f,lat_0,long_0)
    
            #mask,Int_c,Ext_c = FST.Couronne_Mask_2(X_g,Y_g,data_g,size_i,size_e,lat_0,long_0) #mask,Int couronne, Ext couronne
            #data_g[mask]=0 # Permet de mettre toutes les valeur a l'int du mask egale a zero pour definir le rayon sur la fig.
            #mask,Int,Ext = FST.Circular_Mask_2(X_g,Y_g,data_g,float(F.Diameter),lat_0,long_0) # Int_Crater, Ext_Crater. Renvoie un vecteur contenant les valeur a l'interieur et a l'exterireur du mask
            mask_c,Int_c,Ext_c = FST.Couronne_Mask_2(X_g,Y_g,data_g,size_i,size_e,lat_0,long_0)#mask,Int couronne, Ext couronne
            
            
            FST.Plot_Lambert(F.Name[0]+'_'+str(mod),float(F.Diameter[0]),lat_0,long_0,Fen_l,X_l,Y_l,data_l,ax[0][i],[data_l.min().min(),data_l.max().max()],r'Topography (km)',self.font) # Plot de la fig lola
            FST.Plot_Lambert(F.Name[0]+'_'+str(mod),float(F.Diameter[0]),lat_0,long_0,Fen_g,X_g,Y_g,data_g,ax[1][i],self.g_lim,r'Gravity Anomay (mGals)',self.font) # Plot de la fig gravi
            FST.tick(ax[0][i],self.font)
            FST.tick(ax[1][i],self.font)
            
        self.m_save_fig(fig)

    def m_Image_2(self):
        
        '''This method plot a grid plot, 2 columns (topo)+(gravi) and a
        number x of line defined as x = len(Iter). Iter is a vector containing the values of
        lambda (filter) we want to plot. It is defined by the global varialbe Taper.Model.'''

        if len(self.Name)!=1 :
            print 'Error: plot_Im_FFC accept only one argument'
            pass

        fig=plt.figure(figsize = self.figsize) # create the top-level container
        Nb_Ligne = len(self.model) # Nombre de ligne = Nombre de crater a plotter
        gs=gridspec.GridSpec(2,3) # create a GridSpec object avec 2 colonne et N lignes
        ax=[[plt.subplot(gs[f,g]) for f in [0,1]] for g in np.arange(0,3,1)] # creer une liste de liste a ax[0] relatif a la colone de gauche et ax[1] relatif a la colonne de droite.
        
        F = self.m_load(self.Name[0])
        F = F[F.Name.isin(self.Name)]
        F.index=np.arange(0,len(F),1)
        size_i = 0.98*float(F.Diameter)  # Defini le rayon Int de la couronne
        size_e = 1.05*float(F.Diameter) # Defini le rayon Ext de la couronne autour du rim
        size_f = 3*float(F.Diameter)
        lat_0 = float(F.Lat) # lat central
        long_0 = float(F.Long) # long central

        Path_L = '/Users/thorey/Documents/These/Projet/FFC/Gravi_GRAIL/Traitement/LOLA/'
        Carte_L = '34_12_3220_660_80_misfit_Lola.grd' # Lola
        x_l,y_l,z_l = FST.Sample_Grd(Path_L,Carte_L)
        Fen_l,X_l,Y_l,data_l=FST.Select_Zoom_2(x_l,y_l,z_l,size_f,lat_0,long_0) # Fen contient les lat_basgauche/long_basgauche/lat_hautdroit/Long_Haut droit. Fenetre de lambert centre sur le centre du crater et dans laquelle est inscrit un cercle de rayon le cercle du crater (size_e)
        FST.Plot_Lambert(self.Name[0]+'_'+str(float(F.Diameter))+'km',float(F.Diameter[0]),lat_0,long_0,Fen_l,X_l,Y_l,data_l,ax[0][0],[data_l.min().min(),data_l.max().max()],r'Topography (km)',self.font) # Plot de la fig lola
        FST.tick(ax[0][0],self.font)

        i=1
        for mod in self.model: # Rajoute a la suite les crater normaux que l'onv eut plotter
            k = i//3
            j = i%3
            print i,k,j
            Path = '/Users/thorey/Documents/These/Projet/FFC/Gravi_GRAIL/Traitement/GRAV/JGGRAIL_900C9A_CrustAnom_'+str(mod)+'/Global_map'
            Carte_G = '34_12_3220_900_'+str(mod)+'_misfit_rad.grd' #Gravi
            x_g,y_g,z_g = FST.Sample_Grd(Path,Carte_G)
            Fen_g,X_g,Y_g,data_g=FST.Select_Zoom_2(x_g,y_g,z_g,size_f,lat_0,long_0)
    
            mask,Int_c,Ext_c = FST.Couronne_Mask_2(X_g,Y_g,data_g,size_i,size_e,lat_0,long_0) #mask,Int couronne, Ext couronne
            data_g[mask]=0 # Permet de mettre toutes les valeur a l'int du mask egale a zero pour definir le rayon sur la fig.
    
            FST.Plot_Lambert(r'$\lambda$='+str(mod),float(F.Diameter[0]),lat_0,long_0,Fen_g,X_g,Y_g,data_g,ax[j][k],self.g_lim,r'Gravity Anomay (mGals)',self.font) # Plot de la fig gravi
            FST.tick(ax[j][k],self.font)
            i+=1
            
        self.m_save_fig(fig)
        

    def m_Image_Name(self,model):
        
        '''This method plot a grid plot, 2 line (topo)+(gravi) and a
        number x of column defined as x = len(Name). self.Name is a vector containing the crater
        we want to plot..'''

        if len(self.model)>1 :
            print 'Error: plot_Image_Name accept only one model'
            pass

        fig=plt.figure(figsize = self.figsize) # create the top-level container
        Nb_col = len(self.Name) # Nombre de ligne = Nombre de crater a plotter
        gs=gridspec.GridSpec(2,Nb_col) # create a GridSpec object avec 2 colonne et N lignes
        ax=[[plt.subplot(gs[f,g]) for f in [0,1]] for g in np.arange(0,Nb_col,1)] # creer une liste de liste a ax[0] relatif a la colone de gauche et ax[1] relatif a la colonne de droite.
     
        for i,name in enumerate(self.Name): # Rajoute a la suite les crater normaux que l'onv eut plotter
            Path = '/Users/thorey/Documents/These/Projet/FFC/Gravi_GRAIL/Traitement/GRAV/JGGRAIL_900C9A_CrustAnom_'+str(model)+'/Global_map'
            Carte_G = '34_12_3220_900_'+str(model)+'_misfit_rad.grd' #Gravi
            Carte_L = '34_12_3220_660_80_misfit_Lola.grd' # Lola
            
            x_l,y_l,z_l = FST.Sample_Grd(Path,Carte_L)
            x_g,y_g,z_g = FST.Sample_Grd(Path,Carte_G)
            
            F = self.m_load(self.Name[0])
            F = F[F.Name.isin([name])]
            F.index=np.arange(0,len(F),1)
            error=[]
            Name_s=[]
            Index=[]
            size_i = 0.98*float(F.Diameter)  # Defini le rayon Int de la couronne
            size_e = 1.05*float(F.Diameter) # Defini le rayon Ext de la couronne autour du rim
            size_f = 3*float(F.Diameter)
            lat_0 = float(F.Lat) # lat central
            long_0 = float(F.Long) # long central

            Fen_l,X_l,Y_l,data_l=FST.Select_Zoom_2(x_l,y_l,z_l,size_f,lat_0,long_0) # Fen contient les lat_basgauche/long_basgauche/lat_hautdroit/Long_Haut droit. Fenetre de lambert centre sur le centre du crater et dans laquelle est inscrit un cercle de rayon le cercle du crater (size_e)
            Fen_g,X_g,Y_g,data_g=FST.Select_Zoom_2(x_g,y_g,z_g,size_f,lat_0,long_0)
    
            mask,Int_c,Ext_c = FST.Couronne_Mask_2(X_g,Y_g,data_g,size_i,size_e,lat_0,long_0) #mask,Int couronne, Ext couronne
            data_g[mask]=0 # Permet de mettre toutes les valeur a l'int du mask egale a zero pour definir le rayon sur la fig.
    
            FST.Plot_Lambert(F.Name[0]+'_'+str(model),float(F.Diameter[0]),lat_0,long_0,Fen_l,X_l,Y_l,data_l,ax[i][0],[data_l.min().min(),data_l.max().max()],r'Topography (km)',self.font) # Plot de la fig lola
            FST.Plot_Lambert(F.Name[0]+'_'+str(model),float(F.Diameter[0]),lat_0,long_0,Fen_g,X_g,Y_g,data_g,ax[i][1],self.g_lim,r'Gravity Anomaly (mGals)',self.font) # Plot de la fig gravi
            FST.tick(ax[i][0],self.font)
            FST.tick(ax[i][1],self.font)
            
        self.m_save_fig(fig)

        
class SH(object):
    ''' Representation du spectre d'harmonique sphere que l'on considere
    en fonction de la longueur d'onde. En surrimpression apparait la zone
    definit par le degree minmimum et maximum que l'onconside pour les
    coeff spherique

    parametre :
       - range : range des longueurs d'ondes

    attribut :
        - y_lim = boundar for the y axis
        - x_lim = boundary for the x axis
        - x_label = name of the x_label
        - y_label = name of the y_label
        - min_l = min l for spherical harmonic
        - max_l = max l for spherical harmonic

    method :
        - __degree(self,list): Prend un argument une liste de longeur d'onde
        Renvoie l'equivalent en degree l d'harmonicque spherique
        - plot(self) : Plot lambda(l) pour le l defini par min_l et max_l

    Example : Voir notebook Model Crust Anomaly
    '''
       
    def __init__(self,range):
        self.range = range
        self.y_lim = 0,400
        self.x_lim = 0,900
        self.x_label = None
        self.y_label = None
        self.min_l = 26
        self.max_l = 778
    
    def __degree(self,list):
        
        return 2*np.pi*1734/list
        
    def plot(self):
        
        fig, ax = plt.subplots(figsize=(8,5))
        ax.plot(self.range,self.__degree(self.range),color = 'b')
        # Make the shaded region
        a=self.min_l;b=self.max_l
        ix = np.linspace(a,b)
        iy = self.__degree(ix)
        verts = [(a, 0)] + list(zip(ix, iy)) + [(b, 0)]
        poly = Polygon(verts, facecolor='r', edgecolor='r',alpha=0.6)
        ax.add_patch(poly)
        ax.set_ylim(self.y_lim)
        ax.set_xlabel(self.x_label)
        ax.set_ylabel(self.y_label)
