#!/usr/bin/env python

import os
import subprocess

path='/Users/clement/Documents/Gravi_GRAIL/Traitement/GRAV/'
folder = os.listdir(path)
comp = 'total' 
model = [elt.split('_')[-1] for elt in os.listdir(path)]

for i,mod in enumerate(model):

    with open('MakeGRD.sh', 'r') as script:
        with open('MakeGRD_tmp.sh', 'wr+') as script_tmp:
            for l in script:
                if l == 'Scarte1=Carte1\n':
                    to_write = l.replace('Carte1',str(path)+folder[i]+'/Global_map/'+'34_12_3220_900_'+mod+'_misfit_'+comp+'.dat')
                elif l == 'Ccarte1=Carte1\n':
                    to_write = l.replace('Carte1',str(path)+folder[i]+'/Global_map/'+'34_12_3220_900_'+mod+'_misfit_'+comp+'.grd')
                else:
                    to_write = l
                script_tmp.write(to_write)

        subprocess.call('./MakeGRD_tmp.sh',shell=True)
