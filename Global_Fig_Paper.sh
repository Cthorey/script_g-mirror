
gmt gmtset PS_PAGE_ORIENTATION	= portrait
gmt gmtset MAP_ANNOT_OFFSET_PRIMARY = 0.2c
gmt gmtset MAP_LABEL_OFFSET = 0.1c
gmt gmtset PS_MEDIA =  tabloid

Title1=titre
Title2=titre
Title3=titre

Name=Image
position=Position_Mer
position11=Position
position12=Position
position21=Position
position22=Position
position31=Position
position32=Position

Projection=JW0/9i

# carte

gmt psbasemap -R0/360/-90/90 -$Projection -X3 -Y28 -$Title1 -K -P -V > $Name.ps
gmt psxy $position -$Projection  -R-180/180/-90/90 -V -f0x,1y -L -K -O >> $Name.ps
gmt psxy $position11 -$Projection -R-180/180/-90/90 -Sc0.1i -Gblue -W -K -O -V >> $Name.ps
gmt psxy $position12 -$Projection -R-180/180/-90/90  -St0.2i -Gred -K -O -V >> $Name.ps

gmt psbasemap -R0/360/-90/90 -$Projection -Y-14 -$Title2 -K -P -V -O >> $Name.ps
gmt psxy $position -$Projection  -R-180/180/-90/90 -V -f0x,1y -L -K -O >> $Name.ps
gmt psxy $position21 -$Projection -R-180/180/-90/90 -Sc0.1i -Gblue -W -K -O -V >> $Name.ps
gmt psxy $position22 -$Projection -R-180/180/-90/90  -St0.2i -Gred -K -O -V >> $Name.ps

gmt psbasemap -R0/360/-90/90 -$Projection -Y-14 -$Title3 -K -P -V -O >> $Name.ps
gmt psxy $position -$Projection  -R-180/180/-90/90 -V -f0x,1y -L -K -O >> $Name.ps
gmt psxy $position31 -$Projection -R-180/180/-90/90 -Sc0.1i -Gblue -W -K -O -V >> $Name.ps
gmt psxy $position32 -$Projection -R-180/180/-90/90  -St0.2i -Gred -K -O -V >> $Name.ps

convert $Name.ps $Name.png
convert $Name.png -crop 800x900+10+10 $Name.png

#rm *.xy
rm *.ps
rm *.cpt

open $Name.pdf
