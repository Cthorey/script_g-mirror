
gmt gmtset PS_PAGE_ORIENTATION	= portrait
gmt gmtset MAP_ANNOT_OFFSET_PRIMARY = 0.2c
gmt gmtset MAP_LABEL_OFFSET = 0.1c
gmt gmtset PS_MEDIA =  tabloid

Title=titre
Scarte1=Carte1
SLegend1=legend
SScale1=Echelle
SColormap1=col

Scarte2=Carte2
SLegend2=legend
SScale2=Echelle
SColormap2=col

Name=Image
position1=Position_Mer
position2=Position
position3=Position

color=Gred
Symbol=Sa0.125i
Projection=JW0/9i
Scale_Diameter='-900/900/1'
Legend_Crater=Bf100a100:"Mean_crustal_anomaly_(mGal))":

gmt makecpt -C$SColormap1 -T$SScale1 -V -Z > carte1.cpt
gmt makecpt -C$SColormap2 -T$SScale2 -V -Z > carte2.cpt
gmt makecpt -C$SColormap1 -T$SScale1 -V -Z > carte4.cpt

# carte

gmt psbasemap -R0/360/-90/90 -$Projection -X3 -Y28 -$Title -K -P -V > $Name.ps
#gmt grdimage $Scarte1 -Ccarte1.cpt -$Projection -R -Bg30WSen -O -K -V -Ei >> $Name.ps
gmt psxy $position1 -$Projection  -R-180/180/-90/90 -V -f0x,1y -L -K -O >> $Name.ps
gmt psxy $position2 -$Projection -R-180/180/-90/90 -Sc0.1i -Gblue -W -K -O -V >> $Name.ps
gmt psxy $position3 -$Projection -R-180/180/-90/90  -St0.2i -Gred -K -O -V >> $Name.ps
#gmt psscale -Ccarte1.cpt -$SLegend1 -D10/-.5/20/.5h -E -O -K -V -X1.35 >>  $Name.ps

#gmt psbasemap -R0/360/-90/90 -$Projection -Y-14 -X-1.35 -Bg30WSen -O -K -V >> $Name.ps
#gmt grdimage $Scarte2 -Ccarte2.cpt -$Projection -R0/360/-90/90 -Bg30WSen -O -K -V -Ei >> $Name.ps
#gmt psxy $position1 -$Projection  -R-180/180/-90/90 -V -f0x,1y -L -K -O >> $Name.ps
#gmt psxy $position2 -$Projection -R-180/180/-90/90 -Ccarte4.cpt -Sc0.1i -K -O -V >> $Name.ps
#gmt psscale -Ccarte4.cpt -$SLegend1 -D10/-.5/20/.5h -E -O -K -V -X1.35 >>  $Name.ps

convert $Name.ps $Name.png
convert $Name.png -crop 800x430+10+10 $Name.png

#rm *.xy
rm *.ps
rm *.cpt

open $Name.pdf
