import sys,os
import matplotlib.pylab as plt
import numpy as np
import pandas as pd
import Fig_Tool as FST
from mpl_toolkits.axes_grid1 import make_axes_locatable
    
class Plot(object):

    ''' The aim of this class is to help making plot  !!
    Class parameter:
    Path_Save = Path where save the plots
    attribut :
        ...
        - fig_b : IF True, save the plot in Load_G.Path_Save
        - fig_name : Name of the figure if the plot is saved

    method :
    - color_map(N): Make a list that can be used in
    iteration plot, N in the size of the iteration

    - axes(ax): make beautiful axes :)

    - figure() : creer une figure

    - save_fig() : routine to save fig. '''

    Path_Save = '/Users/thorey/Documents/These/Projet/FFC/Gravi_GRAIL/Resultat/Notebook_GRAIL/Image/'
    
    def __init__(self):
    
        self.colormap = plt.cm.hsv
        self.x_lim = [0,200]
        self.y_lim = [-20,20]
        self.x_label = r'Maxmimum value for $\sigma$ outside the crater'
        self.y_label = r'Mean local crust anomaly $\delta_{g}$'
        self.colormap = plt.cm.gist_ncar
        self.markersize = 200
        self.figsize = 8,6
        self.fig_b = False
        self.fig_name = 'fig1'
        self.font = 18
        self.loc = 'upper right'
        self.alpha = 0.5


    def m_color_map(self,N):
        
        c=[self.colormap(i) for i in np.linspace(0,1,N)]
        c = ['b','r','g','c','m','y','k']
        return c

    def m_axes(self,ax):
        ax.set_xlabel(self.x_label,size=self.font)
        ax.set_ylabel(self.y_label,size=self.font)
        ax.set_xlim(self.x_lim)
        ax.set_ylim(self.y_lim)
        FST.tick(ax,self.font)
        FST.legd_Scatter(ax,self.loc,self.font)

    def m_axes_Ss_L(self,ax):
        ax.set_xlabel(self.x_label,size=self.font)
        ax.set_ylabel(self.y_label,size=self.font)
        ax.set_xlim(self.x_lim)
        ax.set_ylim(self.y_lim)
        FST.tick(ax,self.font)
        
    def m_figure(self):
            
        fig, ax = plt.subplots(figsize=self.figsize)
        return fig,ax

    def m_save_fig(self,fig):
        
        if self.fig_b:
            fig.savefig(Plot.Path_Save+'_'+self.fig_name+'.eps',rasterized=True, dpi=300)
