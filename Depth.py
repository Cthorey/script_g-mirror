import sys,os
import matplotlib.pylab as plt
import numpy as np
import pandas as pd
import Fig_Tool as FST
from mpl_toolkits.axes_grid1 import make_axes_locatable
from Data_Load import *
from Plot import *    
from Histo_Mean import *
from sympy import latex
import StringIO #Used as buffer



class Depth(Load_G,Plot):
    '''
    global :
       Same than Load_G

    attribut :
        col_err  : Name of the colum containing the error on the depth = 'Sigma_Depth' 
        err_max :  Error maximum allowed for the dataset !
        col = 'Thick_C' Name of the collone we use to calculate the thickness
        of the intrusion. Thick_C derived from the data, Thick_K calculate from the Kalynn scalin
        g laws.

    method :
        m_Coeff_Fit(self): Method that return a dict corresponding to mask
        with the values for A and B for the fit in the power law A*D**B with
        D the diameter. mask is a dict with keys the name of the location and values
        the corresponding dataframe.

        m_table_thickness(self): Return a dict that contains the keys from mask that are
        FFC locations and the values corresponding to a vector [A,B]
            example :{'C_H_SPA': [0.68135345470221709, 0.41666223160479554],
                      'C_MARIA': [0.32545523618216815, 0.51021076719164349],
                      'F_H_SPA': [0.045934239381247903, 1.0681107790804083],
                      'F_MARIA': [0.092760607203796253, 0.77886144990856132]}
        
        m_table_thick(self): Retunr a table with 5 columns and each row correspondign to one
        FFC :
            example : 	Name	Diameter	Thick_C	Thick_K	Location
                107	 Schluter	 89.0	-0.787041	 0.222437	 F_MARIA
                108	 Briggs	 37.0	 1.006526	 2.053681	 F_MARIA
                109	 Hansteen	 45.0	 1.293389	 2.346024	 F_MARIA
        
        m_plot_thickness(self): Return a plot Intrusion Thickness versus Diameter.
        Don't forget to define self.col = 'Thick_K' or 'Thick_C' corresponding
        to whant you want ( see attribut)
        
        m_error_bar(self,mod): retunr a scatter plot depth versus Diameter
        with error bar corresponding to the different location defined
        in self.mask_C and self.mask_F. The corresponding scaling laws ( output of Coeff_Fit)
        are also shown
        
        m_plot(self,mod): retunr a scatter plot depth versus Diameter
        corresponding to the different location defined in self.mask_C and self.mask_F.
        The corresponding scaling laws ( output of Coeff_Fit)
        are also shown
        
        m_tab_bin(self,mod): Return a table with the Thickness versus Diameter
        separated in different bins. For example with, self.bin = 40. Mean correspond
        to the mean of self.col of the bean, std the standard deviation, ste the standar error
        and loc the location.
              example
                Diameter	Mean	Std	Size	StE	Loc
                0	 30	 1.940439	 0.800327	 37	 0.319006	 F_MARIA
                1	 130	 2.050016	 0.865043	 26	 0.402041	 F_MARIA
                0	 30	 2.192782	 0.780156	 88	 0.233751	 F_H_SPA
                1	 130	 2.101775	 0.878733	 53	 0.288701	 F_H_SPA
        
        m_plot_bin(self,mod): Return a plot intrusion thickenss versus Diameter with
        Diameter defined by the bins in tab_bin and the intrusion thickenss is the value of
        the mean in the overlying table.

        Example : Voir notebook DEPTH_UC_FFC
    '''
        
    def __init__(self,mask_C,mask_F):
        Load_G.__init__(self)
        Plot.__init__(self)
        self.col_err = 'Sigma_Depth'
        self.err_max = 100
        self.col = 'Thick_C'
        self.mask_C = mask_C
        self.mask_F = mask_F
        self.info = {}

    def m_Coeff_Fit(self):
        '''Method that return a dict corresponding to mask
        with the values for A and B for the fit in the power law A*D**B with
        D the diameter. mask is a dict with keys the name of the location and values
        the corresponding dataframe.'''
        
        mask = self.m_mask_F_C(self.mask_F,self.mask_C)
        tab = dict.fromkeys(mask)
        for keys,df in mask.iteritems():
            df = df[df.Sigma_Depth<self.err_max]
            x_1 = df.Diameter
            y_1 = df.Depth
            y_err = df.Sigma_Depth
            a,sigma_a,b,sigma_b = FST.LSM(np.log(x_1),np.log(y_1),y_err/y_1)
            sigma = np.std(y_1-np.exp(a)*x_1**b)
            tab[keys] = [np.exp(a),b,sigma]

        return tab

    def m_plot_power(self):
        '''Method that return a dict plot power law relationship.'''
        
        mask = self.m_mask_F_C(self.mask_F,self.mask_C)
        
        fig,ax = self.m_figure() # create the top-level container
        c = self.m_color_map(len(mask.keys()))
        
        dD = self.m_Coeff_Fit()
        i=0
        for key,coeff in dD.iteritems():
            print key,coeff
            x=np.arange(20,180,0.5)
            ax.plot(x,coeff[0]*x**coeff[1],color=c[i],linewidth = 3,label = key+': '+'d={0:1.3f}'.format(coeff[0])+r'$D^{'+'{0:1.3f}'.format(coeff[1])+r'}$')
            i+=1
        ax.plot(x,1.558*x**0.254,color='k',linewidth = 3)
        self.m_axes(ax)
        
            
        
    def m_table_thickness(self):
        ''' Return a dict that contains the keys from mask that are
        FFC locations and the values corresponding to a vector [A,B]
            example :{'C_H_SPA': [0.68135345470221709, 0.41666223160479554],
                      'C_MARIA': [0.32545523618216815, 0.51021076719164349],
                      'F_H_SPA': [0.045934239381247903, 1.0681107790804083],
                      'F_MARIA': [0.092760607203796253, 0.77886144990856132]}'''

        mask = self.m_mask_F_C(self.mask_F,self.mask_C)
        mask_F = { key: mask[key] for key in self.mask_F}
        dD = { key: self.m_Coeff_Fit()[key] for key in self.mask_C}

        for key,df in mask_F.iteritems():
            if key == 'F_SPA':
                key_C = 'C_SPA'
            elif key == 'F_M':
                key_C = 'C_M'
            else:
                key_C = 'C_F_H'
            df['Thick_C']= dD[key_C][0]*df.Diameter**dD[key_C][1] - df.Depth
            df['Sigma_Thick_C'] = np.sqrt(dD[key_C][2]**2+df.Sigma_Depth**2)


        return mask_F

    def m_table_Depth(self):
        ''' Retunr a table for the paper concerning the depth'''
        
        mask = self.m_mask_F_C(self.mask_F,self.mask_C)
        tab = dict.fromkeys(mask)
        coeff=self.m_Coeff_Fit()
        for key,df in mask.iteritems():
            print 
            if 'Thick_C' in df.columns:
                tab[key]= [df.Depth.mean(),df.Sigma_Depth.mean(),coeff[key][0],coeff[key][1],coeff[key][2],df.Thick_C.mean(),df.Sigma_Thick_C.mean()]
            else :
                tab[key]= [df.Depth.mean(),df.Sigma_Depth.mean(),coeff[key][0],coeff[key][1],coeff[key][2],'-','-']

        return  pd.DataFrame(tab,index=[r'$\mu_{depth}$ (km)','$\sigma_{depth}$ (km)','$A$ (km)','$B$','$\sigma_{fit}$ (km)','a','b'])

    def m_table_Depth_2(self):
        ''' Retunr a table for the paper concerning the depth'''

        columns = ['N',r'$\mu_{\text{depth}}$',r'$\sigma_{\text{depth}}$',r'$A$',r'$B$', r'$\sigma_{fit}$']
        mask = self.m_mask_F_C(self.mask_F,self.mask_C)
        tab = dict.fromkeys(columns)
        coeff=self.m_Coeff_Fit()
        a=[]
        for key,df in mask.iteritems():
            a.append([len(df.Depth),df.Depth.mean(),df.Sigma_Depth.mean(),coeff[key][0],coeff[key][1],coeff[key][2]])
              
        return  pd.DataFrame(a,index=mask.keys(),columns=columns)
    

    def m_table_thick(self):

        '''Retunr a table with 5 columns and each row correspondign to one
        FFC :
            example : 	Name	Diameter	Thick_C	Thick_K	Location
                107	 Schluter	 89.0	-0.787041	 0.222437	 F_MARIA
                108	 Briggs	 37.0	 1.006526	 2.053681	 F_MARIA
                109	 Hansteen	 45.0	 1.293389	 2.346024	 F_MARIA'''
        
        mask_F = self.m_table_thickness()
        tab = dict.fromkeys(mask_F)
        for key,df in mask_F.iteritems():
            tab[key]= df[['Name','Diameter','Thick_C','Sigma_Thick_C']]
            tab[key]['Location'] = key
        
        return  pd.concat(tab.values())


    def m_plot_thickness(self):

        '''Return a plot Intrusion Thickness versus Diameter.
        Don't forget to define self.col = 'Thick_K' or 'Thick_C' corresponding
        to whant you want ( see attribut)'''
        
        mask_F = self.m_table_thickness()
        fig,ax = self.m_figure() # create the top-level container
        c = self.m_color_map(len(mask_F.keys())) # hold for kalynn and derived thickenss
        i=0
        for key,df in mask_F.iteritems():
            print key
            df['size']= 100
            ax.scatter(df.Diameter,df[self.col],df.size,color=c[i],label = key)
            i+=1
        self.x_label = 'Diameter (km)'
        self.y_label = 'Intrusion thickness (km)'
        self.m_axes(ax)
        self.m_save_fig(fig)

    def m_plot_thickness_err(self):

        '''Return a plot Intrusion Thickness versus Diameter.
        Don't forget to define self.col = 'Thick_K' or 'Thick_C' corresponding
        to whant you want ( see attribut)'''
        
        mask_F = self.m_table_thickness()
        fig,ax = self.m_figure() # create the top-level container
        c = self.m_color_map(len(mask_F.keys())) # hold for kalynn and derived thickenss
        i=0
        for key,df in mask_F.iteritems():
            print key
            df['size']= 100
            ax.errorbar(df.Diameter,df.Thick_C,yerr=df.Sigma_Thick_C,color=self.info[key]['color'],alpha=self.info[key]['alpha'],fmt=self.info[key]['marker'],markersize=self.info[key]['s'],zorder=0,label = self.info[key]['label'])
            i+=1
        self.x_label = 'Diameter (km)'
        self.y_label = 'Intrusion thickness (km)'
        self.m_axes(ax)
        self.m_save_fig(fig)
        
        
    def m_error_bar(self):
        ''' retunr a scatter plot depth versus Diameter
        with error bar corresponding to the different location defined
        in self.mask_C and self.mask_F. The corresponding scaling laws ( output of Coeff_Fit)
        are also shown'''

        fig,ax = self.m_figure() 
        c = self.m_color_map(len(self.mask_C+self.mask_F))
        mask = self.m_mask_F_C(self.mask_F,self.mask_C)
        coeff = self.m_Coeff_Fit()
        i=0
        for key,df in mask.iteritems():
            df = df[df.Sigma_Depth<self.err_max]
            df = df.sort('Diameter')
            ax.errorbar(df.Diameter,df.Depth,yerr=df.Sigma_Depth/df.Depth,color=self.info[key]['color'],alpha=self.info[key]['alpha'],fmt=self.info[key]['marker'],markersize=self.info[key]['s'],zorder=0,label = self.info[key]['label'])
            if self.info[key]['bool']:
                ax.plot(df.Diameter,coeff[key][0]*df.Diameter**coeff[key][1],color='k',zorder = 0,linewidth = 3,label = self.info[key]['label_fit'])#+': '+'d={0:1.3f}'.format(coeff[key][0])+r'$D^{'+'{0:1.3f}'.format(coeff[key][1])+r'}$')
            i +=1
        self.m_axes(ax)
        ax.set_rasterization_zorder(1)
        self.m_save_fig(fig)
        

    def m_scatter(self):
        ''' retunr a scatter plot depth versus Diameter
        corresponding to the different location defined in self.mask_C and self.mask_F.
        The corresponding scaling laws ( output of Coeff_Fit)
        are also shown'''

        fig,ax = self.m_figure() # create the top-level container
        c = self.m_color_map(len(self.mask_C+self.mask_F))
        mask = self.m_mask_F_C(self.mask_F,self.mask_C)
        coeff = self.m_Coeff_Fit()
        i=0
        for key,df in mask.iteritems():
            df = df[df.Sigma_Depth<self.err_max]
            ax.scatter(df.Diameter,df.Depth,color=c[i], alpha=self.alpha, marker='o',
                        label= key+':'+'d={0:1.3f}'.format(coeff[key][0])+r'$D^{'+'{0:1.3f}'.format(coeff[key][1])+r'}$')
            ax.scatter(df.Diameter,coeff[key][0]*df.Diameter**coeff[key][1],color=c[i])
            i +=1
        self.x_label = 'Diameter (km)'
        self.y_label = 'Crater depth (km)'
        self.y_lim = 0,8
        self.m_axes(ax)
        self.m_save_fig(fig)

        return ax

    def To_LaTeX(self,df,size):
        """
        Convert a pandas dataframe to a LaTeX tabular.
        Prints labels in bold, does not use math mode
        """
        df=df.applymap(lambda x: '%10.2f' % x)
        df.index= [elt.replace('_','\_') for elt in df.index]
        numColumns = df.shape[1]
        numRows = df.shape[0]
        output = StringIO.StringIO()
        alignment='>{\\centering\\arraybackslash}m{'+size+'}|'
        numColumns = df.shape[1]
        colFormat = ("%s | %s " % (alignment, alignment * (numColumns)))
        #Write header
        output.write("\\begin{tabular}{%s}\n" % colFormat)
        columnLabels = ["%s" % label for label in df.columns]
        output.write('&\multicolumn{3}{'+alignment+'}{}&\multicolumn{3}{c|}{$d=AD^B$} \\\\ \n')
        output.write("& %s \\\\ \n" % " & ".join(columnLabels))
        output.write("\hline \n")
        #output.write('&'*(numColumns) + '\\\\ \n')
        #Write data lines
        for i in xrange(numRows):
            output.write("%s & %s \\\\ \n"
                        % (df.index[i], " & ".join([str(val) for val in df.ix[i]])))
           # output.write('&'*(numColumns) + '\\\\ \n')
        #Write footer
        output.write("\\end{tabular}")
        return output.getvalue()
