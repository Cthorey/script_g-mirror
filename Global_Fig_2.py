#!/usr/bin/env python
#######################
# Ce script est utiliser pour creer des cartes globals. Permets de plotter les craters ( de differentes sortes)"


#######################
# Import des library

import os
import subprocess
import pandas as pd
import sys
import numpy as np
from Data_Load import *
from Plot import *    
from Histo_Mean import *

Racine = '/Users/thorey/Documents/These/Projet/FFC/Gravi_GRAIL/Traitement/Script_G/'

#for mod in np.concatenate((np.arange(10,40,10),np.arange(40,120,20)),axis=0):
map(os.remove,[elt for elt in os.listdir(Racine) if elt.split('.')[-1]=='xy'])

for mod in [60]:
    #######################
    # A definir
    Name_root = '/Users/thorey/Documents/These/Projet/FFC/Gravi_GRAIL/Resultat/Global_Fig/'
    Carte1 = '/Users/thorey/Documents/These/Projet/FFC/Gravi_GRAIL/Traitement/LOLA/34_12_3220_660_80_misfit_Lola.grd'
    #Carte1 = '/Users/thorey/Documents/These/Projet/FFC/Gravi_GRAIL/Traitement/GRAV/JGGRAIL_900C9A_CrustAnom_'+str(mod)+'/Global_map/34_12_3220_900_'+str(mod)+'_misfit_rad.grd'
    Carte2 = '/Users/thorey/Documents/These/Projet/FFC/Gravi_GRAIL/Traitement/GRAV/JGGRAIL_900C9A_CrustAnom_'+str(mod)+'/Global_map/34_12_3220_900_'+str(mod)+'_misfit_rad.grd'

    C1 = [Carte1,'Bf2a2:"Topography_(km)":','-8/8/1','/Users/thorey/Documents/Tool/Colormap/33_blue_red.cpt']
    #C1=[Carte1,'Bf20a20:"Radial_Gravity_Anomaly(mGal)":','-80/80/10','/Users/thorey/Documents/Tool/Colormap/33_blue_red.cpt']
    C2=[Carte2,'Bf20a20:"Radial_Gravity_Anomaly(mGal)":','-80/80/10','/Users/thorey/Documents/Tool/Colormap/33_blue_red.cpt']
    C4='/Users/thorey/Documents/These/Projet/FFC/Gravi_GRAIL/Grail_Dataset/USGS_Mare_Basalts/USGS_mare_basalts_3.gmt'

    #######################
    # On creer les fichier .xy
    obj = Load_G()
    obj.Lola = 'LDEM_64_'
    obj.Range = 20,184
    mask = obj.m_mask_F_C(['F_H'],['C_H'])

    for key,df in mask.iteritems():
        df.Diameter=(df.Diameter/2.0)*360/(1734*2*3.14)
        df[['Long','Lat','Diameter']].to_csv(Racine+key+'.xy',sep=',',index=None,header=None)

    #df= pd.read_csv('/Users/thorey/Documents/These/Projet/FFC/Gravi_Grail/Traitement/Info_General/FFC_final.txt')
    #df = df[df.Mask_Highlands & df.Mask_Highlands_150]
    #df[['Long','Lat','Class']].to_csv(Racine+'F_H_150'+'.xy',sep = ',',index=None,header=None)
    #df= pd.read_csv('/Users/thorey/Documents/These/Projet/FFC/Gravi_Grail/Traitement/Info_General/Crater_Head_final.txt')
    #df = df[df.Mask_Highlands & df.Mask_Highlands_150]
    #df[['Long','Lat','Lat']].to_csv(Racine+'C_H_150'+'.xy',sep = ',',index=None,header=None)

    file = [f for f in os.listdir(Racine) if f.split('.')[-1]=='xy']

    Name_fig = file[0].split('.')[0]+'_'+file[1].split('.')[0]+'_'+str(mod)
    #Name_fig = file[0].split('.')[0]
    Title_fig = 'Bg30WSen:."{0:s}".:'.format(Name_fig)
    print Name_fig
    #######################
    # Ecriture du fichier temporaire bash

    with open( str(Racine)+'Global_Map.sh' , 'r') as script:
        with open(str(Racine)+'Global_Map'+'_tmp.sh', 'wr+') as script_tmp:
            for l in script:
                if l == 'Scarte1=Carte1\n':
                    to_write = l.replace('Carte1',str(C1[0]))
                elif l== 'SLegend1=legend\n':
                    to_write = l.replace('legend', str(C1[1]))
                elif l== 'SScale1=Echelle\n':
                    to_write = l.replace('Echelle', str(C1[2]))
                elif l== 'SColormap1=col\n':
                    to_write = l.replace('col', str(C1[3]))
                    
                elif l == 'Scarte2=Carte2\n':
                    to_write = l.replace('Carte2',str(C2[0]))
                elif l== 'SLegend2=legend\n':
                    to_write = l.replace('legend', str(C2[1]))
                elif l== 'SScale2=Echelle\n':
                    to_write = l.replace('Echelle', str(C2[2]))
                elif l== 'SColormap2=col\n':
                    to_write = l.replace('col', str(C2[3]))
        
                elif l == 'Title=titre\n':
                    to_write = l.replace('titre',Title_fig)
                elif l == 'Name=Image\n':
                    to_write = l.replace('Image',Name_root+Name_fig)
                elif l == 'position1=Position_Mer\n':
                    to_write = l.replace('Position_Mer',C4)
                elif l == 'position2=Position\n':
                    to_write = l.replace('Position',Racine+file[0])
                elif l == 'position3=Position\n':
                    to_write = l.replace('Position',Racine+file[1])
                else:
                    to_write = l
                script_tmp.write(to_write)
            
        subprocess.call(str(Racine)+'/Global_Map'+'_tmp.sh',shell=True)

map(os.remove,[elt for elt in os.listdir(Racine) if elt.split('.')[-1]=='xy'])
#map(os.remove,[f for f in os.listdir(Name_root) if f.split('.')[-1]=='ps'])


# TIP

#C.Diameter = C.Diameter/2000.0
#C['Symbol_Size'] = C.Diameter.apply(str)+'i'
