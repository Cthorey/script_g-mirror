#!/usr/bin/env python

         #######################################################################################################################
                    ################# RECUPERE LES DONNE RELATIF A LA CLASSE DE CRATER ETUDIE #################
         #######################################################################################################################
    
def Variable(type,Root):

    import numpy as np
    import random
    import sys
    import string
    
    name=[]
    lat=[]
    long=[]
    clas=[]
    diame=[]
    Maria=[]
    depth=[]
         #######################################################################################################################
                                    ################# EXTRACTION DES DONNE DU FICHIER DE BASE #################
         #######################################################################################################################
    
    if type=='FFC': # Correspond au fichier de Jozwiak sur les FFCs
        with open(Root+'Gravi_Grail/Traitement/Info_General/Coordonne.txt', 'r') as file:
            line_nb = 0
                # Itere sur les lignes du fichier de data
            for line in file:
                # Nettoie un peu la ligne en enlevant le caratere de fin de lign
                # et range les infos dans une liste = split_line
                split_line = line.split(',')

            for i,elt in enumerate(split_line):
                if i % 5 == 0:
                    name.append(elt)
                if i % 5 == 1:
                    lat.append(elt)
                if i % 5 == 2:
                    long.append(elt)
                if i % 5 == 3:
                    clas.append(elt)
                if i % 5 == 4:
                    diame.append(elt)

                    # On enleve els espace qui font buger les nom
            for i,elt in enumerate(name):
                # print name
                if len(elt.split(' ')) == 2:
                    name[i]=str(elt.split(' ')[0])+str(elt.split(' ')[1])
                if name[i] == '' :
                    name[i]='A'+str(i)
                if len(elt.split(' ')) == 3:
                    name[i]=str(elt.split(' ')[0])+str(elt.split(' ')[1])+str(elt.split(' ')[2])
                if len(elt.split(' ')) == 4:
                    name[i]=str(elt.split(' ')[0])+str(elt.split(' ')[1])+str(elt.split(' ')[2])+str(elt.split(' ')[3])
                if elt.split(' ')[0] == 'Balboa' :
                    name[i]='Balboa'
                if elt.split(' ')[0] == 'Barbier' :
                    name[i]='Barbier'
                if elt.split("'")[0] == 'Bel' :
                    name[i]='BelKovitchA'
                if elt == 'Sommerfeld':
                    name[i]='Somerfeld'

            for i,elt in enumerate(clas):
                if elt == '4a' :
                    clas[i]=4
                elif elt == '4b' :
                    clas[i]=4

            for i,elt in enumerate(diame):
                diame[i]=diame[i].strip()
                diame[i]=np.float(diame[i])
                    #Position general de tous les craters

            for i,elt in enumerate(lat):
                if elt.split(' ')[1] == 'N':
                    lat[i]=float(lat[i].split(' ')[0])
                else:
                    lat[i]=-float(lat[i].split(' ')[0])
                if long[i].split(' ')[1] == 'E':
                    long[i]=float(long[i].split(' ')[0])
                else:
                    long[i]=-float(long[i].split(' ')[0])
                    
        with open(Root+'Gravi_Grail/Traitement/Info_General/FFC_Maria_Resample.csv', 'r') as file:
            FFC_Name=[]
            FFC_Maria=[]
            for line in file:
                split_line = line.split(',')
                for i,elt in enumerate(split_line):
                    if i % 5 == 2:
                        FFC_Name.append(elt)
                    if i % 5 == 4:
                        FFC_Maria.append(elt)
            for i,elt in enumerate(lat) :
                if name[i] in FFC_Name :
                    Maria.append(FFC_Maria[FFC_Name.index(name[i])].strip())
                else :
                    Maria.append('Highland')
            
            
    elif type=='Crater':  # Correspond au fichier des petits crater copernician
        with open(Root+'Gravi_Grail/Traitement/Info_General/Craters.csv', 'r') as file:
            line_nb = 0
            # Itere sur les lignes du fichier de data
            for line in file:
                # Nettoie un peu la ligne en enlevant le caratere de fin de lign
                # et range les infos dans une liste = split_line
                split_line =  line.strip().split(';')

            for i,elt in enumerate(split_line):
                if i % 4 == 0 and len(split_line[i].split(' '))==1:
                    name.append(elt)
                if i % 4 == 1 and len(split_line[i-1].split(' '))==1:
                    lat.append(elt)
                if i % 4 == 2 and len(split_line[i-2].split(' '))==1:
                    long.append(elt)
                if i % 4 == 3 and len(split_line[i-3].split(' '))==1:
                    diame.append(elt)
                    clas.append('')
                Maria.append('')

            # On enleve els espace qui font buger les nom
            for i,elt in enumerate(name):
                if i!=0:
                    name[i]="".join(list(name[i])[1:])

            #On creer des noveaux vecteurs lat et long de la meme maniere que ds l'autre ficheir

            for i,elt in enumerate(lat):
                if len(list(elt)) == 6:
                    lat[i]=str("".join(list(elt)[0:5]))+' '+ str(list(elt)[5])
                if len(list(elt)) == 5:
                    lat[i]=str("".join(list(elt)[0:4]))+' '+ str(list(elt)[4])
                if len(list(elt)) == 4:
                    lat[i]=str("".join(list(elt)[0:3]))+' '+ str(list(elt)[3])

            for i,elt in enumerate(long):
                if len(list(elt)) == 6:
                    long[i]=str("".join(list(elt)[0:5]))+' '+ str(list(elt)[5])
                if len(list(elt)) == 5:
                    long[i]=str("".join(list(elt)[0:4]))+' '+ str(list(elt)[4])
                if len(list(elt)) == 4:
                    long[i]=str("".join(list(elt)[0:3]))+' '+ str(list(elt)[3])

            # On creer les vecteurs lat et long homogene avec les autres set de donnee
                    
            for i,elt in enumerate(lat):
                if elt.split(' ')[1] == 'N':
                    lat[i]=float(lat[i].split(' ')[0])
                else :
                    lat[i]=-float(lat[i].split(' ')[0])
                if long[i].split(' ')[1] == 'E':
                    long[i]=float(long[i].split(' ')[0])
                else:
                    long[i]=-float(long[i].split(' ')[0])
                    
    elif type=='Crater_Head': #Correspond au fichier des craters decrit par Jim Head sur son site www.planetary.brown.edu/html_pages/LOLAcraters.html
        with open('/Users/thorey/Documents/These/Projet/FFC/Gravi_Grail/Traitement/Info_General/LolaLargeLunarCraterCatalog.csv', 'r') as file:
            line_nb = 0
            # Itere sur les lignes du fichier de data
            for line in file:
                split_line = line.split(',')
                line_nb=line_nb+1
                if line_nb!=1:
                    for i,elt in enumerate(split_line):
                        if i % 3 == 0:
                            long.append(elt)
                        if i % 3 == 1:
                            lat.append(elt)
                        if i % 3 == 2:
                            diame.append(elt)
                            name.append('C'+str(line_nb-1))
                            clas.append(0)
                            Maria.append('')

        for i,elt in enumerate(diame):
            diame[i]=diame[i].strip()

        print len(diame)


    elif type=='Crater_Kalynn_2': #Correspond au fichier des craters decrit par Kalynn et al dans son papier sur la revision des Crater-depth-diameter.
        with open('/Users/thorey/Documents/These/Projet/FFC/Gravi_Grail/Traitement/Info_General/Crater_Kalynn_2.csv', 'r') as file:
            line_nb = 0
            # Itere sur les lignes du fichier de data
            for line in file:
                split_line = line.split(',')
                line_nb=line_nb+1
                if line_nb!=1:
                    for i,elt in enumerate(split_line):
                        if i % 6 == 0:
                            diame.append(elt)
                        if i % 6 == 1:
                            lat.append(elt)
                        if i % 6 == 2:
                            long.append(elt)
                        if i % 6 == 5:
                            Maria.append(elt)
                            name.append('C'+str(line_nb-1))
                            clas.append(0)
        for i,elt in enumerate(diame):
            diame[i] = diame[i].strip()
            Maria[i] = Maria[i].strip()

                

         #######################################################################################################################
                                    ################# ON ECRIT DANS UN FICHIER LES CRATERES QU'ON ETUDIE #################
         #######################################################################################################################

    print 'Le nombre de crater est egale a : ',len(name)
    fichier = open('Position_'+str(type)+'.xy', "w+")
    fichier2 = open('KLM_'+str(type)+'.csv','w+')
    for i,elt in enumerate(lat):
        Coord=str(float(lat[i]))+ ' ' + str(float(long[i]))
        fichier.write(str(Coord)+"\n" )
        fichier2.write(str(long[i])+','+ str(lat[i])+','+str(name[i])+"\n")
    fichier.close()
    fichier2.close()


    return [name,lat,long,clas,diame,Maria]


