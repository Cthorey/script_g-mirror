import sys,os
import matplotlib.pylab as plt
import numpy as np
import pandas as pd
import Fig_Tool as FST
from mpl_toolkits.axes_grid1 import make_axes_locatable
from Data_Load import *
from Plot import *    

class HistoMean(Load_G,Plot):

    '''
    The aim of this class is to give a set of tool to understand the evolution
    of the mean gravity as a function of diameter !! :)

    Class parameter:''

    attribut :

    The class inherit from Load_G and Plot.
    bin : taille de la bin
    bin_max : taille de la valeur max dans la bin
    bin_min : taille de la valeur min de la bin
    
    col : Column that we want to examine. If we want to consider gravity,
    put the name of the column +_, e.g 'GE_mean_' or 'GI_mean_'. Instead,
    when considerign a column with no filter dependance, put 'Depth','Class'.
    
    model = List that contains the value of lambda we want to
    consider, e,g [10,20] will considere the value lambda = 10 and
    lambda =20 for the low pass filter. Let blank if considering the
    depth or other column that do not depend upon the filter
    
    mask_F = List that contains the location of the FFC u want to study.
    See Load_F for more details
    mask_C = List that contains the location of the UC u want to study.
    See Load_F for more details.

    method :
    - m_load(): Method that take no argument and that return different
    variable that are gonna be use in this class.

    - m_slice(_df,name) : Method that take a dataframe and the third output of
    m_load. Send back a dataframe with a number of columns that depend upon
    the considered column. Nevertherless, basically, u have  4 colonnes
    4 colonnes 'Diameter','Mean','Std','Size', each row considerign to
    one bin

    - m_table() : Method that take no argument and return a table
    with m_slice out put 'Diameter','Mean','Std','Size'

    -m_plot_mean() Method that take no argument and return a plot
    of Mean versus Diameter for the case considered. Return also
    error bar that correspond to the standard error for each bin.

    -m_plot_var() Method that take no argument and return a plot
    of std versus diameter for the case cosndiered. '''

    
    
    def __init__(self,mask_C,mask_F,model):
        ''' constructor'''
        Load_G.__init__(self)
        Plot.__init__(self)
        self.bin = 10 # valeur de la bin
        self.bin_min = 20 # bin min
        self.bin_max = 210 # bin max
        self.y_label = 'Local Mean $\mu$ (mGal)'
        self.x_label = 'Diameter (km)'
        self.col = 'Local_Mean_'
        self.model = [str(elt) for elt in model]
        self.mask_F = mask_F
        self.mask_C = mask_C
        self.lable = ['UC','UC_close_FFC','FFC']
        self.fig = ''
        self.ax = ''
        self.info = ''
                

    def m_load(self):

        if len(self.model) != 0:
            if self.col[-1] != '_' :
                print ' Add a "_" at the end of the name when considering a column that depend upon the filter'
                #sys.exit()
                
        mask = self.m_mask_F_C(self.mask_F,self.mask_C)
        tab = dict.fromkeys(mask)
        cols = [self.col+mod for mod in self.model]
        name_mean = ['mean'+'_'+elt.split('_')[-1] for elt in cols]
        name_std = ['std'+'_'+elt.split('_')[-1] for elt in cols]
        name_ste = ['ste'+'_'+elt.split('_')[-1] for elt in cols]
        name = ['Diameter']+name_mean+name_std+name_ste+['Size']

        return mask,tab,name,name_mean,name_std,name_ste
    
    def m_slice(self,_df,name):
        ''' Renvoie un Dataframe avec 4 colonnes 'Diameter','Mean','Std','Size' '''

        bins = np.arange(self.bin_min,self.bin_max,self.bin)
        result =[]
        cols = [self.col+mod for mod in self.model]
        
        for elt in bins:
            df_temp = _df[(_df.Diameter<elt+self.bin/2.0)
                              & (_df.Diameter>elt-self.bin/2.0)] # df temporaire correspond a la bin
            try :
                size = len(df_temp)
                mean = [df_temp[col].mean() for col in cols]
                std = [df_temp[col].std() for col in cols]
                ste = [df_temp[col].std()/np.sqrt(size) for col in cols]
            except :
                print 'bin trop petite'
                sys.exit()
            result.append([elt]+mean+std+ste+[size])
            
        df = pd.DataFrame(result,columns = name)
        return df
    
    def m_table(self):
        
        ''' Renvoie un tableau avec ['Diameter','Mean','Std','Size']'''
        
        mask,tab,name,name_mean,name_std,name_ste = self.m_load()
        for key,df in mask.iteritems():
            tab[key] = self.m_slice(df,name)
            tab[key]['Loc']=key
            final = pd.concat([tab[j] for j in mask.keys()])
        return final
    
    def m_plot_mean(self):
        ''' Renvoie un plot'''
        
        mask,tab,name,name_mean,name_std,name_ste = self.m_load()

        if self.fig == '':
            self.fig, self.ax = self.m_figure()
        c = self.m_color_map(len(self.mask_C+self.mask_F))
        i=0
        for key,df in mask.iteritems():
            hist = self.m_slice(df,name)
            for j,col in enumerate(name_mean):
                if self.info[key]['bool']:
                    self.ax.errorbar(hist.Diameter,hist[col],yerr = hist[name_ste[j]],color=self.info[key]['color'],alpha=1,marker='o',label = key,linewidth = 3,zorder=0)
                #ax.errorbar(hist.Diameter,hist[col],yerr = hist[name_ste[j]],color=c[i],alpha=self.alpha,marker='o',label = self.lable[i],linewidth = 3)#,
                           # label = key + ' M = {0:1.2f} mGal; SD = {1:1.2f} mGal'.format(df[self.col+col.split('_')[-1]].mean(),df[self.col+col.split('_')[-1]].std()))
            i+=1
        #self.m_axes(self.ax)
        self.ax.set_rasterization_zorder(1)
        self.m_axes_Ss_L(self.ax)
        self.m_save_fig(self.fig)

        return self.ax

    def m_plot_std(self):
        ''' Renvoie un plot'''
        
        mask,tab,name,name_mean,name_std,name_ste = self.m_load()
        
        fig, ax = self.m_figure()
        c = self.m_color_map(len(self.mask_C+self.mask_F))
        i=0
        for key,df in mask.iteritems():
            hist = self.m_slice(df,name)
            for j,col in enumerate(name_std):
                ax.plot(hist.Diameter,hist[col],color=c[i],alpha=self.alpha,marker ='o',label = key)
            i+=1
        self.m_axes(ax)
        self.m_save_fig(fig)
        

    
